<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Approval User Setup List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Approval User Setup</li>
        <li class="active">Approval User Setup List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
       <div class="row">
            <div class="col-xs-12">
               <div class="box box-primary">
                  <div class="box-header">
                     <form method="get" >

                     
                          <div class="col-sm-2">
                             <input type="text" class="form-control" id="searchnik" name="searchnik" placeholder="Search Name">
                          </div>
                        <div class="col-sm-2">
                           <select class="form-control select2" name="division_code" id="division_code" >
                              <option value="" selected>- Select Division-</option>
                              <?php
                                 $sql = "SELECT * FROM division";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['division_code']."'>".$comrow['division_name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2" name="department_code" id="department_code" >
                              <option value="" selected>- Select Department-</option>
                              <?php
                                 $sql = "SELECT * FROM department";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['dept_code']."'>".$comrow['name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2" name="location_code" id="location_code" >
                              <option value="" selected>- Select Location-</option>
                              <?php
                                 $sql = "SELECT * FROM location";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2" name="jobs" id="jobs" >
                              <option value="" selected>- Select Jobs Level-</option>
                              <?php
                                 $sql = "SELECT * FROM jobs";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['job_code']."'>".$comrow['name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2" name="status" id="status" >
                              <option value="" selected>- Select Status-</option>
                              <?php
                                 $sql = "SELECT * FROM configure where code ='STATUS'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        
                        <div class="col-sm-2">
                           <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>


                         <!--  <a href='employee_excel.php?nik=".$nik."&division=".$division_code."&dept=".$department_code."&location=".$loc_code."&job=".$job_code."&status=".$status."' data-toggle='form' class='btn btn-success btn-sm btn-flat'><i class='fa fa-file-excel-o'></i> Excel</a> -->
                          <?php  
                          if(isset($_GET['searchnik'])){
                               @$nik =$_GET['searchnik'];
                               @$division_code =$_GET['division_code'];
                               @$department_code =$_GET['department_code'];
                               @$loc_code =$_GET['location_code'];
                               @$job_code =$_GET['jobs'];
                               @$status =$_GET['status'];
                              }else{
                                $nik='';
                                $division_code ='';
                                $department_code  ='';
                                $loc_code  ='';
                                $job_code  ='';
                                $status  ='';
                              }
                                   // echo "<a href='employee_excel.php?nik=".$nik."&division=".$division_code."&dept=".$department_code."&location=".$loc_code."&job=".$job_code."&status=".$status."' data-toggle='form' class='btn btn-success btn-sm btn-flat'><i class='fa fa-file-excel-o'></i> Excel</a>" ?>  
                               <!--      <a href="javascript:printDiv('print-area-2');" class="btn btn-sm btn-warning">Cetak <i class="fa fa-print"></i></a> -->
                          <!--  <a href="employee_excel.php?nik=$nik" data-toggle="form" class="btn btn-success btn-sm btn-flat"><i class="fa fa-file-excel-o"></i> Excel</a> -->

                        </div>
                        
                     </form>

                  </div>
               </div>
            </div>
         </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <!--  <a href="employee_add.php"" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a> -->
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Tools</th>
                  <th>Company</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>Division</th>
                  <th>Position</th>
                  <th>Jobs Level</th>
                  <th>Location</th>
                  <th>Approve Level 1</th>
                  <th>Approve Level 1 Name</th>
                  <th>Approve Level 2</th>
                  <th>Approve Level 2 Name</th>
                </thead>
                <tbody>
                  <?php
                  if(isset($_GET['searchnik']))
                                 {
                                  $nik =$_GET['searchnik'];
                                  $division_code =$_GET['division_code'];
                                  $department_code =$_GET['department_code'];
                                  $loc_code =$_GET['location_code'];
                                  $job_code =$_GET['jobs'];
                                  $status =$_GET['status'];
                                   $sql = "SELECT id,
                                  company_code,
                                  nik,
                                  name,
                                  division_name,
                                  position_name,
                                  job_level,
                                  loc_name,
                                  approval_level_1,
                                  approval_level_1_name,
                                  approval_level_2,
                                  approval_level_2_name
                                   FROM approval_setup
                                   WHERE (name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                  OR nik LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END)
                                  AND division_code LIKE CASE '$division_code' WHEN '' THEN '%%' ELSE '$division_code' END
                                  AND department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                  AND loc_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END
                                  AND job_code LIKE CASE '$job_code' WHEN '' THEN '%%' ELSE '$job_code' END
                                  and status ='Active'
                                   ";
                                }
                                else
                                 {
                                  $loc_code =$user['location_code'];
                                   $sql = "SELECT id,
                                  company_code,
                                  nik,
                                  name,
                                  division_name,
                                  position_name,
                                  job_level,
                                  loc_name,
                                  approval_level_1,
                                  approval_level_1_name,
                                  approval_level_2,
                                  approval_level_2_name
                                   FROM approval_setup
                                   WHERE loc_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END
                                   and status ='Active'
                                   ";
                                 }


                  //
                   
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                          <td>
                           <!--<a href="employee_edit.php?employee_add=edit&id=<?php echo $row['id']; ?>">
                          <button name="edit" type="submit" class="btn btn-sm btn-flat"><i class="fa fa-edit"></i> Edit</a>-->  

                           <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['nik'];?>" href="approval_setup_edit.php?approval_setup_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                           <!--  <a class="btn btn-xs btn-warning"   data-toggle="tooltip" title="Delete Employee  <?php echo $row['nik'];?>" href="employee_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['nik'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a> -->
                            
                          </td>
                          <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['nik']; ?></td>
                          <td><?php echo $row['name']?></td>
                          <td><?php echo $row['division_name']; ?></td>
                          <td><?php echo $row['position_name']; ?></td>
                          <td><?php echo $row['job_level']; ?></td>
                          <td><?php echo $row['loc_name']; ?></td>
                          <td><?php echo $row['approval_level_1']; ?></td>
                          <td><?php echo $row['approval_level_1_name']; ?></td>
                          <td><?php echo $row['approval_level_2']; ?></td>
                          <td><?php echo $row['approval_level_2_name']; ?></td>
                          
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
  

  <?php include 'includes/footer.php'; ?>
 
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.photo').click(function(e){
    e.preventDefault();
    var id = $(this).data('id');
    getRow(id);
  });

});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'employee_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#id').val(response.id);
      $('#nik').html(response.nik);
      $('.del_employee_name').html(response.firstname+' '+response.lastname);
      $('#employee_name').html(response.firstname+' '+response.lastname);
      $('#edit_nik').val(response.nik);
      $('#company_val').val(response.company_code).html(response.company_name);
      $('#edit_lastname').val(response.lastname);
      $('#edit_address').val(response.address);
      $('#datepicker_edit').val(response.birthdate);
      $('#edit_contact').val(response.contact_info);
      $('#gender_val').val(response.gender).html(response.gender);
      $('#position_val').val(response.position_id).html(response.description);
      $('#schedule_val').val(response.schedule_id).html(response.time_in+' - '+response.time_out);
    }
  });
}
</script>
</body>
</html>
