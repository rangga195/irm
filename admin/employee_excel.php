<?php
	include 'includes/session.php';
	require_once '../PHPExcel/Classes/PHPExcel.php';
	// Panggil class PHPExcel nya
	$excel = new PHPExcel();
	// Settingan awal file excel
	$excel->getProperties()->setCreator('Administrator')
             ->setLastModifiedBy('')
             ->setTitle("Employee")
             ->setSubject("Employee")
             ->setDescription("Employee Master")
             ->setKeywords("Employee");
    //Style kolom
    $style_col = array(
		'font' => array('bold' => true), // Set font nya jadi bold
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Style row
	$style_row = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Set header
	$excel->setActiveSheetIndex(0)->setCellValue('A1', "EMPLOYEE TACO GROUP");
	$excel->getActiveSheet()->mergeCells('A1:BM1'); // Set Merge Cell pada kolom A1 sampai F1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// Buat header tabel nya pada baris ke 3
	$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('B3', "COMPANY");
	$excel->setActiveSheetIndex(0)->setCellValue('C3', "NIK");
	$excel->setActiveSheetIndex(0)->setCellValue('D3', "NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('E3', "DIVISION");
	$excel->setActiveSheetIndex(0)->setCellValue('F3', "DEPARTMENT");
	$excel->setActiveSheetIndex(0)->setCellValue('G3', "POSITION");
	$excel->setActiveSheetIndex(0)->setCellValue('H3', "JOB LEVEL");
	$excel->setActiveSheetIndex(0)->setCellValue('I3', "PARENT POSITION");
	$excel->setActiveSheetIndex(0)->setCellValue('J3', "LOCATION");
	$excel->setActiveSheetIndex(0)->setCellValue('K3', "GENDER");
	$excel->setActiveSheetIndex(0)->setCellValue('L3', "STATUS");
	$excel->setActiveSheetIndex(0)->setCellValue('M3', "STATUS WORKING");
	$excel->setActiveSheetIndex(0)->setCellValue('N3', "EMAIL OFFICE");
	$excel->setActiveSheetIndex(0)->setCellValue('O3', "EMAIL PERSONAL");
	$excel->setActiveSheetIndex(0)->setCellValue('P3', "JOIN DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('Q3', "RESIGN DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('R3', "BPJS TK NO");
	$excel->setActiveSheetIndex(0)->setCellValue('S3', "BPJS TK DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('T3', "BPJS KS NO");
	$excel->setActiveSheetIndex(0)->setCellValue('U3', "BPJS KS DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('V3', "NPWP NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('W3', "PTKP");
	$excel->setActiveSheetIndex(0)->setCellValue('X3', "KTP NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('Y3', "KK NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('Z3', "SIM NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('AA3', "BIRTH PLACE");
	$excel->setActiveSheetIndex(0)->setCellValue('AB3', "BIRTH DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('AC3', "AGE");
	$excel->setActiveSheetIndex(0)->setCellValue('AD3', "HEIGHT");
	$excel->setActiveSheetIndex(0)->setCellValue('AE3', "WEIGHT");
	$excel->setActiveSheetIndex(0)->setCellValue('AF3', "SIZE");
	$excel->setActiveSheetIndex(0)->setCellValue('AG3', "SHOE SIZE");
	$excel->setActiveSheetIndex(0)->setCellValue('AH3', "RELIGION");
	$excel->setActiveSheetIndex(0)->setCellValue('AI3', "PHONE NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('AJ3', "PHONE NO.2");
	$excel->setActiveSheetIndex(0)->setCellValue('AK3', "PHONE HOME");
	$excel->setActiveSheetIndex(0)->setCellValue('AL3', "BLOOD TYPE");
	$excel->setActiveSheetIndex(0)->setCellValue('AM3', "MARITAL STATUS");
	$excel->setActiveSheetIndex(0)->setCellValue('AN3', "HUSBAND/WIFE NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('AO3', "PHONE NO. HUSBAND/WIFE");
	$excel->setActiveSheetIndex(0)->setCellValue('AP3', "CHILD");
	$excel->setActiveSheetIndex(0)->setCellValue('AQ3', "FATHER NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('AR3', "PHONE NO. FATHER");
	$excel->setActiveSheetIndex(0)->setCellValue('AS3', "MOTHER NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('AT3', "PHONE NO. MOTHER");
	$excel->setActiveSheetIndex(0)->setCellValue('AU3', "NEAR FAMILY NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('AV3', "PHONE NO. FAMILY");
	$excel->setActiveSheetIndex(0)->setCellValue('AW3', "ADDRESS FAMILY");
	$excel->setActiveSheetIndex(0)->setCellValue('AX3', "HOBBY");
	$excel->setActiveSheetIndex(0)->setCellValue('AY3', "LANGUAGE SKILL");
	$excel->setActiveSheetIndex(0)->setCellValue('AZ3', "VEHICLE STATUS");
	$excel->setActiveSheetIndex(0)->setCellValue('BA3', "VEHICLE TYPE");
	$excel->setActiveSheetIndex(0)->setCellValue('BB3', "ADDRESS");
	$excel->setActiveSheetIndex(0)->setCellValue('BC3', "KELURAHAN");
	$excel->setActiveSheetIndex(0)->setCellValue('BD3', "KECAMATAN");
	$excel->setActiveSheetIndex(0)->setCellValue('BE3', "KABUPATEN KOTA");
	$excel->setActiveSheetIndex(0)->setCellValue('BF3', "PROVINCE");
	$excel->setActiveSheetIndex(0)->setCellValue('BG3', "POST CODE");
	$excel->setActiveSheetIndex(0)->setCellValue('BH3', "ADDRESS DOMICILY");
	$excel->setActiveSheetIndex(0)->setCellValue('BI3', "KELURAHAN DOMICILY");
	$excel->setActiveSheetIndex(0)->setCellValue('BJ3', "KECAMATAN DOMICILY");
	$excel->setActiveSheetIndex(0)->setCellValue('BK3', "KABUPATEN KOTA DOMICILY");
	$excel->setActiveSheetIndex(0)->setCellValue('BL3', "PROVINCE DOMICILY");
	$excel->setActiveSheetIndex(0)->setCellValue('BM3', "POST CODE DOMICILY");




	// Apply style header yang telah kita buat tadi ke masing-masing kolom header
	$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('T3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('U3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('V3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('W3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('X3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('Y3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('Z3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AA3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AB3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AC3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AD3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AE3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AF3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AG3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AH3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AI3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AJ3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AK3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AL3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AM3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AN3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AO3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AP3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AQ3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AR3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AS3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AT3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AU3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AV3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AW3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AX3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AY3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('AZ3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BA3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BB3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BC3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BD3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BE3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BF3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BG3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BH3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BI3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BJ3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BK3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BL3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('BM3')->applyFromArray($style_col);

	// Set height baris ke 1, 2 dan 3
	$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(30);
	
	$nik =$_GET['nik'];
    $division_code =$_GET['division'];
    $department_code =$_GET['dept'];
    $loc_code =$_GET['location'];
    $job_code =$_GET['job'];
    $status =$_GET['status'];
	$sql = mysqli_query($conn,"SELECT id,company_code,nik,NAME,division_name,department_name,description,job_name,
						parent_position,
						loc_name,
						gender,
						STATUS,
						status_work,
						email_office,
						email_personal,
						CASE entry_date WHEN '1900-01-01' THEN ''
								WHEN '0000-00-00' THEN ''
								ELSE IFNULL(entry_date,'') END AS entry_date ,
						CASE resign_date WHEN '1900-01-01' THEN ''
								WHEN '0000-00-00' THEN ''
								ELSE IFNULL(resign_date,'') END AS resign_date ,
						bpjs_tk_no,
						CASE bpjs_tk_date WHEN '1900-01-01' THEN ''
								WHEN '0000-00-00' THEN ''
								ELSE IFNULL(bpjs_tk_date,'') END AS bpjs_tk_date,
						bpjs_ks_no,
						CASE bpjs_ks_date WHEN '1900-01-01' THEN ''
								WHEN '0000-00-00' THEN ''
								ELSE IFNULL(bpjs_ks_date,'') END AS bpjs_ks_date,
						npwp_no,
						ptkp_status,
						ktp_no,
						kk_no,
						sim_no,
						birth_place,
						birth_date,
						age,
						height,
						weight,
						size,
						shoe_size,
						religion,
						phone_no,
						phone_no2,
						phone_home,
						blood_type,
						marital_status,
						partner_name,
						phone_no_partner,
						child,
						father_name,
						phone_no_father,
						mother_name,
						phone_no_mother,
						near_family,
						phone_no_family,
						address_family,
						hobby,
						language_skill,
						vehicle_status,
						vehicle_type,
						address,
						kelurahan,
						kecamatan,
						kabupaten_kota,
						provinsi,
						post_code,
						address2,
						kelurahan2,
						kecamatan2,
						kabupaten_kota2,
						provinsi2,
						post_code2
		FROM employee_view  WHERE name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                  AND division_code LIKE CASE '$division_code' WHEN '' THEN '%%' ELSE '$division_code' END
                                  AND department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                  AND location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END
                                  AND job_code LIKE CASE '$job_code' WHEN '' THEN '%%' ELSE '$job_code' END
                                  AND STATUS LIKE CASE '$status' WHEN '' THEN '%%' ELSE '$status' END
        order by nik ");
	
	$no = 0;
	$numrow = 3;
	while ($row = mysqli_fetch_array($sql)) {
		$no++;
		$numrow++;
		$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $row['company_code']);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('C'.$numrow, $row['nik'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('D'.$numrow, $row['name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, $row['division_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, $row['department_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('G'.$numrow, $row['description'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('H'.$numrow, $row['job_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('I'.$numrow, $row['parent_position'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('J'.$numrow, $row['loc_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('K'.$numrow, $row['gender'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('L'.$numrow, $row['status'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('M'.$numrow, $row['status_work'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('N'.$numrow, $row['email_office'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('O'.$numrow, $row['email_personal'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('P'.$numrow, $row['entry_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('Q'.$numrow, $row['resign_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('R'.$numrow, $row['bpjs_tk_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('S'.$numrow, $row['bpjs_tk_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('T'.$numrow, $row['bpjs_ks_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('U'.$numrow, $row['bpjs_ks_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('V'.$numrow, $row['npwp_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('W'.$numrow, $row['ptkp_status'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('X'.$numrow, $row['ktp_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('Y'.$numrow, $row['kk_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('Z'.$numrow, $row['sim_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AA'.$numrow, $row['birth_place'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AB'.$numrow, $row['birth_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AC'.$numrow, $row['age'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AD'.$numrow, $row['height'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AE'.$numrow, $row['weight'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AF'.$numrow, $row['size'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AG'.$numrow, $row['shoe_size'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AH'.$numrow, $row['religion'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AI'.$numrow, $row['phone_no'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AJ'.$numrow, $row['phone_no2'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AK'.$numrow, $row['phone_home'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AL'.$numrow, $row['blood_type'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AM'.$numrow, $row['marital_status'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AN'.$numrow, $row['partner_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AO'.$numrow, $row['phone_no_partner'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AP'.$numrow, $row['child'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AQ'.$numrow, $row['father_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AR'.$numrow, $row['phone_no_father'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AS'.$numrow, $row['mother_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AT'.$numrow, $row['phone_no_mother'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AU'.$numrow, $row['near_family'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AV'.$numrow, $row['phone_no_family'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AW'.$numrow, $row['address_family'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AX'.$numrow, $row['hobby'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AY'.$numrow, $row['language_skill'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('AZ'.$numrow, $row['vehicle_status'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BA'.$numrow, $row['vehicle_type'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BB'.$numrow, $row['address'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BC'.$numrow, $row['kelurahan'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BD'.$numrow, $row['kecamatan'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BE'.$numrow, $row['kabupaten_kota'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BF'.$numrow, $row['provinsi'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BG'.$numrow, $row['post_code'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BH'.$numrow, $row['address2'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BI'.$numrow, $row['kelurahan2'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BJ'.$numrow, $row['kecamatan2'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BK'.$numrow, $row['kabupaten_kota2'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BL'.$numrow, $row['provinsi2'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('BM'.$numrow, $row['post_code2'], PHPExcel_Cell_DataType::TYPE_STRING);

		// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('U'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('V'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('W'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('X'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('Y'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('Z'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AN'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AO'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AP'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AQ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AR'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AS'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AT'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AU'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AV'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AW'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AX'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AY'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('AZ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BA'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BB'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BC'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BD'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BE'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BF'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BG'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BH'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BI'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BJ'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BK'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BL'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('BM'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		
	}
	// Set width kolom
	$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
	$excel->getActiveSheet()->getColumnDimension('B')->setWidth(13); // Set width kolom B
	$excel->getActiveSheet()->getColumnDimension('C')->setWidth(23); // Set width kolom C
	$excel->getActiveSheet()->getColumnDimension('D')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('E')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('F')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('G')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('H')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('I')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('J')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('K')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('L')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('M')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('N')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('O')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('P')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('Q')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('R')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('S')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('T')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('U')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('V')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('W')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('X')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('Y')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('Z')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AA')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AB')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AC')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AD')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AE')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AF')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AG')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AH')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AI')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AK')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AL')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AM')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AN')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AO')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AP')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AR')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AS')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AT')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AU')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AV')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AW')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AX')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AY')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('AZ')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BA')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BB')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BC')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BD')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BE')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BF')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BG')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BH')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BI')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BJ')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BK')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BL')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('BM')->setWidth(23); // Set width kolom D
	// Set orientasi kertas jadi LANDSCAPE
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	// Set judul file excel nya
	$excel->getActiveSheet(0)->setTitle("Employee");
	$excel->setActiveSheetIndex(0);
	// Proses file excel
	// $objWriter = new PHPExcel_Writer_Excel2007($excel); 
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment; filename="employee.xlsx"'); // Set nama file excel nya
	header('Cache-Control: max-age=0');
	$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	// $write = PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	$write->save('php://output');
	exit;
?>