<?php include 'includes/session.php'; ?>
<?php
   include '../timezone.php';
   $range_to = date('Y-m-d');
   $range_from = date('Y-m-01');//date('Y-m-d', strtotime('-30 day', strtotime($range_to)));
   ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
      <?php include 'includes/navbar.php'; ?>
      <?php include 'includes/menubar.php'; ?>
    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
            <h1>
               Attendance
            </h1>
            <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
               <li class="active">Attendance</li>
            </ol>
         </section>
         <!-- Main content -->
         <section class="content">
            <?php
               if(isset($_SESSION['error'])){
                 echo "
                   <div class='alert alert-danger alert-dismissible'>
                     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                     <h4><i class='icon fa fa-warning'></i> Error!</h4>
                     ".$_SESSION['error']."
                   </div>
                 ";
                 unset($_SESSION['error']);
               }
               if(isset($_SESSION['success'])){
                 echo "
                   <div class='alert alert-success alert-dismissible'>
                     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                     <h4><i class='icon fa fa-check'></i> Success!</h4>
                     ".$_SESSION['success']."
                   </div>
                 ";
                 unset($_SESSION['success']);
               }
               ?>
            <div class="row">
               <div class="col-xs-12">
                  <div class="box box-primary">
                     <div class="box-header">
                        <form method="get" >
                           <div class="col-sm-2">
                             <input type="text" class="form-control" id="searchnik" name="searchnik" placeholder="Search Name">
                          </div>
                           <div class="input-group">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <div class="col-sm-2">
                                 <input autocomplete="off" type="text" class="form-control " id="start_date" name="start_date" value="<?php echo  $range_from; ?>" >
                              </div>
                              <div class="col-sm-2">
                                 <input autocomplete="off" type="text" class="form-control " id="end_date" name="end_date" value="<?php echo  $range_to; ?>" >
                              </div>
                              <div class="col-sm-2">
                               <select class="form-control select2" name="department_code" id="department_code" >
                                  <option value="" selected>- Select Department-</option>
                                  <?php
                                     $sql = "SELECT * FROM department";
                                     $query = $conn->query($sql);
                                     while($comrow = $query->fetch_assoc()){
                                       echo "
                                         <option value='".$comrow['dept_code']."'>".$comrow['name']."</option>
                                       ";
                                     }
                                     ?>
                               </select>
                            </div>
                              <div class="col-sm-2">
                                 <select class="form-control select2" name="location_code" id="location_code" >
                                    <option value="" selected>- Select Location -</option>
                                    <?php
                                       $loc_code =$user['location_code'];
                                       $sql = "SELECT * FROM location  
                                               WHERE loc_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END";
                                       $query = $conn->query($sql);
                                       while($comrow = $query->fetch_assoc()){
                                         echo "
                                           <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                                         ";
                                       }
                                       ?>
                                 </select>
                              </div>
                              <div class="col-sm-2">
                              <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                                 
                                     <?php  
                                    if(isset($_GET['searchnik'])){
                                         @$nik =$_GET['searchnik'];
                                         @$start_date =$_GET['start_date'];
                                         @$end_date =$_GET['end_date'];
                                         @$department_code =$_GET['department_code'];
                                         @$location_code =$_GET['location_code'];
                                        
                                        }else{
                                          $nik ='';
                                          $start_date =$range_from;
                                          $end_date=$range_to;
                                          $location_code ='';
                                          $department_code='';
                                        }
                                          
                                 echo "<a href='attendance_summary_excel.php?nik=".$nik."&start_date=".$start_date."&end_date=".$end_date."&department_code=".$department_code."&location_code=".$location_code."' data-toggle='form' class='btn btn-success btn-sm btn-flat'><i class='fa fa-file-excel-o'></i> Excel</a>"?> 
                             </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <div class="box box-solid box-primary">
                     <div class="box-header">
                        <div class="pull-right">
                           <form method="get" class="form-inline" >
                           </form>
                        </div>
                       
                     </div>
                     <div class="box-body">
                        <div class="table-responsive">
                           <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                              <thead>
                                
                                 <th>Company</th>
                                 <th>Employee ID</th>
                                 <th>Name</th>
                                 <th>Department</th>
                                 <th>Location</th>
                                 <th>Work Hour</th>
                                 <th>Work Minute</th>
                                 <th>Late Minute</th>
                                 <th>Overtime</th>
                                 <th>Holiday</th>
                                 <th>Work Day</th>
                                 <th>Attend</th>
                                 <th>Late</th>
                                 <th>Leave</th>
                                 <th>Special Leave</th>
                                 <th>Sick</th>
                                 <th>Permit</th>
                                 <th>Abnormal</th>
                                 <th>Absent</th>
                                
                              </thead>
                              <tbody>
                                 <?php
                                    if(isset($_GET['start_date']))
                                    {
                                      $nik = $_GET['searchnik'];
                                      $start_date = $_GET['start_date'];
                                      $end_date = $_GET['end_date'];
                                      $loc_code =$_GET['location_code'];
                                      $department_code =$_GET['department_code'];
                                      $location_code =$user['location_code'];

                                      if ($loc_code=='')
                                      {
                                         $sql = "SELECT a.company_code,a.nik,b.name,b.loc_name,b.department_code,b.department_name,
                                                FLOOR( IFNULL(SUM(a.work_hour),0) + (IFNULL(SUM(a.work_minute),0)/60)) AS Hours,
                                                ROUND((CONVERT(SUBSTR(ROUND(IFNULL(SUM(a.work_minute),0)/60 ,2), LOCATE('.', ROUND(IFNULL(SUM(a.work_minute),0)/60 ,2))+1),DECIMAL)/100)*60,0) AS Minutes,
                                                IFNULL(SUM(a.late_minute),0) AS Late_Minute,
                                                ROUND(IFNULL(SUM(a.overtime),0),2) AS Overtime,
                                                IFNULL(SUM(a.is_holiday),0) AS Holiday,
                                                IFNULL(COUNT(att_date),0)-IFNULL(SUM(a.is_holiday),0) AS Work_Day,
                                                IFNULL(SUM(a.is_att),0) AS Att,
                                                IFNULL(SUM(a.is_late),0) AS late,
                                                IFNULL(SUM(a.is_leave),0) AS Cuti,
                                                IFNULL(SUM(a.is_sick),0) AS Sick,
                                                IFNULL(SUM(a.is_spc_leave),0) AS Special,
                                                IFNULL(SUM(a.is_izin),0) AS Izin,
                                                IFNULL(SUM(a.is_abnormal),0) AS Abnormal,
                                                IFNULL(SUM(a.is_absent),0) AS Absent
                                                FROM  attendance_new a
                                                LEFT JOIN employee_view b ON a.nik = b.nik
                                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date'
                                                and b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                                and b.location_code LIKE CASE '$location_code' WHEN '' THEN '%%' ELSE '$location_code' END 
                                                 AND b.status ='Active'
                                                  AND b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                                GROUP BY a.company_code,a.nik,b.name,b.department_code,b.department_name";
                                    
                                              }else {
                                                $sql = "SELECT a.company_code,a.nik,b.name,b.loc_name,b.department_code,b.department_name,
                                                FLOOR( IFNULL(SUM(a.work_hour),0) + (IFNULL(SUM(a.work_minute),0)/60)) AS Hours,
                                                ROUND((CONVERT(SUBSTR(ROUND(IFNULL(SUM(a.work_minute),0)/60 ,2), LOCATE('.', ROUND(IFNULL(SUM(a.work_minute),0)/60 ,2))+1),DECIMAL)/100)*60,0) AS Minutes,
                                                IFNULL(SUM(a.late_minute),0) AS Late_Minute,
                                                ROUND(IFNULL(SUM(a.overtime),0),2) AS Overtime,
                                                IFNULL(SUM(a.is_holiday),0) AS Holiday,
                                                IFNULL(COUNT(att_date),0)-IFNULL(SUM(a.is_holiday),0) AS Work_Day,
                                                IFNULL(SUM(a.is_att),0) AS Att,
                                                IFNULL(SUM(a.is_late),0) AS late,
                                                IFNULL(SUM(a.is_leave),0) AS Cuti,
                                                IFNULL(SUM(a.is_sick),0) AS Sick,
                                                IFNULL(SUM(a.is_spc_leave),0) AS Special,
                                                IFNULL(SUM(a.is_izin),0) AS Izin,
                                                IFNULL(SUM(a.is_abnormal),0) AS Abnormal,
                                                IFNULL(SUM(a.is_absent),0) AS Absent
                                                FROM  attendance_new a
                                                LEFT JOIN employee_view b ON a.nik = b.nik
                                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date'
                                                and b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                                and b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END 
                                                 AND b.status ='Active'
                                                  AND b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                                GROUP BY a.company_code,a.nik,b.name,b.department_code,b.department_name";
                                              }

                                     
                                    }
                                    else
                                    {
                                        
                                        $start_date = $range_from;
                                        $end_date = $range_to;
                                        $loc_code =$user['location_code'];
                                        $sql = "SELECT a.company_code,a.nik,b.name,b.loc_name,b.department_code,b.department_name,
                                                FLOOR( IFNULL(SUM(a.work_hour),0) + (IFNULL(SUM(a.work_minute),0)/60)) AS Hours,
                                                 ROUND((CONVERT(SUBSTR(ROUND(IFNULL(SUM(a.work_minute),0)/60 ,2), LOCATE('.', ROUND(IFNULL(SUM(a.work_minute),0)/60 ,2))+1),DECIMAL)/100)*60,0) AS Minutes,
                                                IFNULL(SUM(a.late_minute),0) AS Late_Minute,
                                                ROUND(IFNULL(SUM(a.overtime),0),2) AS Overtime,
                                                IFNULL(SUM(a.is_holiday),0) AS Holiday,
                                                IFNULL(COUNT(att_date),0)-IFNULL(SUM(a.is_holiday),0) AS Work_Day,
                                                IFNULL(SUM(a.is_att),0) AS Att,
                                                IFNULL(SUM(a.is_late),0) AS late,
                                                IFNULL(SUM(a.is_leave),0) AS Cuti,
                                                IFNULL(SUM(a.is_sick),0) AS Sick,
                                                IFNULL(SUM(a.is_spc_leave),0) AS Special,
                                                IFNULL(SUM(a.is_izin),0) AS Izin,
                                                IFNULL(SUM(a.is_abnormal),0) AS Abnormal,
                                                IFNULL(SUM(a.is_absent),0) AS Absent
                                                FROM  attendance_new a
                                                LEFT JOIN employee_view b ON a.nik = b.nik
                                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date' 
                                                and b.location_code  LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END 
                                                 AND b.status ='Active'
                                                  AND b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                                GROUP BY a.company_code,a.nik,b.name,b.department_code,b.department_name";
                                    
                                    }
                                    
                                    $query = $conn->query($sql);
                                    while($row = $query->fetch_assoc()){
                                    // $status = ($row['status'])?'<span class="label label-warning pull-right">ontime</span>':'<span class="label label-danger pull-right">late</span>';
                                    ?>
                                 <tr>
                                    
                                    <td><?php echo $row['company_code']; ?></td>
                                    <td><?php echo $row['nik']; ?></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><?php echo $row['department_name']; ?></td>
                                    <td><?php echo $row['loc_name']; ?></td>
                                    <td><?php echo $row['Hours']; ?></td>
                                    <td><?php echo $row['Minutes']; ?></td>
                                    <td><?php echo $row['Late_Minute']; ?></td>
                                    <td><?php echo $row['Overtime']; ?></td>
                                    <td><?php echo $row['Holiday']; ?></td>
                                    <td><?php echo $row['Work_Day']; ?></td>
                                    <td><?php echo $row['Att']; ?></td>
                                    <td><?php echo $row['late']; ?></td>
                                    <td><?php echo $row['Cuti']; ?></td>
                                    <td><?php echo $row['Special']; ?></td>
                                    <td><?php echo $row['Sick']; ?></td>
                                    <td><?php echo $row['Izin']; ?></td>
                                    <td><?php echo $row['Abnormal']; ?></td>
                                    <td><?php echo $row['Absent']; ?></td>
                                   
                                 </tr>
                                 <?php
                                    // echo "
                                    //   <tr>
                                    //     <td class='hidden'></td>
                                    //     <td>".date('M d, Y', strtotime($row['att_date']))."</td>
                                    //     <td>".$row['nik']."</td>
                                    //     <td>".$row['name']."</td>
                                    //     <td>".$row['time_in']."</td>
                                    //     <td>".$row['time_out']."</td>
                                    //     <td>".$row['work_hour']."</td>
                                    //     <td>".$row['work_minute']."</td>
                                    //     <td>
                                    //       <button class='btn btn-success btn-sm btn-flat edit' data-id='".$row['id']."'><i class='fa fa-edit'></i> Edit</button>
                                    //       <button class='btn btn-danger btn-sm btn-flat delete' data-id='".$row['id']."'><i class='fa fa-trash'></i> Delete</button>
                                    //     </td>
                                    //   </tr>
                                    // ";
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>   
   </div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/attendance_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
   <script>
      $(function(){
        $('.edit').click(function(e){
          e.preventDefault();
          $('#edit').modal('show');
          var id = $(this).data('id');
          getRow(id);
        });
      
        $('.delete').click(function(e){
          e.preventDefault();
          $('#delete').modal('show');
          var id = $(this).data('id');
          getRow(id);
        });
      });
      
      function getRow(id){
        $.ajax({
          type: 'POST',
          url: 'attendance_row.php',
          data: {id:id},
          dataType: 'json',
          success: function(response){
            $('#datepicker_edit').val(response.date);
            $('#attendance_date').html(response.date);
            $('#edit_time_in').val(response.time_in);
            $('#edit_time_out').val(response.time_out);
            $('#attid').val(response.attid);
            $('#employee_name').html(response.firstname+' '+response.lastname);
            $('#del_attid').val(response.attid);
            $('#del_employee_name').html(response.firstname+' '+response.lastname);
          } 
        });
      }
   </script>
</body>
</html>