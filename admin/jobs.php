<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Jobs Level
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Jobs</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <a href="jobs_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Company</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM jobs";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                         <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['job_code']; ?></td>
                           <td><?php echo $row['name']; ?></td>
                          <td>
                            <a class="btn btn-sm btn-primary btn-flat "   data-toggle="tooltip" title="Edit Data <?php echo $row['name'];?>" href="jobs_edit.php?jobs_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Jobs  <?php echo $row['name'];?>" href="jobs_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['name'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/jobs_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'jobs_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#jobsid').val(response.id);
      $('#edit_code').val(response.job_code);
      $('#edit_name').val(response.name);
      $('#del_jobsid').val(response.id);
      $('#del_jobs').html(response.name);
      $('#company_val').val(response.company_code).html(response.company_name);
    }
  });
}
</script>
</body>
</html>
