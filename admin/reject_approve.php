<?php
include 'includes/session.php';
include 'includes/sendEmail-v156.php';
if(($_GET['id'])&& $_GET['code']=='Leave')
{ 
	$data = mysqli_fetch_array(mysqli_query($conn, " SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.leave_code,c.config_desc,a.start_date,a.end_date,a.reason,a.remaining,a.approve_by,b.approval_level_1,b.approval_level_2 ,
		CASE a.approve_by WHEN  '' THEN b.approval_level_1 ELSE b.approval_level_2 END AS Approve_To
		FROM leave_employee a 
		JOIN employee_view b ON a.nik = b.nik 
		JOIN (SELECT * FROM configure WHERE CODE ='LEAVE') c ON a.leave_code = c.config_code
		JOIN request_approval d ON a.nik = d.nik AND a.id = d.id
		WHERE a.id='".$_GET['id']."'and a.status =2 AND d.code ='Leave' 

		"));
	$approve1 = $data['approval_level_1'];
	$approve2 = $data['approval_level_2'];
	$approveby =$data['approve_by'];

	if (($approveby=='') &&  ($approve1=$user['username']) && ($approve2 ==''))
	{

		$data_update1 ="update leave_employee
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";
		if($conn->query($data_update1))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:        </strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];


			if(email_localhost($to, $subject, $message, $sender, $password))
			{
											// echo "<script>alert('Request has been approve and email has been sent!')</script>
											//       ";
				$_SESSION['success'] = 'Request has been reject and email has been sent!';
			}
			else
			{
											 // echo "Email sending failed";   
				$_SESSION['error'] = 'Email sending failed!';
			}
		}



	}

	if (($approveby=='') &&  ($approve1=='') && ($approve2 ==$user['username']))
	{

		$data_update1 ="update leave_employee
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";
		if($conn->query($data_update1))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:        </strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];


			if(email_localhost($to, $subject, $message, $sender, $password))
			{
											// echo "<script>alert('Request has been approve and email has been sent!')</script>
											//       ";
				$_SESSION['success'] = 'Request has been reject and email has been sent!';
			}
			else
			{
											 // echo "Email sending failed";   
				$_SESSION['error'] = 'Email sending failed!';
			}
		}



	}


	if (($approveby=='') &&  ($approve1=$user['username']) && ($approve2 !=''))
	{
		$data_update2 ="update leave_employee
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";

		if($conn->query($data_update2))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:        </strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];
										//$password = base64_decode($ambilsmtp['password']);

			if(email_localhost($to, $subject, $message, $sender, $password))
			{
				$_SESSION['success'] = 'Request has been rejected and email has been sent!';
			}
			else
			{
				$_SESSION['error'] = 'Email sending failed !';
			}
		} 
	}   
	if (($approveby!='') &&  ($approve1!='') && ($approve2 =$user['username']))
	{

		$data_update1 ="update leave_employee
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";
		if($conn->query($data_update1))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';


			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];


			if(email_localhost($to, $subject, $message, $sender, $password))
			{
												// echo "<script>alert('Request has been approve and email has been sent!')</script>
												//       ";
				$_SESSION['success'] = 'Request has been rejected and email has been sent!';
			}
			else
			{
												 // echo "Email sending failed";   
				$_SESSION['error'] = 'Email sending failed!';
			}
		}

	}  
}



if(($_GET['id'])&& $_GET['code']=='Trip')
{
	$data = mysqli_fetch_array(mysqli_query($conn, "SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.start_date,a.end_date,a.check_in,a.check_out,a.notes,a.approve_by,b.approval_level_1,b.approval_level_2 ,CASE a.approve_by WHEN  '' THEN b.approval_level_1 ELSE b.approval_level_2 END AS Approve_To
		FROM trip a 
		JOIN employee_view b ON a.nik = b.nik 
		JOIN request_approval c ON a.nik = c.nik AND a.id = c.id
		WHERE a.id='".$_GET['id']."'and a.status =2 AND c.code ='Trip' 
		"));
	$approvetrip1 = $data['approval_level_1'];
	$approvetrip2 = $data['approval_level_2'];
	$approvetripby =$data['approve_by'];
	if (($approvetripby=='') &&  ($approvetrip1=$user['username']) && ($approvetrip2 ==''))
	{
		$data_update2 ="update trip
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update2))
		{

			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Business Trip for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Request Bussiness Trip has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
			{

				$_SESSION['success'] = 'Request has been rejected and email has been sent!';
			}


			else
			{

				$_SESSION['error'] = 'Email sending failed!';
			}
		}
	}
	if (($approvetripby=='') &&  ($approvetrip1=='') && ($approvetrip2 ==$user['username']))
	{
		$data_update2 ="update trip
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update2))
		{

			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Business Trip for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Request Bussiness Trip has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
			{

				$_SESSION['success'] = 'Request has been rejected and email has been sent!';
			}


			else
			{

				$_SESSION['error'] = 'Email sending failed!';
			}
		}
	}
	if (($approvetripby=='') &&  ($approvetrip1=$user['username']) && ($approvetrip2 !=''))
	{

		$data_update1 ="update trip
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";

		if($conn->query($data_update1))
		{
									// $_SESSION['success'] = 'Leave has been approve successfully';
									//echo "<script>alert('Request Leave Approved.1..!')</script>";


			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Business Trip for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Request Bussiness Trip has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';


			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];
										//$password = base64_decode($ambilsmtp['password']);

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = 'Email sending failed!';


		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = 'Email sending failed!';
		}  
	}

	if (($approvetripby!='') &&  ($approvetrip1!='') && ($approvetrip2 =$user['username']))
	{
		$data_update2 ="update trip
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update2))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Rejected Request Business Trip for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Request Bussiness Trip has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = 'Email sending failed!';

		}
		else
		{
							 // $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = 'Email sending failed!';
		}  

	}


}



if(($_GET['id'])&& $_GET['code']=='Overtime')
{
	$data = mysqli_fetch_array(mysqli_query($conn, "SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.overtime_date,a.start_time,a.end_time,a.notes,a.approve_by,b.approval_level_1,b.approval_level_2 ,CASE a.approve_by WHEN  '' THEN b.approval_level_1 ELSE b.approval_level_2 END AS Approve_To
		FROM overtime a 
		JOIN employee_view b ON a.nik = b.nik 
		JOIN request_approval c ON a.nik = c.nik AND a.id = c.id
		WHERE a.id='".$_GET['id']."'AND a.status =2 AND c.code ='Overtime'"));


	$approveOT1 = $data['approval_level_1'];
	$approveOT2 = $data['approval_level_2'];
	$approveOTby = $data['approve_by'];

	if (($approveOTby=='') &&  ($approveOT1=$user['username']) && ($approveOT2 ==''))
	{
		$data_update2 ="update overtime
		set status = 0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update2))
		{

			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Overtime Request for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';

			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Overtime Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Overtime Date:</strong></td><td>" . $data['overtime_date']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_time']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_time'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
		  $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='user'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = 'Email sending failed!';

		}
		else
		{
							 // $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = 'Email sending failed!';
		}                         

	}
	if (($approveOTby=='') &&  ($approveOT1='') && ($approveOT2 ==$user['username']))
	{
		$data_update2 ="update overtime
		set status = 0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update2))
		{

			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Overtime Request for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Overtime Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Overtime Date:</strong></td><td>" . $data['overtime_date']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_time']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_time'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = 'Email sending failed!';

		}
		else
		{
							 // $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = 'Email sending failed!';
		}                         

	}

	if (($approveOTby=='') &&  ($approveOT1=$user['username']) && ($approveOT2 !=''))
	{
		$data_update1 ="update overtime
		set status=0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";

		if($conn->query($data_update1))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Overtime Request to '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Overtime Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Overtime Date:</strong></td><td>" . $data['overtime_date']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_time']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_time'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';


			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];
										//$password = base64_decode($ambilsmtp['password']);

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = 'Email sending failed!';


		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = 'Email sending failed!';
		} 


	}
	if (($approveOTby!='') &&  ($approveOT1!='') && ($approveOT2 =$user['username']))

	{

		$data_update3 ="update overtime
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";               

		if($conn->query($data_update3))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Overtime Request for'.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Overtime request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Overtime Date:</strong></td><td>" . $data['overtime_date']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_time']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_time'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.="</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = 'Email sending failed!';

		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = 'Email sending failed!';
		}

	}

}
if(($_GET['id'])&& $_GET['code']=='Correction')
{

	$data = mysqli_fetch_array(mysqli_query($conn, "SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.att_date,a.check_in,a.check_out,a.reason,a.approve_by,b.approval_level_1,b.approval_level_2,CASE a.approve_by WHEN  '' THEN b.approval_level_1 ELSE b.approval_level_2 END AS Approve_To
		FROM attendance_correction a 
		JOIN employee_view b ON a.nik = b.nik 
		JOIN request_approval c ON a.nik = c.nik AND a.id = c.id
		WHERE a.id='".$_GET['id']."' and a.status =2"));

	$approveCorr1 = $data['approval_level_1'];
	$approveCorr2 = $data['approval_level_2'];
	$approveCorrby =$data['approve_by'];

	if (($approveCorrby=='') &&  ($approveCorr1=$user['username']) && ($approveCorr2 ==''))
	{
		$data_update3 ="update attendance_correction
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update3))
		{

			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Attendance Correction Request for'.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Attendance Correction Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Correction Date:</strong></td><td>" . $data['att_date']. "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.="</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = "Email sending failed"; 

		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = "Email sending failed"; 
		}
	}   
	if (($approveCorrby=='') &&  ($approveCorr1=='') && ($approveCorr2 ==$user['username']))
	{
		$data_update3 ="update attendance_correction
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update3))
		{

			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Attendance Correction Request for'.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Attendance Correction Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Correction Date:</strong></td><td>" . $data['att_date']. "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.="</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = "Email sending failed"; 

		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = "Email sending failed"; 
		}
	}                          

	if (($approveCorrby=='') &&  ($approveCorr1=$user['username']) && ($approveCorr2 !=''))
	{
		$data_update1 ="update attendance_correction
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."'and status =2 ";
		if($conn->query($data_update1))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Attendance Correction Request for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';

			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Attendance Correction Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Correction Date:</strong></td><td>" . $data['att_date']. "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';


			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = "Email sending failed"; 


		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = "Email sending failed"; 
		}  
	} 

	if (($approveCorrby!='') &&  ($approveCorr1!='') && ($approveCorr2 =$user['username']))
	{
		$data_update3 ="update attendance_correction
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update3))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Attendance Correction Request for'.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';

			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Attendance Correction Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Correction Date:</strong></td><td>" . $data['att_date']. "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['reason']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = "Email sending failed"; 


		}
		else
		{
									// $_SESSION['error'] = $conn->error;
			$_SESSION['error'] = "Email sending failed"; 
		}

	}

}

if(($_GET['id'])&& $_GET['code']=='Itinerary')
	
{
	$data = mysqli_fetch_array(mysqli_query($conn, "SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.transport_code,c.config_desc,a.from_departure,a.to_arrive,a.start_date,a.end_date,a.notes,a.approve_by,b.approval_level_1,b.approval_level_2 
		FROM travel a 
		LEFT JOIN employee_view b ON a.nik = b.nik 
		LEFT JOIN configure c ON a.transport_code = c.config_code 
		WHERE c.code='TRAVEL' and a.id='".$_GET['id']."' and a.status= 2"));
	$approveTravel1 = $data['approval_level_1'];
	$approveTravel2 = $data['approval_level_2'];
	$approveTravelby =$data['approve_by'];

	if (($approveTravelby=='') &&  ($approveTravel1=$user['username']) && ($approveTravel2 ==''))
	{
		$data_update1 ="update travel
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update1))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='user'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Itinerary Request for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Itinerary request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
			$message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
			$message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';


			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = "Email sending failed"; 


		}
		else
		{
			$_SESSION['error'] = "Email sending failed"; 
		}  

	}
	if (($approveTravelby=='') &&  ($approveTravel1=$user['username']) && ($approveTravel2 !=''))
	{
		$data_update2 ="update travel
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2 ";
		if($conn->query($data_update2))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Itinerary Request for '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';			
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Itinerary Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
			$message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
			$message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";			
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';
			
			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';
			
			else
				$_SESSION['error'] = "Email sending failed"; 

		}
		else
		{
			$_SESSION['error'] = "Email sending failed"; 
		}                         
	}                               
	if (($approveTravelby!='') &&  ($approveTravel1!='') && ($approveTravel2 =$user['username']))
	{
		$data_update3 ="update travel
		set status =0,
		approve_by ='',
		last_update_by = '".$user['username']."',
		last_update_date =NOW()
		where id ='".$_GET['id']."' and status =2";
		if($conn->query($data_update3))
		{
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['nik']."'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Reject Itinerary Request for'.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
			// $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
			// $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Itinerary Request has been rejected :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
			$message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
			$message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.="</table>";
			 $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='admin'"));
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];

			if(email_localhost($to, $subject, $message, $sender, $password))
				$_SESSION['success'] = 'Request has been rejected and email has been sent !';

			else
				$_SESSION['error'] = "Email sending failed"; 
		}
		else
		{
			$_SESSION['error'] = "Email sending failed"; 
		}
	}




}





header('location: request_approval.php'); 


?>