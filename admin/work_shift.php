<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
      <?php include 'includes/navbar.php'; ?>
      <?php include 'includes/menubar.php'; ?>
      <?php
         function bulan($int){
           if ($int==1){$hasil='January';}
           else if ($int==2){$hasil='February';}
           else if ($int==3){$hasil='March';}
           else if ($int==4){$hasil='April';}
           else if ($int==5){$hasil='May';}
           else if ($int==6){$hasil='June';}
           else if ($int==7){$hasil='July';}
           else if ($int==8){$hasil='August';}
           else if ($int==9){$hasil='September';}
           else if ($int==10){$hasil='October';}
           else if ($int==11){$hasil='November';}
           else if ($int==12){$hasil='December';}
           return $hasil;
         }
         ?>
      <?php
         include '../timezone.php';
         $range_to = date('Y-m-d');
         $range_from = date('Y-m-01');//date('Y-m-d', strtotime('-30 day', strtotime($range_to)));
         $bulan =date('mm');
         $tahun =date('YYYY');
         $tanggal='';
         ?>
        
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
            <h1>
               Shift Management
            </h1>
            <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
               <li class="active"> Shift Management</li>
            </ol>
         </section>
         <!-- Main content -->
         <section class="content">
            <?php
               if(isset($_SESSION['error'])){
                 echo "
                   <div class='alert alert-danger alert-dismissible'>
                     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                     <h4><i class='icon fa fa-warning'></i> Error!</h4>
                     ".$_SESSION['error']."
                   </div>
                 ";
                 unset($_SESSION['error']);
               }
               if(isset($_SESSION['success'])){
                 echo "
                   <div class='alert alert-success alert-dismissible'>
                     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                     <h4><i class='icon fa fa-check'></i> Success!</h4>
                     ".$_SESSION['success']."
                   </div>
                 ";
                 unset($_SESSION['success']);
               }
               ?>
            <!-- <div class="row">
               <div class="col-xs-12">
                  <div class="box box-primary">
                     <div class="box-header">
                        <form method="get" >
                          
                        </form>
                     </div>
                  </div>
               </div>
            </div> -->

            <div class="row">
               <div class="col-xs-12">
                  <div class="box box-solid box-primary">
                     <form  autocomplete="off" enctype="multipart/form-data" name="work_shift" method="POST" action="work_shift_save.php" >

                        <div class="box-header ">
                           <div class="input-group">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <div class="col-sm-2">
                                 <input autocomplete="off" type="text" class="form-control " id="tahun" name="tahun" placeholder="Years" required >
                              </div>
                              <div class="col-sm-2">
                                 <select class="form-control select2" name="location_code" id="location_code" onchange="loadGroup();loadWorkSCH();" required>
                                    <option value="" selected>- Select Location -</option>
                                    <?php
                                       $loc_code =$user['location_code'];
                                       $sql = "SELECT * FROM location  
                                               WHERE loc_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END";
                                       $query = $conn->query($sql);
                                       while($comrow = $query->fetch_assoc()){
                                         echo "
                                           <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                                         ";
                                       }
                                       ?>
                                 </select>
                              </div>
                              <div class="col-sm-3">
                                 <select class="form-control select2" name="group_code" id="group_code"  onchange="loadWorkSCH();" >
                                    <option  value="" selected>- Select -</option>
                                 </select>
                              </div>
                             <!--  <div class="col-sm-2">
                                 <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                              </div> -->
                           </div>
                          <div class="pull-right">
                           <!--  <a href="work_shift_save.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat" method="POST"  role="form" ><i class="fa fa-plus"></i> New</a> -->
                        </div>
                        <div class="box-body">
                           <div class="table-responsive">
                              <table class="table table-bordered table-striped nowrap stripe hover" id="example1">
                                 <!--  <table id="example1" class="table table-bordered table-striped nowrap stripe hover"> -->
                                 <thead>
                                    <!--  <tr style="background-color: #4292EE;color: #fff;"> -->
                                    <th>Month</th>
                                    <!-- tampil berdasar tanggal--> 
                                    <?php 
                                       for($k=1;$k<=31;$k++){
                                         echo '<th>'.$k.' Working Days</th>';
                                       }
                                       ?>          
                                    <!-- </tr> -->
                                 </thead>
                                <tbody id="isitable">  
                                     <?php
                                       if ((@$_GET['tahun']!='') AND (@$_GET['location_code']!='') AND (@$_GET['group_code']!='') ) {
                                         
                                           $thn=$_GET['tahun'];
                                           $tglBaseCal=array();
                                           $calendar="SELECT * FROM calendar WHERE YEAR(holi_date) ='$thn'";
                                           $query = $conn->query($calendar);
                                           while($comrow = $query->fetch_assoc())
                                           {
                                             $ubahformat=$comrow['holi_date'];
                                             $pecahubahformat=explode("-",$ubahformat);
                                             $hari=ltrim($pecahubahformat[2], '0'); 
                                             $bulan=ltrim($pecahubahformat[1], '0'); 
                                             $tglBaseCal[$hari.'_'.$bulan]=$ubahformat;
                                           }
                                       
                                            $WorkParameter=array();
                                            $dat=0;
                                            $loc_code =$_GET['location_code'];
                                            $querSchWork="SELECT id,loc_code,sch_code,sch_name FROM schedules_work 
                                                                       WHERE loc_code='$loc_code'";
                                            $query = $conn->query($querSchWork);                            
                                            while ($data2 = $query->fetch_assoc()){ 
                                                    $WorkParameter[$dat]=array($data2['sch_code'],$data2['sch_name']);
                                                    $dat=$dat+1; 
                                            }
                                            $maxdat=$dat;
                                       
                                             $DetailWorkSCH=array();
                                             $loc_code =$_GET['location_code'];
                                             $thn=$_GET['tahun'];
                                             $group_code=$_GET['group_code'];
                                             $workshift="SELECT loc_code,group_code,sch_id,work_date,DAY(work_date)AS hari,MONTH(work_date)AS bulan FROM working_time
                                                WHERE loc_code='$loc_code' AND group_code ='$group_code' AND YEAR(work_date)  = '$thn'ORDER BY work_date
                                            ";
                                            $queryshift = $conn->query($workshift);
                                           while($DataShift = $queryshift->fetch_assoc())
                                           {
                                              
                                              $ubahformat2=$DataShift['work_date'];
                                              $pecahubahformat2=explode("-",$ubahformat2);
                                              $hari2=ltrim($pecahubahformat2[2], '0'); 
                                              $bulan2=ltrim($pecahubahformat2[1], '0'); 
                                              $DetailWorkSCH[$hari2.'_'.$bulan2]=$DataShift['sch_id'];
                                       
                                       
                                           }
                                       
                                           for ($i=1;$i<=12;$i++){
                                       echo '<tr>';
                                       $tanggal = cal_days_in_month(CAL_GREGORIAN, $i ,$thn );
                                       echo '<td>'.bulan($i).'</td>';
                                       for ($j=1; $j < $tanggal+1; $j++) { 
                                       
                                       
                                       $SabtuMinggu=date("w",mktime(0,0,0,$i,$j,$thn));
                                       $HariSelect=date("Y-m-d",mktime(0,0,0,$i,$j,$thn));
                                       if ($SabtuMinggu==0){ ?>
                                        <td style="background-color: #FF7373;">
                                        <?php echo '<select id="sch'.$i.'_'.$j.'" name="sch'.$i.'_'.$j.'" style="width:100%; appearance:none;-webkit-appearance:none;-moz-appearance:none;border: none;padding: 0.25rem 0.5rem;border-radius: 0.25rem;text-align:center;">';                
                                        echo '<option value=""> </option>';
                                        for($k=0;$k<$maxdat;$k++){
                                          if($WorkParameter[$k][0]==$DetailWorkSCH[$j.'_'.$i]){ ?>
                                            <option selected="selected" value='<?php echo $WorkParameter[$k][0]; ?>'> <?php echo $WorkParameter[$k][0].'&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;'.$WorkParameter[$k][1]; ?> </option>
                                          <?php } else { ?>
                                            <option value='<?php echo $WorkParameter[$k][0]; ?>'> <?php echo $WorkParameter[$k][0].'&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;'.$WorkParameter[$k][1]; ?> </option>
                                          <?php }
                                          ?>
                                          

                                        <?php } ?>
                                    </td>
                                    <?php } else { 
                                       if(@$tglBaseCal[$j.'_'.$i]==$HariSelect){ ?>
                                    <td style="background-color: #FFFF8F;">
                                        <?php echo '<select id="sch'.$i.'_'.$j.'" name="sch'.$i.'_'.$j.'" style="width:100%; appearance:none;-webkit-appearance:none;-moz-appearance:none;border: none;padding: 0.25rem 0.5rem;border-radius: 0.25rem;text-align:center;">';                
                                        echo '<option value=""> </option>';
                                        for($k=0;$k<$maxdat;$k++){
                                          if($WorkParameter[$k][0]==$DetailWorkSCH[$j.'_'.$i]){ ?>
                                            <option selected="selected" value='<?php echo $WorkParameter[$k][0]; ?>'> <?php echo $WorkParameter[$k][0].'&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;'.$WorkParameter[$k][1]; ?> </option>
                                          <?php } else { ?>
                                            <option value='<?php echo $WorkParameter[$k][0]; ?>'> <?php echo $WorkParameter[$k][0].'&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;'.$WorkParameter[$k][1]; ?> </option>
                                          <?php }
                                          ?>
                                          

                                        <?php } ?>
                                    </td>
                                    <?php } else { ?>
                                    <td>
                                        <?php echo '<select id="sch'.$i.'_'.$j.'" name="sch'.$i.'_'.$j.'" style="width:100%; appearance:none;-webkit-appearance:none;-moz-appearance:none;border: none;padding: 0.25rem 0.5rem;border-radius: 0.25rem;text-align:center;">';
                    
                    
                                        echo '<option value=""> </option>';
                                        for($k=0;$k<$maxdat;$k++){
                                          if($WorkParameter[$k][0]==$DetailWorkSCH[$j.'_'.$i]){ ?>
                                            <option selected="selected" value='<?php echo $WorkParameter[$k][0]; ?>'> <?php echo $WorkParameter[$k][0].'&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;'.$WorkParameter[$k][1]; ?> </option>
                                          <?php } else { ?>
                                            <option value='<?php echo $WorkParameter[$k][0]; ?>'> <?php echo $WorkParameter[$k][0].'&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;'.$WorkParameter[$k][1]; ?> </option>
                                          <?php }
                                          ?>
                                          

                                        <?php } ?>
                                    </td> 
                                    <?php }
                                       }
                                       }
                                       echo '</tr>';
                                         }
                                         }
                                      
                                       ?>
                                        <?php 

            
                                    ?> 
                                 </tbody>
                              </table>
                            
                               <!--  <?php  
                                    if(isset($_GET['tahun'])){
                                         @$location_code =$_GET['location_code'];
                                         @$group_code =$_GET['group_code'];
                                         @$tahun =$_GET['tahun'];
                                        }else{
                                          $location_code='';
                                          $group_code ='';
                                          $tahun  ='';
                                        }
                                              // echo "<a href='work_shift_save.php?tahun=".$tahun."&location_code=".$location_code."&group_code=".$group_code."' type ='submit' name ='add' data-toggle='form' class='btn btn-primary btn-flat'><i class='fa fa-save'></i> Save</a>" 
                                              // echo "<button type='submit' class='btn btn-primary btn-flat' name='add'><i class='fa fa-save'></i> Save</button> "
                                             ?>    -->
                            
                            


                           </div>
                            <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <?php include 'includes/footer.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
  
</body>
</html>