<?php
	include 'includes/session.php';
	require_once '../PHPExcel/Classes/PHPExcel.php';
	// Panggil class PHPExcel nya
	$excel = new PHPExcel();
	// Settingan awal file excel
	$excel->getProperties()->setCreator('Administrator')
             ->setLastModifiedBy('')
             ->setTitle("Attendance Detail")
             ->setSubject("Attendance ")
             ->setDescription("Attendance Detail")
             ->setKeywords("Attendance");
    //Style kolom
    $style_col = array(
		'font' => array('bold' => true), // Set font nya jadi bold
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Style row
	$style_row = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Set header
	$excel->setActiveSheetIndex(0)->setCellValue('A1', "ATTENDANCE DETAIL");
	$excel->getActiveSheet()->mergeCells('A1:O1'); // Set Merge Cell pada kolom A1 sampai F1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// Buat header tabel nya pada baris ke 3
	$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('B3', "COMPANY");
	$excel->setActiveSheetIndex(0)->setCellValue('C3', "DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('D3', "DAYS");
	$excel->setActiveSheetIndex(0)->setCellValue('E3', "NIK");
	$excel->setActiveSheetIndex(0)->setCellValue('F3', "NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('G3', "DEPARTMENT");
	$excel->setActiveSheetIndex(0)->setCellValue('H3', "LOCATION");
	$excel->setActiveSheetIndex(0)->setCellValue('I3', "TIME IN");
	$excel->setActiveSheetIndex(0)->setCellValue('J3', "TIME OUT");
	$excel->setActiveSheetIndex(0)->setCellValue('K3', "WORK HOUR");
	$excel->setActiveSheetIndex(0)->setCellValue('L3', "WORK MINUTE");
	$excel->setActiveSheetIndex(0)->setCellValue('M3', "LATE MINUTE");
	$excel->setActiveSheetIndex(0)->setCellValue('N3', "OVERTIME");
	$excel->setActiveSheetIndex(0)->setCellValue('O3', "NOTES");

	// Apply style header yang telah kita buat tadi ke masing-masing kolom header
	$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
	// Set height baris ke 1, 2 dan 3
	$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(30);
	$nik =$_GET['nik'];
	$start_date = $_GET['start_date'];
    $end_date = $_GET['end_date'];
    $loc_code =$_GET['location_code'];
    $department_code =$_GET['department_code'];
    $location_code =$user['location_code'];

    if ($loc_code=='')
    {
    	$sql = mysqli_query($conn,"SELECT a.company_code, 
									a.att_date,
									DAYNAME(a.att_date) AS Days,
									a.nik,
									b.name,
									b.department_code,
									b.department_name,
									b.location_code,
									b.loc_name,
									IFNULL(a.time_in,'') AS time_in,
									IFNULL(a.time_out,'') AS time_out,
									a.work_hour,
									a.work_minute,
									a.late_minute,
									a.overtime,
									a.notes
    						FROM attendance_new a
                            LEFT JOIN employee_view b ON a.company_code = b.company_code AND a.nik = b.nik
                            where a.att_date between '$start_date' and '$end_date'
                            and b.location_code LIKE CASE '$location_code' WHEN '' THEN '%%' ELSE '$location_code' END
                            and b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                             AND b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                              AND b.status ='Active'
                            ORDER BY a.att_date desc");
    }else
    {
    	$sql = mysqli_query($conn,"SELECT
    								a.company_code, 
									a.att_date,
									DAYNAME(a.att_date) AS Days,
									a.nik,
									b.name,
									b.department_code,
									b.department_name,
									b.location_code,
									b.loc_name,
									IFNULL(a.time_in,'') AS time_in,
									IFNULL(a.time_out,'') AS time_out,
									a.work_hour,
									a.work_minute,
									a.late_minute,
									a.overtime,
									a.notes
    						FROM attendance_new a
                            LEFT JOIN employee_view b ON a.company_code = b.company_code AND a.nik = b.nik
                            where a.att_date between '$start_date' and '$end_date'
                            and b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END
                            and b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                             AND b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                              AND b.status ='Active'
                            ORDER BY a.att_date desc");
    }

	
	
	$no = 0;
	$numrow = 3;
	while ($row = mysqli_fetch_array($sql)) {
		$no++;
		$numrow++;
		$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $row['company_code']);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('C'.$numrow, $row['att_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('D'.$numrow, $row['Days'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, $row['nik'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, $row['name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('G'.$numrow, $row['department_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('H'.$numrow, $row['loc_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('I'.$numrow, $row['time_in'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('J'.$numrow, $row['time_out'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('K'.$numrow, $row['work_hour'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('L'.$numrow, $row['work_minute'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('M'.$numrow, $row['late_minute'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('N'.$numrow, $row['overtime'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('O'.$numrow, $row['notes'], PHPExcel_Cell_DataType::TYPE_STRING);
		// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		
	}
	// Set width kolom
	$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
	$excel->getActiveSheet()->getColumnDimension('B')->setWidth(13); // Set width kolom B
	$excel->getActiveSheet()->getColumnDimension('C')->setWidth(13); // Set width kolom C
	$excel->getActiveSheet()->getColumnDimension('D')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('E')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('F')->setWidth(25); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('G')->setWidth(25); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('H')->setWidth(15); 
	$excel->getActiveSheet()->getColumnDimension('I')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('J')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('K')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('L')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('M')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('N')->setWidth(13); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('O')->setWidth(30); // Set width kolom D
	// Set orientasi kertas jadi LANDSCAPE
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	// Set judul file excel nya
	$excel->getActiveSheet(0)->setTitle("Attendance Detail");
	$excel->setActiveSheetIndex(0);
	// Proses file excel
	// $objWriter = new PHPExcel_Writer_Excel2007($excel); 
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment; filename="Attendance_Detail'.$_GET['start_date'].'_'.$_GET['end_date'].'.xlsx"'); // Set nama file excel nya
	header('Cache-Control: max-age=0');
	$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	// $write = PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	$write->save('php://output');
	exit;
?>