<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['approval_setup_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM approval_setup WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Employee List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Employees</li>
         <li class="active">Employee List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="approval_setup_update.php?id=<?php echo $row['id']; ?>">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Approval Setup Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <label for="company_code" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="company_code" id="company_code" disabled="true" required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM company";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['company_code']; ?>" <?php if(($k['company_code'])== ($row['company_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['company_code']==$k['company_code'])?print(" "):print(""); ?>  > <?php echo $k['company_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="nik" class="col-sm-2 control-label">NIK</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="nik" name="nik" value="<?php echo $row['nik'];?>" disabled ='true' required>
                  </div>
               </div>
               <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="name" name="name"  value="<?php echo $row['name'];?>" disabled = 'true' required>
                  </div>
               </div>
               <div class="form-group">
                  <label for="division_code" class="col-sm-2 control-label">Division<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="division_code" id="division_code" onchange="loadDepartment();"  required>

                           <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM division";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['division_code']; ?>" <?php if(($k['division_code'])== ($row['division_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['division_code']==$k['division_code'])?print(" "):print(""); ?>  > <?php echo $k['division_name'];?>
                           </option> <?php   } ?>

                     </select>
                  </div>
               </div>
                <div class="form-group">
               <label for="department_code" class="col-sm-2 control-label">Department<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="department_code" id="department_code" required>

                           <option value=" ">- Select -</option>
                           <?php $q = "SELECT division_code,dept_code,name FROM department";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['dept_code']; ?>" <?php if(($k['dept_code'])== ($row['department_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['dept_code']==$k['dept_code'])?print(" "):print(""); ?>  > <?php echo $k['name'];?>
                           </option> <?php   } ?>

                     </select>
                  </div>
               </div>
                <div class="form-group">
                  <label for="position_code" class="col-sm-2 control-label">Position<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="position_code" id="position_code" required>
                       <!--  <option value="" selected>- Select -</option> -->
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM position";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['position_code']; ?>" <?php if(($k['position_code'])== ($row['position_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['position_code']==$k['position_code'])?print(" "):print(""); ?>  > <?php echo $k['description'];?>
                           </option> <?php   } ?>

                     </select>
                  </div>
               </div>
               <div class="form-group">
               <label for="job_code" class="col-sm-2 control-label">Jobs<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="job_code" id="job_code" required>
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM jobs";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['job_code']; ?>" <?php if(($k['job_code'])== ($row['job_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['job_code']==$k['job_code'])?print(" "):print(""); ?>  > <?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>





               <div class="form-group">
                  <label for="approval_level_1" class="col-sm-2 control-label">Approval Level 1</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="approval_level_1" id="approval_level_1"  >
                        <option value="">- Select -</option>
                           <?php $q = "SELECT * FROM employee_view where STATUS ='active'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['nik']; ?>" <?php if(($k['nik'])== ($row['approval_level_1']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['nik']==$k['nik'])?print(" "):print(""); ?>  >  <?php echo $k['nik'];?> - <?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              <div class="form-group">
                  <label for="approval_level_2" class="col-sm-2 control-label">Approval Level 2</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="approval_level_2" id="approval_level_2"  >
                        <option value="">- Select -</option>
                           <?php $q = "SELECT * FROM employee_view where STATUS ='active'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['nik']; ?>" <?php if(($k['nik'])== ($row['approval_level_2']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['nik']==$k['nik'])?print(" "):print(""); ?>  >  <?php echo $k['nik'];?> - <?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>

              <div>
         </div>
        
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit" href="javascript:history.back()" ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Kembali</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/employee_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>