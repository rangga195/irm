<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
  <script src="../aset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['employee_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM employee_view WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Employee List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Employees</li>
         <li class="active">Employee List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="employee_update.php?id=<?php echo $row['id']; ?>" enctype="multipart/form-data" >
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Employee Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <label for="company_code" class="col-sm-2 control-label">Company<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="company_code" id="company_code"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM company";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['company_code']; ?>" <?php if(($k['company_code'])== ($row['company_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['company_code']==$k['company_code'])?print(" "):print(""); ?>  > <?php echo $k['company_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
                  <label for="nik" class="col-sm-2 control-label">NIK<label class="text-danger">*</label></label>
                  <div class="col-sm-3 ">
                     <input type="text" class="form-control" id="nik" name="nik" value="<?php echo $row['nik'];?>" required>
                  </div>
               </div>
             
               <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="name" name="name"  value="<?php echo $row['name'];?>"required>
                  </div>
                   <label for="nick_name" class="col-sm-2 control-label">Nick Name</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="nick_name" name="nick_name" value="<?php echo $row['nick_name'];?>">
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="division_code" class="col-sm-2 control-label">Division<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="division_code" id="division_code" onchange="loadDepartment();"  required>

                           <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM division";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['division_code']; ?>" <?php if(($k['division_code'])== ($row['division_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['division_code']==$k['division_code'])?print(" "):print(""); ?>  > <?php echo $k['division_name'];?>
                           </option> <?php   } ?>

                     </select>
                  </div>
                  <label for="department_code" class="col-sm-2 control-label">Department<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="department_code" id="department_code" required>

                           <option value=" ">- Select -</option>
                           <?php $q = "SELECT division_code,dept_code,name FROM department";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['dept_code']; ?>" <?php if(($k['dept_code'])== ($row['department_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['dept_code']==$k['dept_code'])?print(" "):print(""); ?>  > <?php echo $k['name'];?>
                           </option> <?php   } ?>

                     </select>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="position_code" class="col-sm-2 control-label">Position<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="position_code" id="position_code" required>
                       <!--  <option value="" selected>- Select -</option> -->
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM position";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['position_code']; ?>" <?php if(($k['position_code'])== ($row['position_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['position_code']==$k['position_code'])?print(" "):print(""); ?>  > <?php echo $k['description'];?>
                           </option> <?php   } ?>

                     </select>
                  </div>
                  <label for="job_code" class="col-sm-2 control-label">Jobs<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="job_code" id="job_code" required>
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM jobs";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['job_code']; ?>" <?php if(($k['job_code'])== ($row['job_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['job_code']==$k['job_code'])?print(" "):print(""); ?>  > <?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="parent_position" class="col-sm-2 control-label">Parent Position</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="parent_position" name="parent_position"  value="<?php echo $row['parent_position'];?>" >
                  </div>
                  <label for="location_code" class="col-sm-2 control-label">Location<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="location_code" id="location_code" onchange="loadGroup();" required>
                       <!--  <option value="" selected>- Select -</option> -->
                         <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM location";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['loc_code']; ?>" <?php if(($k['loc_code'])== ($row['location_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['loc_code']==$k['loc_code'])?print(" "):print(""); ?>  > <?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="group_code" class="col-sm-2 control-label">Working Group</label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="group_code" id="group_code" >
                       <!--  <option value="" selected>- Select -</option> -->
                         <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM schedules_group";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['group_code']; ?>" <?php if(($k['group_code'])== ($row['group_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['group_code']==$k['group_code'])?print(" "):print(""); ?>  > <?php echo $k['group_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
                   <label for="gender" class="col-sm-2 control-label">Gender<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="gender" id="gender" required>
                       <!--  <option value="" selected>- Select -</option> -->
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='GENDER'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['gender']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status Employee<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="status" id="status" required>
                       <!--  <option value="" selected>- Select -</option> -->
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='STATUS'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['status']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
                  <label for="status_work" class="col-sm-2 control-label">Status Working<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="status_work" id="status_work" required>
                     <!-- <option value="" selected>- Select -</option> -->
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='STATUS WK'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['status_work']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="email_office" class="col-sm-2 control-label">Email Office</label>
                  <div class="col-sm-3">
                     <input type="email" class="form-control " id="email_office" name="email_office" value="<?php echo $row['email_office'];?>" >
                  </div>
                  <label for="email_personal" class="col-sm-2 control-label">Email Personal</label>
                  <div class="col-sm-3">
                     <input type="email" class="form-control " id="email_personal" name="email_personal" value="<?php echo $row['email_personal'];?>" >
                  </div>
               </div>
               <div class="form-group">
                  <label for="entry_date" class="col-sm-2 control-label">Join Date<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <div class="date">
                        <input type="text" class="form-control" id="entry_date" name="entry_date" value="<?php echo $row['entry_date'];?>">
                     </div>
                  </div>
                    <label for="resign_date" class="col-sm-2 control-label">Resign Date</label>
                  <div class="col-sm-3">
                     <div class="date">
                        <input type="text" class="form-control " id="resign_date" name="resign_date" value="<?php echo $row['resign_date'];?>">
                     </div>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="bpjs_tk_no" class="col-sm-2 control-label">BPJS TK No.</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="bpjs_tk_no" name="bpjs_tk_no" value="<?php echo $row['bpjs_tk_no'];?>" >
                  </div>
                  <label for="bpjs_tk_date" class="col-sm-2 control-label">BPJS TK Date</label>
                  <div class="col-sm-3">
                     <div class="date">
                        <input type="text" class="form-control" id="bpjs_tk_date" name="bpjs_tk_date" value="<?php echo $row['bpjs_tk_date'];?>"  >
                     </div>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="bpjs_ks_no" class="col-sm-2 control-label">BPJS KS No.</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="bpjs_ks_no" name="bpjs_ks_no" value="<?php echo $row['bpjs_ks_no'];?>"   >
                  </div>
                  <label for="bpjs_ks_date" class="col-sm-2 control-label">BPJS KS Date</label>
                  <div class="col-sm-3">
                     <div class="date">
                        <input type="text" class="form-control " id="bpjs_ks_date" name="bpjs_ks_date" value="<?php echo $row['bpjs_ks_date'];?>"  >
                     </div>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="npwp_no" class="col-sm-2 control-label">NPWP No.</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="npwp_no" name="npwp_no" data-inputmask='"mask": "99.999.999.9-999.999"' data-mask value="<?php echo $row['npwp_no'];?>"  >
                  </div>
                   <label for="ptkp_status" class="col-sm-2 control-label">PTKP Status</label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="ptkp_status" id="ptkp_status" >
                      <!--   <option value="" selected>- Select -</option> -->
                         <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='PTKP'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['ptkp_status']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               
            </div>
         </div>
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="glyphicon glyphicon-briefcase"></i> Other Employee Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <label for="ktp_no" class="col-sm-2 control-label">KTP No.<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="ktp_no" name="ktp_no"  value="<?php echo $row['ktp_no'];?>" required>
                  </div>
                   <label for="kk_no" class="col-sm-2 control-label">KK No.<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="kk_no" name="kk_no"  value="<?php echo $row['kk_no'];?>" >
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="sim_no" class="col-sm-2 control-label">SIM No.</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control" id="sim_no" name="sim_no" value="<?php echo $row['sim_no'];?>"  >
                  </div>
               </div>
               <div class="form-group">
                  <label for="birth_place" class="col-sm-2 control-label">Birth Place<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="birth_place" name="birth_place" value="<?php echo $row['birth_place'];?>"  required>
                  </div>
                  <label for="birth_date" class="col-sm-2 control-label">Birth Date<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <div class="date">
                        <input type="text" class="form-control" id="birth_date" name="birth_date" value="<?php echo $row['birth_date'];?>"  >
                     </div>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="age" class="col-sm-2 control-label">Age</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control" id="age" name="age"  value="<?php echo $row['age'];?>"  >
                  </div>
                  <label for="height" class="col-sm-2 control-label">Height</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control" id="height" name="height"  value="<?php echo $row['height'];?>"  >
                  </div>
               </div>
             
               <div class="form-group">
                  <label for="weight" class="col-sm-2 control-label">Weight</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="weight" name="weight"  value="<?php echo $row['weight'];?>"   >
                  </div>
                  <label for="size" class="col-sm-2 control-label">Size</label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="size" id="size" >
                      <!--   <option value="" selected>- Select -</option> -->
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SIZE'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['size']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="shoe_size" class="col-sm-2 control-label">Shoe Size</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="shoe_size" name="shoe_size" value="<?php echo $row['shoe_size'];?>"   >
                  </div>
                  <label for="religion" class="col-sm-2 control-label">Religion<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="religion" id="religion" >
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELIGION'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['religion']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="phone_no" class="col-sm-2 control-label">Phone No.<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="phone_no" name="phone_no" value="<?php echo $row['phone_no'];?>"  >
                  </div>
                   <label for="phone_no2" class="col-sm-2 control-label">Phone No. 2</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="phone_no2" name="phone_no2" value="<?php echo $row['phone_no2'];?>"  >
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="phone_home" class="col-sm-2 control-label">Phone Home</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="phone_home" name="phone_home" value="<?php echo $row['phone_home'];?>"   >
                  </div>
                  <label for="blood_type" class="col-sm-2 control-label">Blood Type</label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="blood_type" id="blood_type" >
                        <!-- <option value="" selected>- Select -</option> -->
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='BLOOD'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['blood_type']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              
              
               <div class="form-group">
                  <label for="marital_status" class="col-sm-2 control-label">Marital Status<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="marital_status" id="marital_status" >
                        <!-- <option value="" selected>- Select -</option> -->
                         <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='MARITAL'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['marital_status']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
                  <label for="partner_name" class="col-sm-2 control-label">Husband/Wife Name</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="partner_name" name="partner_name"  value="<?php echo $row['partner_name'];?>" >
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="phone_no_partner" class="col-sm-2 control-label">Phone No. Husband/Wife</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="phone_no_partner" name="phone_no_partner"  value="<?php echo $row['phone_no_partner'];?>"  >
                  </div>
                  <label for="child" class="col-sm-2 control-label">Child</label>
                  <div class="col-sm-3">
                     <input type="number" class="form-control " id="child" name="child" value="<?php echo $row['child'];?>"  >
                  </div>
               </div>

                <div class="form-group">
                  <label for="father_name" class="col-sm-2 control-label">Father Name</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="father_name" name="father_name" value="<?php echo $row['father_name'];?>"   >
                  </div>
                  <label for="phone_no_father" class="col-sm-2 control-label">Phone No. Father</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="phone_no_father" name="phone_no_father" value="<?php echo $row['phone_no_father'];?>"   >
                  </div>
               </div>
                <div class="form-group">
                  <label for="mother_name" class="col-sm-2 control-label">Mother Name<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="mother_name" name="mother_name"  value="<?php echo $row['mother_name'];?>" required>
                  </div>
                   <label for="phone_no_mother" class="col-sm-2 control-label">Phone No. Mother</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="phone_no_mother" name="phone_no_mother" value="<?php echo $row['phone_no_mother'];?>" >
                  </div>
               </div>
              <div class="form-group">
                  <label for="near_family" class="col-sm-2 control-label">Family Name<label class="text-danger">*</label></label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="near_family" name="near_family"  value="<?php echo $row['near_family'];?>"  required>
                  </div>
                   <label for="phone_no_family" class="col-sm-2 control-label">Family Phone No.</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="phone_no_family" name="phone_no_family"  value="<?php echo $row['phone_no_family'];?>" >
                  </div>
               </div>
               <div class="form-group">
                  <label for="address_family" class="col-sm-2 control-label">Family Address</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control " id="address_family" name="address_family"  value="<?php echo $row['address_family'];?>" >
                  </div>
               </div>
               <div class="form-group">
                  <label for="hobby" class="col-sm-2 control-label">Hobby</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="hobby" name="hobby"  value="<?php echo $row['hobby'];?>"  >
                  </div>
                  <label for="language_skill" class="col-sm-2 control-label">Language Skill</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control " id="language_skill" name="language_skill"  value="<?php echo $row['language_skill'];?>"  >
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="vehicle_status" class="col-sm-2 control-label">Vehicle Status</label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="vehicle_status" id="vehicle_status" >
                       <!--  <option value="" selected>- Select -</option> -->
                         <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='VEHICLE'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['vehicle_status']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
                  <label for="vehicle_type" class="col-sm-2 control-label">Vehicle Type</label>
                  <div class="col-sm-3">
                     <select class="form-control select2" name="vehicle_type" id="vehicle_type" >
                        <!-- <option value="" selected>- Select -</option> -->
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='VEHI TYPE'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['vehicle_type']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_code'];?>
                        </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               
            </div>
         </div>
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="glyphicon glyphicon-briefcase"></i> Photo Employee & Other Image 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              
               <div class="form-group">
                  <label for="emp_photo" class="col-sm-2 control-label">Photo</label>
                  <div class="col-sm-9">
                     <figure>
                        <a data-toggle="modal" data-target="#popemp_photo">
                        <img src="<?php echo (!empty($row['emp_photo']))? '../emp_photo/'.$row['emp_photo']:'../emp_photo/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_photo" name="emp_photo" value="<?php echo $row['emp_photo'];?>" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>
                <!-- modal Photo -->

               <div class="modal fade" id="popemp_photo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_photo">Photo Profile <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_photo']))? '../emp_photo/'.$row['emp_photo']:'../emp_photo/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>




               <div class="form-group">
                  <label for="emp_kk" class="col-sm-2 control-label">KK</label>
                  <div class="col-sm-9">
                     <figure>
                        <a data-toggle="modal" data-target="#popemp_kk">
                        <img src="<?php echo (!empty($row['emp_kk']))? '../emp_kk/'.$row['emp_kk']:'../emp_kk/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_kk" name="emp_kk" value="<?php echo $row['emp_kk'];?>" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>

                <!-- modal kk -->
               <div class="modal fade" id="popemp_kk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_kk">KK <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_kk']))? '../emp_kk/'.$row['emp_kk']:'../emp_kk/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>



               <div class="form-group">
                  <label for="emp_ktp" class="col-sm-2 control-label">KTP</label>
                  <div class="col-sm-9">
                     <figure>
                        <a data-toggle="modal" data-target="#popemp_ktp">
                        <img src="<?php echo (!empty($row['emp_ktp']))? '../emp_ktp/'.$row['emp_ktp']:'../emp_ktp/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_ktp" name="emp_ktp" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>


                <!-- modal ktp -->
               <div class="modal fade" id="popemp_ktp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_ktp">KTP <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_ktp']))? '../emp_ktp/'.$row['emp_ktp']:'../emp_ktp/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>


               <div class="form-group">
                  <label for="emp_npwp" class="col-sm-2 control-label">NPWP</label>
                  <div class="col-sm-9">
                     <figure>
                        <a data-toggle="modal" data-target="#popemp_npwp">
                        <img src="<?php echo (!empty($row['emp_npwp']))? '../emp_npwp/'.$row['emp_npwp']:'../emp_npwp/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_npwp" name="emp_npwp" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>
                <!-- modal npwp -->
               <div class="modal fade" id="popemp_npwp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_npwp">NPWP <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_npwp']))? '../emp_npwp/'.$row['emp_npwp']:'../emp_npwp/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>



               <div class="form-group">
                  <label for="emp_sima" class="col-sm-2 control-label">SIM A</label>
                  <div class="col-sm-9">
                      <figure>
                        <a data-toggle="modal" data-target="#popemp_sima">
                        <img src="<?php echo (!empty($row['emp_sima']))? '../emp_sima/'.$row['emp_sima']:'../emp_sima/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_sima" name="emp_sima" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>

                <!-- modal SIMA -->
               <div class="modal fade" id="popemp_sima" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_sima">SIM A <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_sima']))? '../emp_sima/'.$row['emp_sima']:'../emp_sima/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>

               <div class="form-group">
                  <label for="emp_simc" class="col-sm-2 control-label">SIM C</label>
                  <div class="col-sm-9">
                      <figure>
                        <a data-toggle="modal" data-target="#popemp_simc">
                        <img src="<?php echo (!empty($row['emp_simc']))? '../emp_simc/'.$row['emp_simc']:'../emp_simc/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_simc" name="emp_simc" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>
                 <!-- modal SIMA -->
               <div class="modal fade" id="popemp_simc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_simc">SIM C <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_simc']))? '../emp_simc/'.$row['emp_simc']:'../emp_simc/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>


               <div class="form-group">
                  <label for="emp_siml" class="col-sm-2 control-label">SIM Others</label>
                  <div class="col-sm-9">
                     <figure>
                        <a data-toggle="modal" data-target="#popemp_siml">
                        <img src="<?php echo (!empty($row['emp_siml']))? '../emp_siml/'.$row['emp_siml']:'../emp_siml/profile.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                     <input type="file" id="emp_siml" name="emp_siml" accept="image/gif,image/jpeg,image/jpg,image/png,">
                  </div>
               </div>
                 <!-- modal SIML -->
               <div class="modal fade" id="popemp_siml" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_siml">SIM L <?php echo $row['name'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['emp_siml']))? '../emp_siml/'.$row['emp_siml']:'../emp_siml/profile.jpg'; ?>" height='50%' width='50%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>

            </div>
         </div>
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="glyphicon glyphicon-briefcase"></i> Address Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="address" name="address"   value="<?php echo $row['address'];?>"  required>
                  </div>
               </div>

               <div class="form-group">
                  <label for="provinsi" class="col-sm-2 control-label">Province<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="provinsi" id="provinsi" onchange='loadKab();loadKec();loadKel();loadKodepos();' required>
                        <!-- <option value="" selected>- Select -</option> -->
                       <option value=" ">- Select -</option>
                           <?php $q = "SELECT provinsi FROM postcode
                                          GROUP BY provinsi";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['provinsi']; ?>" <?php if(($k['provinsi'])== ($row['provinsi']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['provinsi']==$k['provinsi'])?print(" "):print(""); ?>  > <?php echo $k['provinsi'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="kabupaten_kota" class="col-sm-2 control-label">District<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="kabupaten_kota" id="kabupaten_kota" onchange='loadKec();loadKel();loadKodepos();' required>
                        <option value="<?php echo $row['kabupaten_kota'];?>" selected><?php echo $row['kabupaten_kota'];?></option>
                       <!--  <option value=" ">- Select -</option>
                           <?php $q = "SELECT kabupaten_kota FROM postcode
                                          GROUP BY kabupaten_kota";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kabupaten_kota']; ?>" <?php if(($k['kabupaten_kota'])== ($row['kabupaten_kota']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kabupaten_kota']==$k['kabupaten_kota'])?print(" "):print(""); ?>  > <?php echo $k['kabupaten_kota'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="kecamatan" class="col-sm-2 control-label">Sub -District<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="kecamatan" id="kecamatan" onchange='loadKel();loadKodepos();' required>
                      <option value="<?php echo $row['kecamatan'];?>" selected><?php echo $row['kecamatan'];?></option>
                       <!--  <option value=" ">- Select -</option>
                           <?php $q = "SELECT kecamatan FROM postcode
                                          GROUP BY kecamatan";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kecamatan']; ?>" <?php if(($k['kecamatan'])== ($row['kecamatan']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kecamatan']==$k['kecamatan'])?print(" "):print(""); ?>  > <?php echo $k['kecamatan'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>

               <div class="form-group">
                  <label for="kelurahan" class="col-sm-2 control-label">Region<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="kelurahan" id="kelurahan"  onchange='loadKodepos();'required>
                        <option value="<?php echo $row['kelurahan'];?>" selected><?php echo $row['kelurahan'];?></option>
                        <!--  <option value=" ">- Select -</option>
                           <?php $q = "SELECT kelurahan FROM postcode
                                          GROUP BY kelurahan";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kelurahan']; ?>" <?php if(($k['kelurahan'])== ($row['kelurahan']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kelurahan']==$k['kelurahan'])?print(" "):print(""); ?>  > <?php echo $k['kelurahan'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
               
               
               
               <div class="form-group">
                  <label for="post_code" class="col-sm-2 control-label">Post Code<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="post_code" id="post_code" required>
                        <option value="<?php echo $row['post_code'];?>" selected><?php echo $row['post_code'];?></option>
                        <!-- <option value=" ">- Select -</option>
                           <?php $q = "SELECT kodepos FROM postcode
                                          GROUP BY kodepos";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kodepos']; ?>" <?php if(($k['kodepos'])== ($row['post_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kodepos']==$k['kodepos'])?print(" "):print(""); ?>  > <?php echo $k['kodepos'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="glyphicon glyphicon-briefcase"></i> Address Domicile Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <label for="address2" class="col-sm-2 control-label">Address Domicile<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="address2" name="address2" value="<?php echo $row['address2'];?>"  required>
                  </div>
               </div>
               <div class="form-group">
                  <label for="provinsi2" class="col-sm-2 control-label">Province Domicile<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="provinsi2" id="provinsi2" onchange='loadKabDom();loadKecDom();loadKelDom();loadKodeposDom();' required>
                        <!-- <option value="" selected>- Select -</option> -->
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT provinsi FROM postcode
                                          GROUP BY provinsi";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['provinsi']; ?>" <?php if(($k['provinsi'])== ($row['provinsi2']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['provinsi']==$k['provinsi'])?print(" "):print(""); ?>  > <?php echo $k['provinsi'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
                <div class="form-group">
                  <label for="kabupaten_kota2" class="col-sm-2 control-label">District Domicile<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="kabupaten_kota2" id="kabupaten_kota2"  onchange='loadKecDom();'required>
                       <option value="<?php echo $row['kabupaten_kota2'];?>" selected><?php echo $row['kabupaten_kota2'];?></option>
                        <!--  <option value=" ">- Select -</option>
                           <?php $q = "SELECT kabupaten_kota FROM postcode
                                          GROUP BY kabupaten_kota";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kabupaten_kota']; ?>" <?php if(($k['kabupaten_kota'])== ($row['kabupaten_kota']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kabupaten_kota']==$k['kabupaten_kota'])?print(" "):print(""); ?>  > <?php echo $k['kabupaten_kota'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="kecamatan2" class="col-sm-2 control-label">Sub -District Domicile<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="kecamatan2" id="kecamatan2" onchange='loadKelDom();' required>
                          <option value="<?php echo $row['kecamatan2'];?>" selected><?php echo $row['kecamatan2'];?></option>
                       <!--  <option value=" ">- Select -</option>
                           <?php $q = "SELECT kecamatan FROM postcode
                                          GROUP BY kecamatan";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kecamatan']; ?>" <?php if(($k['kecamatan'])== ($row['kecamatan2']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kecamatan']==$k['kecamatan'])?print(" "):print(""); ?>  > <?php echo $k['kecamatan'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="kelurahan2" class="col-sm-2 control-label">Region Domicile<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="kelurahan2" id="kelurahan2" onchange='loadKodeposDom();'   required>
                        <option value="<?php echo $row['kelurahan2'];?>" selected><?php echo $row['kelurahan2'];?></option>
                         <!-- <option value=" ">- Select -</option>
                           <?php $q = "SELECT kelurahan FROM postcode
                                          GROUP BY kelurahan";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kelurahan']; ?>" <?php if(($k['kelurahan'])== ($row['kelurahan2']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kelurahan']==$k['kelurahan'])?print(" "):print(""); ?>  > <?php echo $k['kelurahan'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="post_code2" class="col-sm-2 control-label">Post Code Domicile<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <select class="form-control  select2" name="post_code2" id="post_code2" required>
                       <option value="<?php echo $row['post_code2'];?>" selected><?php echo $row['post_code2'];?></option>
                      <!--  <option value=" ">- Select -</option>
                           <?php $q = "SELECT kodepos FROM postcode
                                          GROUP BY kodepos";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['kodepos']; ?>" <?php if(($k['kodepos'])== ($row['post_code2']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['kodepos']==$k['kodepos'])?print(" "):print(""); ?>  > <?php echo $k['kodepos'];?>
                           </option> <?php   } ?> -->
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <!-- <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="glyphicon glyphicon-briefcase"></i> Family Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <label for="near_family" class="col-sm-2 control-label">Family Name<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="near_family" name="near_family"  value="<?php echo $row['near_family'];?>"  required>
                  </div>
               </div>
               <div class="form-group">
                  <label for="phone_no_family" class="col-sm-2 control-label">Family Phone No.</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="phone_no_family" name="phone_no_family"  value="<?php echo $row['phone_no_family'];?>" >
                  </div>
               </div>
               <div class="form-group">
                  <label for="address_family" class="col-sm-2 control-label">Family Address</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="address_family" name="address_family"  value="<?php echo $row['address_family'];?>" >
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="father_name" class="col-sm-2 control-label">Father Name</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="father_name" name="father_name" value="<?php echo $row['father_name'];?>"   >
                  </div>
               </div>
               <div class="form-group">
                  <label for="phone_no_father" class="col-sm-2 control-label">Phone No. Father</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="phone_no_father" name="phone_no_father" value="<?php echo $row['phone_no_father'];?>"   >
                  </div>
               </div>
               <div class="form-group">
                  <label for="mother_name" class="col-sm-2 control-label">Mother Name<label class="text-danger">*</label></label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control " id="mother_name" name="mother_name"  value="<?php echo $row['mother_name'];?>" required>
                  </div>
               </div>
               <div class="form-group">
                  <label for="phone_no_mother" class="col-sm-2 control-label">Phone No. Mother</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="phone_no_mother" name="phone_no_mother" value="<?php echo $row['phone_no_mother'];?>" >
                  </div>
               </div>
            </div>
         </div> -->
            <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Experience Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     
                  <?php
                     $nik =$row['nik'];
                     $data="SELECT * FROM employee_experience WHERE nik ='$nik' ORDER BY join_year desc";
                     $query = $conn->query($data);
                     while($edit = $query->fetch_assoc())
                     {
                       ?>

                        <div class="form-group">
                        <label class="col-sm-2 control-label">Job Experience</label>
                        <div>
                           <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Jobs Position" placeholder="Jobs Position"   name="job[]"  value="<?php echo $edit['job']; ?>"  >
                           </div>
                           <div class="col-sm-3">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Company" placeholder="Company"   name="company[]" value="<?php echo $edit['company']; ?>">
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearin[]" value="<?php echo $edit['join_year']; ?>">

                              </div>
                              <!-- /.input group -->
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearout[]" value="<?php echo $edit['resign_year']; ?>">
                              </div>
                              <!-- /.input group -->
                           </div>
                        </div>
                        <a class="hapus-box btn btn-danger btn-sm btn-flat"   data-toggle="tooltip" title="Hapus Data" href="employee_exp_delete.php?id=<?php echo $edit['id']; ?>"  alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA <?php echo $edit['job']; ?> ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                        </div>

                      <?php
                    }
                  ?> 
                  <div class="form-group">
                  <div id="main">
                      <div class="my-form">
                              <center>
                           <p class="text-box">
                                  <a class="add-box btn btn-primary btn-flat" href="#Add" id > <i class="fa fa-plus"> </i> Add More</a>
                              </p></center>
                      </div>
                  </div>
                  <script type="text/javascript">
                  jQuery(document).ready(function($){
                      $('.my-form .add-box').click(function(){
                          var n = $('.text-box').length + 1;
                          if( 5 < n ) {
                              alert('Stop it!');
                              return false;
                          }
                          var box_html = $('<div class="form-group"><label class="col-sm-2 control-label">Job Experience</label><div><div class="col-sm-2"> <input type="text" class="form-control"  data-toggle="tooltip" title="Jobs Position" placeholder="Jobs Position"   name="add_job[]"   ></div><div class="col-sm-3"><input type="text" class="form-control"  data-toggle="tooltip" title="Company" placeholder="Company"   name="add_company[]" ></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearin[]"></div></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearout[]" ></div></div></div><a href="#" class="remove-box btn btn-danger btn-sm "><i class="fa fa-remove"> </i></a></div>');
                          box_html.hide();
                          $('.my-form p.text-box:last').after(box_html);
                          box_html.fadeIn('slow');
                          return false;
                      });
                      $('.my-form').on('click', '.remove-box', function(){
                          $(this).parent().css( 'background-color', '#' );
                          $(this).parent().fadeOut("slow", function() {
                              $(this).remove();
                              $('.box-number').each(function(index){
                                  $(this).text( index + 1 );
                              });
                          });
                          return false;
                      });
                  });
                  </script>
         </div>
      </div>
   </div>
         <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Education Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     
                  <?php
                     $nik =$row['nik'];
                     $data="SELECT * FROM employee_education WHERE nik ='$nik' ORDER BY year_in  desc";
                     $query_edu = $conn->query($data);
                     while($edit_edu = $query_edu->fetch_assoc())
                     {
                       ?>

                        <div class="form-group">
                        <label class="col-sm-1 control-label">Education</label>
                        <div>
                           <div class="col-sm-2">
                              <select class="form-control select2"  data-toggle="tooltip" title="Education Level" placeholder="Eduction Level"   name="school_type[]"  >
                               <option value=" ">- Select -</option>
                                 <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_edu['school_type']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select>
                           </div>
                           <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Education" placeholder="Education"   name="school_name[]" value="<?php echo $edit_edu['school_name']; ?>">
                           </div>
                            <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Education Department" placeholder="Education Department"   name="education_dept[]" value="<?php echo $edit_edu['education_dept']; ?>">
                           </div>

                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearin[]" value="<?php echo $edit_edu['year_in']; ?>">
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearout[]" value="<?php echo $edit_edu['year_out']; ?>">
                              </div>
                           </div>
                        </div>
                        <a class="hapus-box btn btn-danger btn-sm btn-flat"   data-toggle="tooltip" title="Hapus Data" href="employee_edu_delete.php?id=<?php echo $edit_edu['id']; ?>"  alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA <?php echo $edit_edu['school_name']; ?> ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                        </div>

                      <?php
                    }
                  ?> 
                  <div class="form-group">
                  <div id="main">
                      <div class="form-ku">
                              <center>
                           <p class="text-itan">
                                  <a class="add-box btn btn-primary btn-flat" href="#" id > <i class="fa fa-plus"> </i> Add More</a>
                              </p></center>
                      </div>
                  </div>
                  <script type="text/javascript">
                  jQuery(document).ready(function($){
                      $('.form-ku .add-box').click(function(){
                          var n = $('.text-itan').length + 1;
                          if( 5 < n ) {
                              alert('Stop it!');
                              return false;
                          }
                          var box_html = $(' <div class="form-group"><label class="col-sm-1 control-label">Education</label><div><div class="col-sm-2"><select class="form-control select2"  data-toggle="tooltip" title="Education Level" placeholder="Eductaion Level"   name="add_school_type[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_edu['school_type']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-2"><input type="text" class="form-control"  data-toggle="tooltip" title="Education" placeholder="Education"   name="add_school_name[]" value="<?php echo $edit_edu['school_name']; ?>"></div><div class="col-sm-2"><input type="text" class="form-control"  data-toggle="tooltip" title="Education Department" placeholder="Education Department"   name="add_education_dept[]" value="<?php echo $edit_edu['education_dept']; ?>"></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearin[]" value="<?php echo $edit_edu['year_in']; ?>"></div></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control" data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year" name="add_yearout[]" value="<?php echo $edit_edu['year_out']; ?>"></div></div></div><a href="#" class="remove-box btn btn-danger btn-sm "><i class="fa fa-remove"> </i></a></div>');
                          box_html.hide();
                          $('.form-ku p.text-itan:last').after(box_html);
                          box_html.fadeIn('slow');
                          return false;
                      });
                      $('.form-ku').on('click', '.remove-box', function(){
                          $(this).parent().css( 'background-color', '#' );
                          $(this).parent().fadeOut("slow", function() {
                              $(this).remove();
                              $('.box-number').each(function(index){
                                  $(this).text( index + 1 );
                              });
                          });
                          return false;
                      });
                  });
                  </script>


         </div>
      </div>
   </div>


                   <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Family Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     
                  <?php
                     $nik =$row['nik'];
                     $data="SELECT * FROM employee_family WHERE nik ='$nik' ";
                     $query_fam = $conn->query($data);
                     while($edit_fam = $query_fam->fetch_assoc())
                     {
                       ?>

                        <div class="form-group">
                        <label class="col-sm-1 control-label">Family</label>
                        <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Family Name" placeholder="Family Name"   name="family_name[]" value="<?php echo $edit_fam['family_name']; ?>">
                           </div>
                           <div class="col-sm-1">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="KTP No." placeholder="KTP No."   name="ktp_no_fam[]" value="<?php echo $edit_fam['ktp_no']; ?>">
                           </div>
                           <div class="col-sm-1">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Birth Place" placeholder="Birth Place"   name="birth_place1[]" value="<?php echo $edit_fam['birth_place']; ?>">
                           </div>
                           <div class="col-sm-1">
                              <div class="date">
                                 <input type="text" class="form-control" data-toggle="tooltip" title="Birth Date" placeholder="Birth Date"  id="birth_date1" name="birth_date1[]" value="<?php echo $edit_fam['birth_date'];?>"  >
                              </div>
                           </div>
                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Religion" placeholder="Religion"   name="religion_fam[]"  >
                               <option value=" ">- Select -</option>
                                 <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELIGION'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['religion']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select>
                           </div>
                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Gender" placeholder="Gender"   name="gender_fam[]"  >
                               <option value=" ">- Select -</option>
                                 <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='GENDER'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['gender']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select>
                           </div>

                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Education" placeholder="Education"   name="education[]"  >
                               <option value=" ">- Select -</option>
                                 <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['education']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select>
                           </div>
                           <div class="col-sm-1">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Jobs" placeholder="Jobs"   name="jobs[]" value="<?php echo $edit_fam['jobs']; ?>">
                           </div>
                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Relation" placeholder="Relation"   name="relation[]"  >
                               <option value=" ">- Select -</option>
                                 <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELATION'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['relation']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select>
                           </div>
                            <div class="col-sm-1">
                           <a class="hapus-box btn btn-danger btn-sm btn-flat"   data-toggle="tooltip" title="Hapus Data" href="employee_fam_delete.php?id=<?php echo $edit_fam['id']; ?>"  alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA <?php echo $edit_fam['family_name']; ?> ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                        </div>
                        </div>
                      <?php
                    }
                  ?> 
                  <div class="form-group">
                  <div id="main">
                      <div class="form-fam">
                              <center>
                           <p class="text-fam">
                                  <a class="add-box btn btn-primary btn-flat" href="#" id > <i class="fa fa-plus"> </i> Add More</a>
                              </p></center>
                      </div>
                  </div>
                  <script type="text/javascript">
                  jQuery(document).ready(function($){
                      $('.form-fam .add-box').click(function(){
                          var n = $('.text-fam').length + 1;
                          if( 5 < n ) {
                              alert('Stop it!');
                              return false;
                          }
                          var box_html = $('<div class="form-group"><label class="col-sm-1 control-label">Family</label><div class="col-sm-2"><input type="text" class="form-control"  data-toggle="tooltip" title="Family Name" placeholder="Family Name"   name="add_family_name[]" ></div><div class="col-sm-1"><input type="text" class="form-control"  data-toggle="tooltip" title="KTP No." placeholder="KTP No."   name="add_ktp_no_fam[]" "></div><div class="col-sm-1"><input type="text" class="form-control"  data-toggle="tooltip" title="Birth Place" placeholder="Birth Place"   name="add_birth_place[]" ></div><div class="col-sm-1"><div class="date"><input type="text" class="form-control"  id="birth_date1" name="add_birth_date1[]" placeholder="Birth Date" ></div></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Religion" placeholder="Religion"   name="add_religion_fam[]"  <option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELIGION'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['religion']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Gender" placeholder="Gender"   name="add_gender_fam[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='GENDER'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['gender']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Education" placeholder="Education"   name="add_education[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['education']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-1"><input type="text" class="form-control"  data-toggle="tooltip" title="Jobs" placeholder="Jobs"   name="add_jobs[]" ></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Relation" placeholder="Relation"   name="add_relation[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELATION'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($edit_fam['relation']))
                                             {echo "selected=\"selected\"";};?>
                                 <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><a href="#" class="remove-box btn btn-danger btn-sm "><i class="fa fa-remove"> </i></a></div>');
                          box_html.hide();
                          $('.form-fam p.text-fam:last').after(box_html);
                          box_html.fadeIn('slow');
                          return false;
                      });
                      $('.form-fam').on('click', '.remove-box', function(){
                          $(this).parent().css( 'background-color', '#' );
                          $(this).parent().fadeOut("slow", function() {
                              $(this).remove();
                              $('.box-number').each(function(index){
                                  $(this).text( index + 1 );
                              });
                          });
                          return false;
                      });
                  });
                  </script>
               </div>
            </div>
         </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/employee_modal.php'; ?> 
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>