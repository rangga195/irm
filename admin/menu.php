<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Menu</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <a href="menu_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Menu Name</th>
                   <th>Link</th>
                  <th>Icon</th>
                  <th>Main Menu</th>
                  <th>Sub Menu</th>
                  <th>Sub Sub Menu</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM menu order by is_main_menu,is_sub_menu,is_sub_sub_menu desc";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                        <td><?php echo $row['menu_name']; ?></td>
                          <td><?php echo $row['link']; ?></td>
                           <td><?php echo $row['icon']; ?></td>
                           <td><?php echo $row['is_main_menu']; ?></td>
                           <td><?php echo $row['is_sub_menu']; ?></td>
                           <td><?php echo $row['is_sub_sub_menu']; ?></td>
                          <td>
                           <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['menu_name'];?>" href="menu_edit.php?menu_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Config  <?php echo $row['menu_name'];?>" href="menu_delete.php?id=<?php echo $row['id']; ?>"  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['menu_name'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/configure_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'configure_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#configid').val(response.id);
      $('#edit_code').val(response.code);
      $('#edit_config_code').val(response.config_code);
      $('#edit_config_desc').val(response.config_desc);
      $('#del_configid').val(response.id);
      $('#del_config').html(response.config_desc);
      //$('#company_val').val(response.company_code).html(response.company_name);
    }
  });
}
</script>
</body>
</html>
