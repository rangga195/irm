<?php
	include 'includes/session.php';
	require_once '../PHPExcel/Classes/PHPExcel.php';
	// Panggil class PHPExcel nya
	$excel = new PHPExcel();
	// Settingan awal file excel
	$excel->getProperties()->setCreator('Administrator')
             ->setLastModifiedBy('')
             ->setTitle("Overtime Summary")
             ->setSubject("Overtime ")
             ->setDescription("Overtime Summaryr")
             ->setKeywords("Overtime");
    //Style kolom
    $style_col = array(
		'font' => array('bold' => true), // Set font nya jadi bold
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Style row
	$style_row = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Set header
	$excel->setActiveSheetIndex(0)->setCellValue('A1', "OVERTIME SUMMARY");
	$excel->getActiveSheet()->mergeCells('A1:S1'); // Set Merge Cell pada kolom A1 sampai F1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// Buat header tabel nya pada baris ke 3
	$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('B3', "COMPANY");
	$excel->setActiveSheetIndex(0)->setCellValue('C3', "NIK");
	$excel->setActiveSheetIndex(0)->setCellValue('D3', "NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('E3', "LOCATION");
	$excel->setActiveSheetIndex(0)->setCellValue('F3', "ATTENDANCE DATE");
	$excel->setActiveSheetIndex(0)->setCellValue('G3', "ACTUAL");
	$excel->setActiveSheetIndex(0)->setCellValue('H3', "OVERTIME");

	// Apply style header yang telah kita buat tadi ke masing-masing kolom header
	$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	// Set height baris ke 1, 2 dan 3
	$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(30);
	$nik =$_GET['nik'];
	$start_date = $_GET['start_date'];
    $end_date = $_GET['end_date'];
    $loc_code =$_GET['location_code'];
    $location_code =$user['location_code'];
    if ($loc_code=='') 
    {
    	$sql = mysqli_query($conn,"SELECT a.company_code,
                                a.nik,
                                b.name,
                                b.loc_name,
                                a.att_date,
                                IFNULL(a.overtime,0) AS Actual,
                                a.overtime_hour1+a.overtime_hour2+a.overtime_hour3+a.overtime_hour4 AS Overtime,
                                COUNT( a.id) AS jml_id
                                FROM  attendance_new a
                                LEFT JOIN employee_view b ON a.nik = b.nik
                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date'
                                AND b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                AND b.location_code LIKE CASE '$location_code' WHEN '' THEN '%%' ELSE '$location_code' END 
                                 AND b.status ='Active'
                                GROUP BY a.nik, b.loc_name,b.name, a.att_date
                                HAVING COUNT( a.id) =1
                                ORDER BY a.att_date DESC");
    }
    else
    {
    	$sql = mysqli_query($conn,"SELECT a.company_code,
                                a.nik,
                                b.name,
                                b.loc_name,
                                a.att_date,
                                IFNULL(a.overtime,0) AS Actual,
                                a.overtime_hour1+a.overtime_hour2+a.overtime_hour3+a.overtime_hour4 AS Overtime,
                                COUNT( a.id) AS jml_id
                                FROM  attendance_new a
                                LEFT JOIN employee_view b ON a.nik = b.nik
                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date'
                                AND b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                AND b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END 
                                 AND b.status ='Active'
                                GROUP BY a.nik, b.loc_name,b.name, a.att_date
                                HAVING COUNT( a.id) =1
                                ORDER BY a.att_date DESC");
    }
	
	
	$no = 0;
	$numrow = 3;
	while ($row = mysqli_fetch_array($sql)) {
		$no++;
		$numrow++;
		$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $row['company_code']);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('C'.$numrow, $row['nik'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('D'.$numrow, $row['name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, $row['loc_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, $row['att_date'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('G'.$numrow, $row['Actual'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('H'.$numrow, $row['Overtime'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
	
		
		// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		
	}
	// Set width kolom
	$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
	$excel->getActiveSheet()->getColumnDimension('B')->setWidth(13); // Set width kolom B
	$excel->getActiveSheet()->getColumnDimension('C')->setWidth(23); // Set width kolom C
	$excel->getActiveSheet()->getColumnDimension('D')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('E')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('F')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('G')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('H')->setWidth(10); // Set width kolom D
	// Set orientasi kertas jadi LANDSCAPE
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	// Set judul file excel nya
	$excel->getActiveSheet(0)->setTitle("Overtime");
	$excel->setActiveSheetIndex(0);
	// Proses file excel
	// $objWriter = new PHPExcel_Writer_Excel2007($excel); 
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment; filename="Overtime_Summary'.$_GET['start_date'].'_'.$_GET['end_date'].'.xlsx"'); // Set nama file excel nya
	header('Cache-Control: max-age=0');
	$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	// $write = PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	$write->save('php://output');
	exit;
?>