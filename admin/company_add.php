<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Company List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Company</li>
         <li class="active">Company List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="company_save.php" enctype="multipart/form-data">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Company Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="code" name="code" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="name" name="name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="address" name="address" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address2" class="col-sm-2 control-label">Address2</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="address2" name="address2" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone No</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="phone" name="phone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vat" class="col-sm-2 control-label">VAT No.</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="vat" name="vat" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="postcode" class="col-sm-2 control-label">Post Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="postcode" name="postcode" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label">City</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="city" name="city" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label">Country</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="country" name="country" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="series" class="col-sm-2 control-label">No Series</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="series" name="series" required>
                    </div>
                </div>
               <div class="form-group">
                    <label for="images" class="col-sm-2 control-label">Image</label>

                    <div class="col-sm-9">
                      <input type="file" name="images" id="images"  >
                    </div>
                </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/company_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>

</html>