<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['postcode_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM postcode WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Post Code List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Post Code</li>
         <li class="active">Post Code List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="postcode_update.php?id=<?php echo $row['id']; ?>">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Post Code Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <div class="form-group">
                    <label for="edit_code" class="col-sm-2 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_code" name="code" value="<?php echo $row['kodepos'];?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit_region" class="col-sm-2 control-label">Region</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_region" name="region" value="<?php echo $row['kelurahan'];?>" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="edit_subdistrict" class="col-sm-2 control-label">Sub-Distric</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_subdistrict" name="subdistrict" value="<?php echo $row['kecamatan'];?>" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="edit_district" class="col-sm-2 control-label">Distric</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_district" name="district"  value="<?php echo $row['kabupaten_kota'];?>" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="edit_province" class="col-sm-2 control-label">Province</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_province" name="province" value="<?php echo $row['provinsi'];?>" required>
                    </div>
                </div>

            </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Kembali</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/organization_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>