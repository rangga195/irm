<?php
	include 'includes/session_approval.php';
	include 'includes/sendEmail-v156.php';
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$no = $_POST['no'];
		$company_code = $_POST['company_code'];
		$nik = $_POST['nik'];
		$att_date = $_POST['att_date'];
		$check_in = $_POST['check_in'];
		$check_out = $_POST['check_out'];
		$reason = $_POST['reason'];
		$user = $user['username'];

		$sql = "SELECT * FROM attendance_correction WHERE id = '$id' and status >0";
		$query = $conn->query($sql);
		if($query->num_rows >= 1){
			$_SESSION['error'] = 'Data Already Exist OR Status must been Open';
		}
		else{

			$sql = "UPDATE attendance_correction SET company_code ='$company_code'
										,nik ='$nik'
										,att_date ='$att_date'
										,check_in ='$check_in'
										,check_out ='$check_out'
										,reason = '$reason'
										,status = '2'
										,last_update_by = '$user'
										,last_update_date = NOW()
										 WHERE id = '$id'";
			if($conn->query($sql)){


				$data = mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.att_date,a.check_in,a.check_out,a.reason,a.approve_by,b.approval_level_1,b.approval_level_2 ,CASE a.approve_by 
                          WHEN  '' 
                          THEN CASE b.approval_level_1 WHEN '' THEN b.approval_level_2 ELSE  b.approval_level_1 END
                          ELSE b.approval_level_1 END AS Approve_To
	                FROM attendance_correction a 
	                LEFT JOIN employee_view b ON a.nik = b.nik 
			        WHERE a.nik ='$nik' AND a.att_date ='$att_date'"));
	   
		    	$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
		   			   WHERE a.nik ='".$data['Approve_To']."'"));
		        $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' AND TYPE='admin'"));

		    	 $to       = $cekmail['email_office'];
		   		 $subject  = 'Attendance Correction Request to '.$cekmail['nik'].'-'.$cekmail['name'].'';
		   		 $message='<html><body>';
		   		 // $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
		   		 // $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';		   		 
		   		 $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
		   		 $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve from  :</span></span>';
		   		 $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
		   		 $message.="<tr style='background: #eee;'><td><strong>No.: </strong></td><td>" . $data['no'] . "</td></tr>";
	             $message.="<tr><td><strong>Company Name: </strong></td><td>" . $data['company_code'] . "</td></tr>";
	             $message.="<tr><td><strong>NIK: </strong></td><td>" . $data['nik'] . "</td></tr>";
	             $message.="<tr><td><strong>Employee Name: </strong></td><td>" . $data['name'] . "</td></tr>";
	             $message.="<tr><td><strong>Division: </strong></td><td>" . $data['division_name'] . "</td></tr>";
	             $message.="<tr><td><strong>Department: </strong></td><td>" . $data['department_name'] . "</td></tr>";
	             $message.="<tr><td><strong>Position: </strong></td><td>" . $data['description'] . "</td></tr>";
	             $message.="<tr><td><strong>Location: </strong></td><td>" . $data['loc_name'] . "</td></tr>";
	             $message.="<tr><td><strong>Correction Date: </strong></td><td>" . $data['att_date']. "</td></tr>";
	             $message.="<tr><td><strong>Check In: </strong></td><td>" . $data['check_in'] . "</td></tr>";
	             $message.="<tr><td><strong>Check Out: </strong></td><td>" . $data['check_out'] . "</td></tr>";
	             $message.="<tr><td><strong>Notes: </strong></td><td>". $data['reason']. "</td></tr>";
		   		 $message.="</table>";
		   		 $message.='<br>Click the following button to:</br>';
		   		 $message.='<p><a href="'.$ambil_smtp['base_url'].'/attendance_correction_approve.php?edit=4&id='.$data['id'].'&username1='.$data['approval_level_1'].'&username2='.$data['approval_level_2'].'"><button class="btn btn-sm btn-success">Approve</button></p>';
		   		 $message.='<p><a href="'.$ambil_smtp['base_url'].'/attendance_correction_reject.php?edit=100&id='.$data['id'].'&username1='.$data['approval_level_1'].'&username2='.$data['approval_level_2'].'"><button class="btn btn-sm btn-danger">Reject</a></p>'; 		   		 
		   		$message.='<div class="footer">';
			          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
			          $message.='               <tr>';
			          $message.='                 <td class="content-block">';
			          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
			          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
			          $message.='                 </td>';
			          $message.='               </tr>';
			          $message.='               <tr>';
			          $message.='                 <td class="content-block powered-by">';
			          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
			          $message.='                 </td>';
			          $message.='               </tr>';
			          $message.='             </table>';
			          $message.='           </div>';
			          $message.='</body></html>';
		   
		   		  
		   		  $sender   = $ambil_smtp['email'];
		   		  $password = $ambil_smtp['password'];

				if(email_localhost($to, $subject, $message, $sender, $password))
				    echo "<script>alert('Approval has been sent!')</script>";
				
					else
						 echo "Email sending failed";
				$_SESSION['success'] = 'Correction Attendance updated successfully';
			}
			else{
				$_SESSION['error'] = $conn->error;
			}
		}
	}
	else{
		$_SESSION['error'] = 'Fill up edit form first';
	}

	header('location:attendance_correction.php');

?>