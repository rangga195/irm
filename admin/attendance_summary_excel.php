<?php
	include 'includes/session.php';
	require_once '../PHPExcel/Classes/PHPExcel.php';
	// Panggil class PHPExcel nya
	$excel = new PHPExcel();
	// Settingan awal file excel
	$excel->getProperties()->setCreator('Administrator')
             ->setLastModifiedBy('')
             ->setTitle("Attendance Summary")
             ->setSubject("Attendance ")
             ->setDescription("Attendance Summary")
             ->setKeywords("Attendance");
    //Style kolom
    $style_col = array(
		'font' => array('bold' => true), // Set font nya jadi bold
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Style row
	$style_row = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
		'borders' => array(
			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		)
	);
	//Set header
	$excel->setActiveSheetIndex(0)->setCellValue('A1', "ATTENDANCE SUMMARY");
	$excel->getActiveSheet()->mergeCells('A1:S1'); // Set Merge Cell pada kolom A1 sampai F1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
	$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// Buat header tabel nya pada baris ke 3
	$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO.");
	$excel->setActiveSheetIndex(0)->setCellValue('B3', "COMPANY");
	$excel->setActiveSheetIndex(0)->setCellValue('C3', "NIK");
	$excel->setActiveSheetIndex(0)->setCellValue('D3', "NAME");
	$excel->setActiveSheetIndex(0)->setCellValue('E3', "DEPARTMENT");
	$excel->setActiveSheetIndex(0)->setCellValue('F3', "LOCATION");
	$excel->setActiveSheetIndex(0)->setCellValue('G3', "HOURS");
	$excel->setActiveSheetIndex(0)->setCellValue('H3', "MINUTES");
	$excel->setActiveSheetIndex(0)->setCellValue('I3', "LATE MINUTES");
	$excel->setActiveSheetIndex(0)->setCellValue('J3', "OVERTIME");
	$excel->setActiveSheetIndex(0)->setCellValue('K3', "HOLIDAY");
	$excel->setActiveSheetIndex(0)->setCellValue('L3', "WORK DAY");
	$excel->setActiveSheetIndex(0)->setCellValue('M3', "ATTEND");
	$excel->setActiveSheetIndex(0)->setCellValue('N3', "LATE");
	$excel->setActiveSheetIndex(0)->setCellValue('O3', "LEAVE");
	$excel->setActiveSheetIndex(0)->setCellValue('P3', "SPECIAL LEAVE");
	$excel->setActiveSheetIndex(0)->setCellValue('Q3', "SICK");
	$excel->setActiveSheetIndex(0)->setCellValue('R3', "PERMIT");
	$excel->setActiveSheetIndex(0)->setCellValue('S3', "ABNORMAL");
	$excel->setActiveSheetIndex(0)->setCellValue('T3', "ABSENT");

	// Apply style header yang telah kita buat tadi ke masing-masing kolom header
	$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);
	$excel->getActiveSheet()->getStyle('T3')->applyFromArray($style_col);
	// Set height baris ke 1, 2 dan 3
	$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
	$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(30);
	$nik =$_GET['nik'];
	$start_date = $_GET['start_date'];
    $end_date = $_GET['end_date'];
    $loc_code =$_GET['location_code'];
    $department_code =$_GET['department_code'];
    $location_code =$user['location_code'];

    if ($loc_code=='')
    {
    	$sql = mysqli_query($conn,"SELECT a.company_code,a.nik,b.name,b.loc_name,b.department_code,b.department_name,
                                                IFNULL(SUM(a.work_hour),0) AS Hours,
                                                IFNULL(SUM(a.work_minute),0) AS Minutes,
                                                IFNULL(SUM(a.late_minute),0) AS Late_Minute,
                                                IFNULL(SUM(a.overtime),0) AS Overtime,
                                                IFNULL(SUM(a.is_holiday),0) AS Holiday,
                                                IFNULL(COUNT(att_date),0)-IFNULL(SUM(a.is_holiday),0) AS Work_Day,
                                                IFNULL(SUM(a.is_att),0) AS Att,
                                                IFNULL(SUM(a.is_late),0) AS late,
                                                IFNULL(SUM(a.is_leave),0) AS Cuti,
                                                IFNULL(SUM(a.is_sick),0) AS Sick,
                                                IFNULL(SUM(a.is_spc_leave),0) AS Special,
                                                IFNULL(SUM(a.is_izin),0) AS Izin,
                                                IFNULL(SUM(a.is_abnormal),0) AS Abnormal,
                                                IFNULL(SUM(a.is_absent),0) AS Absent
                                                FROM  attendance_new a
                                                LEFT JOIN employee_view b ON a.nik = b.nik
                                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date'
                                                and b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                                and b.location_code LIKE CASE '$location_code' WHEN '' THEN '%%' ELSE '$location_code' END 
                                                 AND b.status ='Active'
                                                 AND b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                                GROUP BY a.company_code,a.nik,b.name");
    }else
    {
    	$sql = mysqli_query($conn,"SELECT a.company_code,a.nik,b.name,b.loc_name,b.department_code,b.department_name,
                                                IFNULL(SUM(a.work_hour),0) AS Hours,
                                                IFNULL(SUM(a.work_minute),0) AS Minutes,
                                                IFNULL(SUM(a.late_minute),0) AS Late_Minute,
                                                IFNULL(SUM(a.overtime),0) AS Overtime,
                                                IFNULL(SUM(a.is_holiday),0) AS Holiday,
                                                IFNULL(COUNT(att_date),0)-IFNULL(SUM(a.is_holiday),0) AS Work_Day,
                                                IFNULL(SUM(a.is_att),0) AS Att,
                                                IFNULL(SUM(a.is_late),0) AS late,
                                                IFNULL(SUM(a.is_leave),0) AS Cuti,
                                                IFNULL(SUM(a.is_sick),0) AS Sick,
                                                IFNULL(SUM(a.is_spc_leave),0) AS Special,
                                                IFNULL(SUM(a.is_izin),0) AS Izin,
                                                IFNULL(SUM(a.is_abnormal),0) AS Abnormal,
                                                IFNULL(SUM(a.is_absent),0) AS Absent
                                                FROM  attendance_new a
                                                LEFT JOIN employee_view b ON a.nik = b.nik
                                                WHERE a.att_date BETWEEN '$start_date' AND '$end_date'
                                                and b.name LIKE CASE '$nik' WHEN '' THEN '%%' ELSE '%$nik%' END
                                                and b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END 
                                                 AND b.status ='Active'
                                                 AND b.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END
                                                GROUP BY a.company_code,a.nik,b.name");
    }

	
	
	$no = 0;
	$numrow = 3;
	while ($row = mysqli_fetch_array($sql)) {
		$no++;
		$numrow++;
		$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $row['company_code']);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('C'.$numrow, $row['nik'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('D'.$numrow, $row['name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, $row['department_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, $row['loc_name'], PHPExcel_Cell_DataType::TYPE_STRING);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('G'.$numrow, $row['Hours'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('H'.$numrow, $row['Minutes'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('I'.$numrow, $row['Late_Minute'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('J'.$numrow, $row['Overtime'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('K'.$numrow, $row['Holiday'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('L'.$numrow, $row['Work_Day'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('M'.$numrow, $row['Att'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('N'.$numrow, $row['late'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('O'.$numrow, $row['Cuti'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('P'.$numrow, $row['Special'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('Q'.$numrow, $row['Sick'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('R'.$numrow, $row['Izin'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('S'.$numrow, $row['Abnormal'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$excel->setActiveSheetIndex(0)->setCellValueExplicit('T'.$numrow, $row['Absent'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
		// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row);
		$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		
	}
	// Set width kolom
	$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
	$excel->getActiveSheet()->getColumnDimension('B')->setWidth(13); // Set width kolom B
	$excel->getActiveSheet()->getColumnDimension('C')->setWidth(23); // Set width kolom C
	$excel->getActiveSheet()->getColumnDimension('D')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('E')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('F')->setWidth(23); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('G')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('H')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('I')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('J')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('K')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('L')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('M')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('N')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('O')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('P')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('Q')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('R')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('S')->setWidth(10); // Set width kolom D
	$excel->getActiveSheet()->getColumnDimension('T')->setWidth(10); // Set width kolom D
	// Set orientasi kertas jadi LANDSCAPE
	$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	// Set judul file excel nya
	$excel->getActiveSheet(0)->setTitle("Attendance Summary");
	$excel->setActiveSheetIndex(0);
	// Proses file excel
	// $objWriter = new PHPExcel_Writer_Excel2007($excel); 
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment; filename="Attendance_Summary'.$_GET['start_date'].'_'.$_GET['end_date'].'.xlsx"'); // Set nama file excel nya
	header('Cache-Control: max-age=0');
	$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	// $write = PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	$write->save('php://output');
	exit;
?>