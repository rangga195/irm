<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
    <?php
    $sql ="SELECT MAX(CONVERT(position_code,DECIMAL)) AS max_id FROM POSITION ORDER BY id";
      $hasil = $conn->query($sql);
      $data = $hasil->fetch_assoc();
     
      $lastID = $data['max_id'];
      //$lastNoUrut = substr($lastID, 3, 3);
      $nextNoUrut = $lastID + 1;
      $nextID =  $nextNoUrut;//"T".sprintf("%02s",$nextNoUrut);
?> 
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Position List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Position</li>
         <li class="active">Position List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="position_save.php">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Position Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <form class="form-horizontal" method="POST" action="division_add.php">
                <div class="form-group">
                    <label for="company" class="col-sm-2 control-label">Company</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="company" id="company" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="org" class="col-sm-2 control-label">Organization</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="org" id="org" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM organization";
                          $query = $conn->query($sql);
                          while($orgrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$orgrow['org_code']."'>".$orgrow['name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="department_code" class="col-sm-2 control-label">Department</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="department_code" id="department_code" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM department";
                          $query = $conn->query($sql);
                          while($orgrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$orgrow['dept_code']."'>".$orgrow['name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="code" name="code"value="<?php echo  $nextID; ?>"required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Position Title</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="title" name="title" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">Status</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="status" id="status" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM configure where code ='POS TYPE'";
                          $query = $conn->query($sql);
                          while($orgrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$orgrow['config_code']."'>".$orgrow['config_desc']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rate" class="col-sm-2 control-label">Rate per Hr</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="rate" name="rate" required>
                    </div>
                </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/company_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>