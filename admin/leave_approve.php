<?php
include 'includes/session_approval.php';
include 'includes/sendEmail-v156.php';
if(($_GET['edit'])&& $_GET['edit']==4)
{

  $data = mysqli_fetch_array(mysqli_query($conn, "SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.location_code,b.loc_name,a.leave_type,a.leave_code,c.config_desc,a.start_date,a.end_date,a.reason,a.remaining,a.approve_by,b.approval_level_1,b.approval_level_2 ,a.status,CASE b.approval_level_1 WHEN '' THEN b.approval_level_2 ELSE b.approval_level_1 END AS approval_nik
   FROM leave_employee a 
   LEFT JOIN employee_view b ON a.nik = b.nik 
   LEFT JOIN (SELECT * FROM configure WHERE CODE ='LEAVE') c ON a.leave_code = c.config_code
   WHERE a.id='".$_GET['id']."'"));
  $approve1 = $data['approval_level_1'];
  $approve2 = $data['approval_level_2'];
  $approveby =$data['approve_by'];
  $start_date =$data['start_date'];
  $end_date =$data['end_date'];
  $leave_type =$data['leave_type'];
  $leave_code =$data['leave_code'];
  $status =$data['status'];
  $location_code =$data['location_code'];
  $nik =$data['nik'];
  if (($approveby=='') &&  ($approve1==$_GET['username1']) && ($approve2 ==''))
  {

   $data_update1 ="update leave_employee
   set status =1,
   approve_by ='".$_GET['username1']."',
   last_update_by = '".$_GET['username1']."',
   last_update_date =NOW()
   where id ='".$_GET['id']."'and status =2 ";
   if ($status == 2)
   {
    
    if($conn->query($data_update1))
       {

        $update_saldo ="UPDATE leave_remaining a
                        LEFT JOIN leave_employee b ON a.nik = b.nik
                        SET a.remaining_leave = b.remaining
                        WHERE b.status=1
                        AND b.start_date ='$start_date'
                        AND b.end_date ='$end_date'
                        AND b.leave_type = 'CUTI TAHUNAN'
                        AND b.leave_code = '$leave_code'
                        AND a.nik = '$nik'";
        $conn->query($update_saldo)  ;         

        $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
         WHERE a.nik ='".$data['nik']."'"));

        $to       = $cekmail['email_office'];
        $subject  = 'Approval Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
        $message='<html><body>';
        $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
        $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been approved :</span></span>';
        $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
        $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>". $data['no'] ."</td></tr>";
        $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
        $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
        $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
        $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
        $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
        $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
        $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
        $message.="<tr><td><strong>Leave Type:</strong></td><td>" . $data['leave_type'] . "</td></tr>";
        $message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
        $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
        $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
        $message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
        $message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
        $message.= "</table>";
        $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

        $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' AND TYPE='admin'"));
        $sender   = $ambil_smtp['email'];
        $password = $ambil_smtp['password'];
        
        $ambil_email=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM email_site WHERE location_code ='$location_code'"));
        $cc=$ambil_email['email'];

        if(email_localhost($to, $subject, $message, $sender, $password))
        {
          echo "<script>alert('Request has been approve and email has been sent...')</script>";
          
        }
        else
        {  
          echo "<script>alert('Email sending failed...')</script>";
          
        }
      }
   }
   elseif($status==1)
   {
    echo "<script>alert('Data Has been Approved...')</script>";
   }
   elseif($status==0)
   {
       echo "<script>alert('Data Has been Rejected...')</script>";
   }
}

if (($approveby=='') &&  ($approve1=='') && ($approve2 ==$_GET['username2']))
  {

   $data_update1 ="update leave_employee
   set status =1,
   approve_by ='".$_GET['username2']."',
   last_update_by = '".$_GET['username2']."',
   last_update_date =NOW()
   where id ='".$_GET['id']."'and status =2 ";
   if ($status == 2)
   {
    
    if($conn->query($data_update1))
       {

        $update_saldo ="UPDATE leave_remaining a
                        LEFT JOIN leave_employee b ON a.nik = b.nik
                        SET a.remaining_leave = b.remaining
                        WHERE b.status=1
                        AND b.start_date ='$start_date'
                        AND b.end_date ='$end_date'
                        AND b.leave_type = 'CUTI TAHUNAN'
                        AND b.leave_code = '$leave_code'
                        AND a.nik = '$nik'";
        $conn->query($update_saldo);

        $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
         WHERE a.nik ='".$data['nik']."'"));

        $to       = $cekmail['email_office'];
        $subject  = 'Approval Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
        $message='<html><body>';
        $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
        $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been approved :</span></span>';
        $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
        $message.="<tr style='background: #eee;'><td><strong>No.:        </strong></td><td>". $data['no'] ."</td></tr>";
        $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
        $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
        $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
        $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
        $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
        $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
        $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
        $message.="<tr><td><strong>Leave Type:</strong></td><td>" . $data['leave_type'] . "</td></tr>";
        $message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
        $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
        $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
        $message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
        $message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
        $message.= "</table>";
        $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

        $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' AND TYPE='admin'"));
        $sender   = $ambil_smtp['email'];
        $password = $ambil_smtp['password'];


        if(email_localhost($to, $subject, $message, $sender, $password))
        {
          echo "<script>alert('Request has been approve and email has been sent...')</script>";
          
        }
        else
        {  
          echo "<script>alert('Email sending failed...')</script>";
          
        }
      }
   }
   elseif($status==1)
   {
    echo "<script>alert('Data Has been Approved...')</script>";
   }
   elseif($status==0)
   {
       echo "<script>alert('Data Has been Rejected...')</script>";
   }
}


if (($approveby=='') &&  ($approve1==$_GET['username1']) && ($approve2 !=''))
{
  $data_update2 ="update leave_employee
  set approve_by ='".$_GET['username1']."',
  last_update_by = '".$_GET['username1']."',
  last_update_date =NOW()
  where id ='".$_GET['id']."'and status =2 and approve_by='' ";
  if ($status ==2)
  {
    if($conn->query($data_update2))
      {
        $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
         WHERE a.nik ='".$data['approval_level_2']."'"));

        $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1'  AND TYPE='admin'"));

        $to       = $cekmail['email_office'];
        $subject  = 'Leave Approval Notification to '.$cekmail['nik'].'-'.$cekmail['name'].'';
        $message='<html><body>';
        $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
        $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve leave from the following employees :</span></span>';
        $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
        $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
        $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
        $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
        $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
        $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
        $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
        $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
        $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
        $message.="<tr><td><strong>Leave Type:</strong></td><td>" . $data['leave_type'] . "</td></tr>";
        $message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
        $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
        $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
        $message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
        $message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
        $message.= "</table>";
        $message.='<br>Click the following button to:</br>';
        $message.='<p><a href="'.$ambil_smtp['base_url'].'/leave_approve.php?edit=4&id='.$data['id'].'&username2='.$data['approval_level_2'].'&username1='.$data['approval_level_1'].'"><button class="btn btn-sm btn-success">Approve</button></a></p>';
        $message.='<p><a href="'.$ambil_smtp['base_url'].'/leave_reject.php?edit=100&id='.$data['id'].'&username2='.$data['approval_level_2'].'&username1='.$data['approval_level_1'].'"><button class="btn btn-sm btn-danger">Reject</button></a></p>';
        $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';


        $sender   = $ambil_smtp['email'];
        $password = $ambil_smtp['password'];

        if(email_localhost($to, $subject, $message, $sender, $password))
        {
          
         echo "<script>alert('Request has been approve and email has been sent...')</script>";
         
         
       }
       else
       {
        echo "<script>alert('Email sending failed...')</script>";
      }
    } 

  }
  elseif ($status==1)
   {
    echo "<script>alert('Data Has been Approved...')</script>";
   }
   elseif ($status==0)
   {
       echo "<script>alert('Data Has been Rejected...')</script>";
   }
}   
if (($approveby!='') &&  ($approve1==$_GET['username1']) && ($approve2 ==$_GET['username2']))
{

  $data_update1 ="update leave_employee
  set status =1,
  approve_by ='".$_GET['username2']."',
  last_update_by = '".$_GET['username2']."',
  last_update_date =NOW()
  where id ='".$_GET['id']."'and status =2 and approve_by ='".$_GET['username1']."'";

  if ($status == 2)
  {
    if($conn->query($data_update1))
      {
        $update_saldo ="UPDATE leave_remaining a
                        LEFT JOIN leave_employee b ON a.nik = b.nik
                        SET a.remaining_leave = b.remaining
                        WHERE b.status=1
                        AND b.start_date ='$start_date'
                        AND b.end_date ='$end_date'
                        AND b.leave_type = 'CUTI TAHUNAN'
                        AND b.leave_code = '$leave_code'
                        AND a.nik = '$nik'";
        $conn->query($update_saldo);
        $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
         WHERE a.nik ='".$data['nik']."'"));

        $to       = $cekmail['email_office'];
        $subject  = 'Approval Request Leave for '.$cekmail['nik'].'-'.$cekmail['name'].'';
        $message='<html><body>';
        $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
        $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Leave Request has been approved :</span></span>';
        $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
        $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>". $data['no'] ."</td></tr>";
        $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
        $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
        $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
        $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
        $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
        $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
        $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
        $message.="<tr><td><strong>Leave Type:</strong></td><td>" . $data['leave_type'] . "</td></tr>";
        $message.="<tr><td><strong>Reason for Leave:</strong></td><td>" . $data['config_desc'] . "</td></tr>";
        $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
        $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
        $message.="<tr><td><strong>Reason Leave:</strong></td><td>" . $data['reason']. "</td></tr>";
        $message.="<tr><td><strong>Remaining Leave:</strong></td><td>" . $data['remaining']. "</td></tr>";
        $message.= "</table>";
        $message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';

        $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1'  AND TYPE='admin'"));
        $sender   = $ambil_smtp['email'];
        $password = $ambil_smtp['password'];


        if(email_localhost($to, $subject, $message, $sender, $password))
        {
          echo "<script>alert('Request has been approve and email has been sent...')</script>";
          
        }
        else
        { 
         echo "<script>alert('Email sending failed...')</script>";
       }
     }
  }
  elseif($status==1)
   {
    echo "<script>alert('Data Has been Approved...')</script>";
   }
   elseif($status==0)
   {
       echo "<script>alert('Data Has been Rejected...')</script>";
   }

  
}
echo "<script type='text/javascript'>
setTimeout(function() { 
 window.location.href = 'http://taconnect.taco.co.id';
 },2000);
 </script>";
}


?>