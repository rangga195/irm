<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
  include '../timezone.php';
   $range_to = date('Y-m-d');
   $range_from = date('Y-m-01');//date('Y-m-d', strtotime('-30 day', strtotime($range_to)));
?>
  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Request to Approve
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Request to Approve</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <!-- <a href="leave_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
              <div class="pull-right">
                <form method="get" class="form-inline" >
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input autocomplete="off" type="text" class="form-control " id="start_date" name="start_date" value="<?php echo  $range_from; ?>" >
                  </div>
                  <input autocomplete="off" type="text" class="form-control " id="end_date" name="end_date" value="<?php echo  $range_to; ?>" >
                   <select class="form-control select2" name="location_code" id="location_code" >
                        <option value="" selected>- Select Location -</option>
                        <?php
                           $loc_code =$user['location_code'];
                           $sql = "SELECT * FROM location
                            where loc_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END";
                           $query = $conn->query($sql);
                           while($comrow = $query->fetch_assoc()){
                             echo "
                               <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                             ";
                           }
                           ?>
                     </select>
                  <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                  
                </form>
              </div> -->
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Code</th>
                  <th>No. Request</th>
                  <th>Company</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>Department</th>
                  <th>Location</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Reason/Notes</th>
                  <th>Status</th>
                  <th>Approve To</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                  // if(isset($_GET['start_date']))
                  //     {
                  //       $start_date = $_GET['start_date'];
                  //       $end_date = $_GET['end_date'];
                  //       $loc_code =$_GET['location_code'];
                  //       $sql = "SELECT a.id
                  //                ,a.company_code
                  //                ,a.nik
                  //                ,c.name
                  //                ,c.loc_name
                  //                ,a.leave_code
                  //                ,b.config_desc
                  //                ,a.start_date
                  //                ,a.end_date
                  //                ,a.reason
                  //                ,a.remaining
                  //                ,case a.status when 0 then 'Open'
                  //                               when 1 then 'Approve'
                  //                               when 2 then 'Pending Approval' 
                  //                               end as status
                  //                ,a.approve_by
                  //           FROM leave_employee a 
                  //         LEFT JOIN  configure b ON a.leave_code = b.config_code
                  //         LEFT JOIN employee_view c on a.nik = c.nik
                  //         WHERE b.code ='LEAVE' 
                  //         and a.start_date between '$start_date' and '$end_date'
                  //         and c.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END ";
                  //       }
                  //       else
                  //       {
                  //         $start_date = $range_from;
                  //         $end_date = $range_to;
                  //         $loc_code =$user['location_code'];
                  //         $sql = "SELECT a.id
                  //                ,a.company_code
                  //                ,a.nik
                  //                ,c.name
                  //                ,c.loc_name
                  //                ,a.leave_code
                  //                ,b.config_desc
                  //                ,a.start_date
                  //                ,a.end_date
                  //                ,a.reason
                  //                ,a.remaining
                  //                ,case a.status when 0 then 'Open'
                  //                               when 1 then 'Approve'
                  //                               when 2 then 'Pending Approval' 
                  //                               end as status
                  //                ,a.approve_by
                  //           FROM leave_employee a 
                  //         LEFT JOIN  configure b ON a.leave_code = b.config_code
                  //         LEFT JOIN employee_view c on a.nik = c.nik
                  //         WHERE b.code ='LEAVE'
                  //         and a.start_date between '$start_date' and '$end_date'
                  //         and c.location_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END ";
                  //       }
                  $sql = "SELECT a.code,a.id,a.no,a.company_code,a.nik,b.name,b.department_name,b.loc_name,
                            a.start_date,a.end_date,a.start_time,a.end_time,
                          a.reason,CASE a.status WHEN 2 THEN 'Pending Approval' END AS status,
                          CASE a.approve_by 
                          WHEN  '' 
                          THEN CASE b.approval_level_1 WHEN '' THEN b.approval_level_2 ELSE  b.approval_level_1 END
                          ELSE b.approval_level_2 END AS Approve_To
                          
                          FROM request_approval a
                          JOIN employee_view b ON a.nik = b.nik                        
                          
                          ";
                          // where CASE a.approve_by WHEN  '' THEN b.approval_level_1 ELSE b.approval_level_2 END ='".$user['username']."'
                   
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      // $status = ($row['status'])?'<span class="label label-danger">Pending Approval</span>':'<span class="label label-success">Approve</span>';
                      ?>

                        <tr>
                           <td><?php echo $row['code']; ?></td>
                           <td><?php echo $row['no']; ?></td>
                           <td><?php echo $row['company_code']; ?></td>
                           <td><?php echo $row['nik']; ?></td>
                           <td><?php echo $row['name']; ?></td>
                           <td><?php echo $row['department_name']; ?></td>
                           <td><?php echo $row['loc_name']; ?></td>
                            <td><?php echo $row['start_date']; ?></td>
                           <td><?php echo $row['end_date']; ?></td>
                           <td><?php echo $row['start_time']; ?></td>
                           <td><?php echo $row['end_time']; ?></td>
                           <td><?php echo $row['reason']; ?></td>
                           <td><?php 
                                            if($row['status'] == 'Open'){
                                echo '<span class="label label-danger btn-flat">Open</span>';
                              }
                                            else if ($row['status'] == 'Pending Approval' ){
                                echo '<span class="label label-warning btn-flat">Pending Approval</span>';
                              }
                                            else if ($row['status'] == 'Approve' ){
                                echo '<span class="label label-success btn-flat">Approve</span>';
                              }
                             ?></td>
                           <td><?php echo $row['Approve_To']; ?></td>
                          <td>
                        
                            <a class="btn btn-sm btn-success btn-flat"   data-toggle="tooltip" title="Approve  <?php echo $row['code'];?> - No.  <?php echo $row['no'];?> <?php echo $row['nik'];?>" href="request_approve.php?id=<?php echo $row['id']; ?>&code=<?php echo $row['code']; ?>"  alt="Approve Data" name ="approve" onclick="return confirm('Are you sure approve this data ? <?php echo $row['code'];?> - No.  <?php echo $row['no'];?> <?php echo  $row['nik'] ?>  ?')"> <i class="glyphicon glyphicon-ok"></i></a>
                           
                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Reject  <?php echo $row['code'];?> - No.  <?php echo $row['no'];?> <?php echo $row['nik'];?>" href="reject_approve.php?id=<?php echo $row['id']; ?>&code=<?php echo $row['code']; ?>"  alt="Reject Data" name ="reject" onclick="return confirm('Are you sure reject this data ? <?php echo $row['code'];?> - No.  <?php echo $row['no'];?> <?php echo  $row['nik'] ?>  ?')"> <i class="glyphicon glyphicon-remove"></i></a>
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/jobs_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'jobs_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#jobsid').val(response.id);
      $('#edit_code').val(response.job_code);
      $('#edit_name').val(response.name);
      $('#del_jobsid').val(response.id);
      $('#del_jobs').html(response.name);
      $('#company_val').val(response.company_code).html(response.company_name);
    }
  });
}
</script>
</body>
</html>
