<?php 
	include 'includes/session.php';

	if(isset($_POST['id'])){
		$id = $_POST['id'];
		$sql = "SELECT  a.id,
                                    a.company_code,
                                    b.company_name,
                                    a.loc_code,
                                    c.name,
                                    a.group_code,
                                    a.group_name
                                     FROM schedules_group a
                      LEFT JOIN company b ON a.company_code = b.company_code
                      LEFT JOIN location c ON a.loc_code = c.loc_code
			    WHERE a.id = '$id'";
		$query = $conn->query($sql);
		$row = $query->fetch_assoc();

		echo json_encode($row);
	}
?>