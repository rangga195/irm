<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shift Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Shift Management</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header ">
              <a href="shift_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>No.</th>
                  <th>Company</th>
                  <th>Location</th>
                  <th>Work Group</th>
                  <th>Time Schedule</th>
                  <th>Work Date</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $loc_code =$user['location_code'];
                    $sql = "SELECT a.id,
                                    a.company_code,
                                    a.loc_code,
                                    d.name,
                                    a.group_code,
                                    b.group_name,
                                    a.sch_id,
                                    c.sch_name,
                                    a.work_date
                                   FROM working_time a
                                  LEFT JOIN schedules_group b ON a.loc_code = b.loc_code AND a.group_code = b.group_code
                                  LEFT JOIN schedules_work c ON a.loc_code = c.loc_code AND a.sch_id = c.sch_code
                                  LEFT JOIN location d ON a.loc_code = d.loc_code
                                  where a.loc_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END
                                  order by a.work_date
                                  ";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                          <td><?php echo $row['id']; ?></td>
                          <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['name']; ?></td>
                          <td><?php echo $row['group_name']; ?></td>
                          <td><?php echo $row['sch_name']; ?></td>
                          <td><?php echo $row['work_date']; ?></td>
                          <td>

                             <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['id'];?>" href="shift_edit.php?shift_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Shift  <?php echo $row['id'];?>" href="shift_delete.php?id=<?php echo $row['id']; ?>"  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['id'] ?>-<?php echo  $row['group_name'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/group_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'group_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#groupid').val(response.id);
      $('#edit_code').val(response.group_code);
      $('#edit_name').val(response.group_name);
      $('#del_groupid').val(response.id);
      $('#del_group').html(response.group_name);
      $('#company_val').val(response.company_code).html(response.company_name);
      $('#location_val').val(response.loc_code).html(response.name);
    }
  });
}
</script>
</body>
</html>
