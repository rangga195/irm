<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['group_corr_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM schedules_group_corr WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Correction Group List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Correction Group</li>
         <li class="active">Correction Group List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="group_corr_update.php?id=<?php echo $row['id']; ?>">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i>Correction Group Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <div class="form-group">
                  <label for="company_code" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="company_code" id="company_code"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM company";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['company_code']; ?>" <?php if(($k['company_code'])== ($row['company_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['company_code']==$k['company_code'])?print(" "):print(""); ?>  > <?php echo $k['company_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="nik" class="col-sm-2 control-label">NIK</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="nik" id="nik"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM employee_view";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['nik']; ?>" <?php if(($k['nik'])== ($row['nik']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['nik']==$k['nik'])?print(" "):print(""); ?>  > <?php echo $k['nik'];?>-<?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
                <div class="form-group">
                  <label for="group_code" class="col-sm-2 control-label">Working Group</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="group_code" id="group_code"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM schedules_group";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['group_code']; ?>" <?php if(($k['group_code'])== ($row['group_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['group_code']==$k['group_code'])?print(" "):print(""); ?>  > <?php echo $k['group_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              <div class="form-group">
                  <label for="start_date" class="col-sm-2 control-label">Start Date</label>
                  <div class="col-sm-9">
                     <div class="date">
                        <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $row['start_date'];?>">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="end_date" class="col-sm-2 control-label">End Date</label>
                  <div class="col-sm-9">
                     <div class="date">
                        <input type="text" class="form-control " id="end_date" name="end_date" value="<?php echo $row['end_date'];?>">
                     </div>
                  </div>
               </div>
                <div class="form-group">
                  <label for="reason" class="col-sm-2 control-label">Reason</label>
                  <div class="col-sm-9">
                     <div class="text">
                        <input type="text" class="form-control " id="reason" name="reason" value="<?php echo $row['reason'];?>">
                     </div>
                  </div>
               </div>
               

            </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/organization_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>