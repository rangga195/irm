<aside class="main-sidebar">
   <link rel="icon" href="../images/images.png">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo (!empty($user['emp_photo'])) ? '../emp_photo/'.$user['emp_photo'] : '../emp_photo/profile.jpg'; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user['firstname'].' '.$user['lastname']; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">REPORTS</li>
        <li class=""><a href="home.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="header">MANAGE</li>
        
        <li><a href="attendance_new.php"><i class="fa fa-calendar"></i> <span>Time & Attendance</span></a></li>
        <li><a href="attendance_summary.php"><i class="fa fa-calendar"></i> <span>Attendance Summary</span></a></li>
        <li><a href="overtime_summary.php"><i class="fa fa-calendar"></i> <span>Overtime Summary</span></a></li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Employees</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="employee.php"><i class="fa fa-circle-o"></i> Employee List</a></li>
            <li><a href="leave.php"><i class="fa fa-circle-o"></i> Leave</a></li>
            <li><a href="trip.php"><i class="fa fa-circle-o"></i> Bussiness Trip</a></li>
            <li><a href="overtime.php"><i class="fa fa-circle-o"></i> Overtime</a></li>
            <li><a href="attendance_correction.php"><i class="fa fa-circle-o"></i> Attendance Correction</a></li>
            <li><a href="travel.php"><i class="fa fa-circle-o"></i> Itinerary Plan</a></li>
          </ul>
        </li> -->
        <!-- <li class="treeview">
        <a href="#">
            <i class="fa fa-home"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="company.php"><i class="fa fa-industry"></i> Company</a></li>
            <li><a href="organization.php"><i class="fa fa-group (alias)"></i> Organization</a></li>
            <li><a href="division.php"><i class="fa fa-institution (alias)"></i> Division</a></li>
            <li><a href="department.php"><i class="fa fa-institution (alias)"></i> Department</a></li>
            <li><a href="position.php"><i class="fa fa-suitcase"></i> Positions</a></li>
            <li><a href="jobs.php"><i class="fa fa-circle-o"></i> Jobs Level</a></li>
            <li><a href="location.php"><i class="fa fa-location-arrow"></i> Location</a></li>
          </ul> -->

        <!-- <li class="treeview">
        <a href="#">
            <i class="fa fa-calendar-o"></i>
            <span>Schedules</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="shift.php"><i class="fa fa-clock-o"></i> Shift Management</a></li>
             <li><a href="work_group.php"><i class="fa fa-clock-o"></i> Work Group</a></li>
            <li><a href="time.php"><i class="fa fa-clock-o"></i> Time Schedule</a></li>
            <li><a href="group.php"><i class="fa fa-envelope-o"></i> Group Schedule</a></li>
            <li><a href="group_corr.php"><i class="fa fa-envelope-o"></i>Correction Group Schedule</a></li>
          </ul>   -->

        <!-- <li class="treeview">
        <a href="#">
            <i class="fa fa-home"></i>
            <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="calendar.php"><i class="fa fa-calendar"></i> Calendar</a></li>
            <li><a href="smtp.php"><i class="fa fa-envelope-o"></i> SMTP</a></li>
             <li><a href="configure.php"><i class="fa fa-envelope-o"></i> Configuration</a></li>
             <li><a href="configure.php"><i class="fa fa-envelope-o"></i> User</a></li>
             <li><a href="approval_setup.php"><i class="fa fa-envelope-o"></i> Approval Setup</a></li>
             <li><a href="closing_period.php"><i class="fa fa-envelope-o"></i> Closing Period</a></li>
             <li><a href="menu.php"><i class="fa fa-envelope-o"></i> Menu</a></li>
          </ul>   -->

          <!-- <li class="treeview">
         <a href="#">
            <i class="fa fa-home"></i>
            <span>Booking Meeting Room</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="meeting.php"><i class="fa fa-calendar"></i> Metting Room 1</a></li>
          </ul>   -->
          <?php
         
          $menu ="SELECT * FROM menu where link ='#' ORDER BY is_main_menu ASC";
          $query = $conn->query($menu);
          while($dataMenu = $query->fetch_assoc())
          {
             $menu_id = $dataMenu['is_main_menu'];
             $submenu = "SELECT * FROM menu WHERE is_main_menu='$menu_id' ORDER BY is_sub_menu ASC";
             $querysubmenu = $conn->query($submenu);
             $ismain_menu = $querysubmenu->fetch_assoc();
            if(($ismain_menu['is_main_menu']) == 0)
            {
              echo '<li><a href="'.$dataMenu['link'].'">'.$dataMenu['menu_name'].'</a></li>';
            }            
            else
            {
              echo '
               <li class="treeview">
                <a href="'.$dataMenu['link'].'">
                    <i class="'.$dataMenu['icon'].'"></i>
                    <span>'.$dataMenu['menu_name'].'</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                <ul class="treeview-menu">';
                while($dataSubmenu = $querysubmenu->fetch_assoc()){
                  echo '<li><a href="'.$dataSubmenu['link'].'"><i class="'.$dataSubmenu['icon'].'"></i>'.$dataSubmenu['menu_name'].'</a></li>';
                }
              echo '
                </ul>
              </li>
              ';
            }
          }
          ?> 


      <!--   <li><a href="deduction.php"><i class="fa fa-file-text"></i> Deductions</a></li>
        
        <li class="header">PRINTABLES</li>
        <li><a href="payroll.php"><i class="fa fa-files-o"></i> <span>Payroll</span></a></li>
        <li><a href="schedule_employee.php"><i class="fa fa-clock-o"></i> <span>Schedule</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>