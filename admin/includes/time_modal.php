<!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Add Time Schedule</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="time_add.php">
                <div class="form-group">
                    <label for="company" class="col-sm-3 control-label">Company</label>

                    <div class="col-sm-9">
                      <select class="form-control input-sm" name="company" id="company" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="location" class="col-sm-3 control-label">Location</label>

                    <div class="col-sm-9">
                      <select class="form-control input-sm" name="location" id="location" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM location";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>

                 <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="code" name="code" required>
                    </div>
                </div>

          		  <div class="form-group">
                  	<label for="name" class="col-sm-3 control-label">Name</label>

                  	<div class="col-sm-9">
                    	<input type="text" class="form-control input-sm" id="name" name="name" required>
                  	</div>
                </div>
                
                <div class="form-group">
                    <label for="start_time" class="col-sm-3 control-label">Start Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="start_time" name="start_time" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="end_time" class="col-sm-3 control-label">End Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="end_time" name="end_time" required>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="late" class="col-sm-3 control-label">Late</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control input-sm" id="late" name="late" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="early" class="col-sm-3 control-label">Early</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control input-sm" id="early" name="early" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkin1" class="col-sm-3 control-label">Check In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="checkin1" name="checkin1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkout1" class="col-sm-3 control-label">Check Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="checkout1" name="checkout1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkin2" class="col-sm-3 control-label">Check in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="checkin2" name="checkin2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="checkout2" class="col-sm-3 control-label">Check Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="checkout2" name="checkout2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="ovetimein1" class="col-sm-3 control-label">Overtime In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="ovetimein1" name="ovetimein1" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout1" class="col-sm-3 control-label">Overtime Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="overtimeout1" name="overtimeout1" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimein2" class="col-sm-3 control-label">Overtime In2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm"  id="overtimein2" name="overtimein2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout2" class="col-sm-3 control-label">Overtime Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker ">
                        <input type="text" class="form-control timepicker input-sm" id="overtimeout2" name="overtimeout2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimein3" class="col-sm-3 control-label">Overtime In3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="overtimein3" name="overtimein3" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout3" class="col-sm-3 control-label">Overtime Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="overtimeout3" name="overtimeout3" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimein4" class="col-sm-3 control-label">Overtime In4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="overtimein4" name="overtimein4" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout4" class="col-sm-3 control-label">Overtime Out4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="overtimeout4" name="overtimeout4" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="breakein1" class="col-sm-3 control-label">Breake in1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="breakein1" name="breakein1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakeout1" class="col-sm-3 control-label">Breake Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="breakeout1" name="breakeout1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakein2" class="col-sm-3 control-label">Breake in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="breakein2" name="breakein2" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakeout2" class="col-sm-3 control-label">Breake Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="breakeout2" name="breakeout2" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakein3" class="col-sm-3 control-label">Breake in3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="breakein3" name="breakein3" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakeout3" class="col-sm-3 control-label">Breake Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="breakeout3" name="breakeout3" required>
                      </div>
                    </div>
                </div>
                
                
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button>
            	</form>
          	</div>
        </div>
    </div>
</div>

<!-- Edit -->
<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Update Time Schedule</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="time_edit.php">
            		<input type="hidden" id="timeid" name="id">
                <div class="form-group">
                    <label for="edit_comp" class="col-sm-3 control-label">Company</label>
                    <div class="col-sm-9">
                      <select class="form-control input-sm" name="edit_comp" id="edit_comp">
                        <option selected id="company_val"></option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($prow = $query->fetch_assoc()){
                            echo "
                              <option value='".$prow['company_code']."'>".$prow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>

                 <div class="form-group">
                    <label for="edit_comp" class="col-sm-3 control-label">Location</label>
                    <div class="col-sm-9">
                      <select class="form-control input-sm" name="edit_loc" id="edit_loc">
                        <option selected id="location_val"></option>
                        <?php
                          $sql = "SELECT * FROM location";
                          $query = $conn->query($sql);
                          while($prow = $query->fetch_assoc()){
                            echo "
                              <option value='".$prow['loc_code']."'>".$prow['name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>

                 
                 <div class="form-group">
                    <label for="edit_code" class="col-sm-3 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_code" name="code" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit_name" class="col-sm-3 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_name" name="name" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="edit_start_time" class="col-sm-3 control-label">Start Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_start_time" name="start_time" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_end_time" class="col-sm-3 control-label">End Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_end_time" name="end_time" required>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit_late" class="col-sm-3 control-label">Late</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control input-sm" id="edit_late" name="late" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_early" class="col-sm-3 control-label">Early</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control input-sm" id="edit_early" name="early" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_checkin1" class="col-sm-3 control-label">Check In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_checkin1" name="checkin1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_checkout1" class="col-sm-3 control-label">Check Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_checkout1" name="checkout1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_checkin2" class="col-sm-3 control-label">Check in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_checkin2" name="checkin2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_checkout2" class="col-sm-3 control-label">Check Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_checkout2" name="checkout2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_ovetimein1" class="col-sm-3 control-label">Overtime In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_ovetimein1" name="ovetimein1" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout1" class="col-sm-3 control-label">Overtime Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimeout1" name="overtimeout1" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein2" class="col-sm-3 control-label">Overtime In2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimein2" name="overtimein2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout2" class="col-sm-3 control-label">Overtime Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker ">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimeout2" name="overtimeout2" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein3" class="col-sm-3 control-label">Overtime In3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimein3" name="overtimein3" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout3" class="col-sm-3 control-label">Overtime Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimeout3" name="overtimeout3" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein4" class="col-sm-3 control-label">Overtime In4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimein4" name="overtimein4" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout4" class="col-sm-3 control-label">Overtime Out4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_overtimeout4" name="overtimeout4" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_breakein1" class="col-sm-3 control-label">Breake in1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_breakein1" name="breakein1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakeout1" class="col-sm-3 control-label">Breake Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_breakeout1" name="breakeout1" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakein2" class="col-sm-3 control-label">Breake in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_breakein2" name="breakein2" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakeout2" class="col-sm-3 control-label">Breake Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_breakeout2" name="breakeout2" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakein3" class="col-sm-3 control-label">Breake in3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_breakein3" name="breakein3" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakeout3" class="col-sm-3 control-label">Breake Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control timepicker input-sm" id="edit_breakeout3" name="breakeout3" required>
                      </div>
                    </div>
                </div>
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-success btn-flat" name="edit"><i class="fa fa-check-square-o"></i> Update</button>
            	</form>
          	</div>
        </div>
    </div>
</div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Deleting...</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="time_delete.php">
            		<input type="hidden" id="del_timeid" name="id">
            		<div class="text-center">
	                	<p>DELETE TIME SCHEDULE</p>
	                	<h2 id="del_time" class="bold"></h2>
	            	</div>
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-danger btn-flat" name="delete"><i class="fa fa-trash"></i> Delete</button>
            	</form>
          	</div>
        </div>
    </div>
</div>


     