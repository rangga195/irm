<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>All rights reserved</b>
    </div>
    <strong>Copyright &copy; 2019 <a href="https://www.taco.co.id.com/">TACO GROUP</a></strong>
</footer>