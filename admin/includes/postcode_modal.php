<!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Add Post Code</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="postcode_add.php">
                 <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="code" name="code" required>
                    </div>
                </div>
          		  <div class="form-group">
                  	<label for="region" class="col-sm-3 control-label">Region</label>

                  	<div class="col-sm-9">
                    	<input type="text" class="form-control input-sm" id="region" name="region" required>
                  	</div>
                </div>
                  <div class="form-group">
                    <label for="subdistrict" class="col-sm-3 control-label">Sub-District</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="subdistrict" name="subdistrict" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="district" class="col-sm-3 control-label">Districts</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="district" name="district" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="province" class="col-sm-3 control-label">Province</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="province" name="province" required>
                    </div>
                </div>
                
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button>
            	</form>
          	</div>
        </div>
    </div>
</div>

<!-- Edit -->
<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Update Post Code</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="postcode_edit.php">
            		<input type="hidden" id="codeid" name="id">
                 <div class="form-group">
                    <label for="edit_code" class="col-sm-3 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_code" name="code" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit_region" class="col-sm-3 control-label">Region</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_region" name="region" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="edit_subdistrict" class="col-sm-3 control-label">Sub-Distric</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_subdistrict" name="subdistrict" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="edit_district" class="col-sm-3 control-label">Distric</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_district" name="district" required>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="edit_province" class="col-sm-3 control-label">Province</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_province" name="province" required>
                    </div>
                </div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-success btn-flat" name="edit"><i class="fa fa-check-square-o"></i> Update</button>
            	</form>
          	</div>
        </div>
    </div>
</div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Deleting...</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="postcode_delete.php">
            		<input type="hidden" id="del_codeid" name="id">
            		<div class="text-center">
	                	<p>DELETE POST CODE</p>
	                	<h2 id="del_postcode" class="bold"></h2>
	            	</div>
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-danger btn-flat" name="delete"><i class="fa fa-trash"></i> Delete</button>
            	</form>
          	</div>
        </div>
    </div>
</div>


     