<?php
	session_start();
	include 'includes/conn.php';

	if(!isset($_SESSION['admin']) || trim($_SESSION['admin']) == '')
	{

		$admin = $_SESSION['admin'];
		header('location: home.php');
		exit;

			
	}

	$sql = "SELECT  a.username,
						a.password,
						a.firstname,
						a.lastname,
						a.photo,
						b.company_name,
						b.id,
						b.nik,
						b.name,
						b.division_name,
						b.department_name,
						b.description,
						b.job_name,
						b.loc_name,
						b.gender,
						b.emp_photo,
						a.created_on,
						b.location_code,
						b.company_code,
						b.language_skill
	 FROM admin a
	 LEFT JOIN employee_view b ON a.username= b.nik WHERE a.id = '".$_SESSION['admin']."'";
		$query = $conn->query($sql);
		$user = $query->fetch_assoc();


	


if (isset($_SESSION["lastActivity"])) {
   if ($_SESSION['lastActivity'] + 3000 * 60 < time()) {
      // last request was more than 30 minutes ago
      session_unset(); // unset $_SESSION variable for the run-time 
      session_destroy(); // destroy session data in storage

      //redirect to your home page
     header('location: ../index.php');

   }
}

$_SESSION["lastActivity"] = time();

?>