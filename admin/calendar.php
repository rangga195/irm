<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Calendar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calendar</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header ">
              <a href="calendar_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
               <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Company</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Date</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT a.id,a.company_code,a.holi_code,a.holi_date,a.description,b.company_name ,c.config_desc AS holi_desc  FROM calendar a
                        LEFT JOIN company b ON a.company_code = b.company_code 
                        LEFT JOIN (SELECT config_code,config_desc FROM configure WHERE CODE='CALENDAR') c ON a.holi_code = c.config_code
                           ";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                         <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['holi_code']; ?></td>  
                           <td><?php echo $row['holi_desc']; ?></td>                      
                          <td><?php echo $row['description']; ?></td>
                         
                           <td><?php echo date('M d, Y', strtotime($row['holi_date']))?></td>
                          <td>
                            <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['description'];?>" href="calendar_edit.php?calendar_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Jobs  <?php echo $row['description'];?>" href="calendar_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['description'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i>
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/calendar_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'calendar_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#calid').val(response.id);
      $('#edit_code').val(response.holi_code);
      $('#datepicker_edit').val(response.holi_date);
      $('#edit_name').val(response.description);
      $('#code_val').val(response.holi_code).html(response.holi_desc);
      $('#del_calid').val(response.id);
      $('#del_calendar').html(response.description);
      $('#company_val').val(response.company_code).html(response.company_name);
    }
  });
}
</script>
</body>
</html>
