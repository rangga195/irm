<?php
	include 'includes/session.php';
	include 'includes/sendEmail-v156.php';
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$no =$_POST['no'];
		$company_code = $_POST['company_code'];
		$nik =$_POST['nik'];
		$leave_type =$_POST['leave_type'];		
		$leave_code =$_POST['leave_code'];
		$start_date =$_POST['start_date'];
		$end_date =$_POST['end_date'];
		$reason =$_POST['reason'];
		$remaining = $_POST['remaining'];
		$user = $user['username'];
		$sql = "SELECT * FROM leave_employee WHERE id = '$id' and status >0 ";

		$query = $conn->query($sql);
		if($query->num_rows >= 1){
			$_SESSION['error'] = 'Data Already Exist OR Status must been Open';
		}
		else
		{

			if ($_FILES['emp_leave']['name']!='')
			{
			$ekstensi_diperbolehkan	= array('png','jpg','jpeg');
			$target_dir='../emp_leave/';
			$file=$_FILES['emp_leave'];
			$type = explode(".", $file["name"]);
			$emp_leave = $nik.'-'.$no.'.'.$type[1];
			$Sqlemp_leave ="UPDATE leave_employee
							set file_upload='$emp_leave'
							where id ='$id' ";
			$conn->query($Sqlemp_leave)	;			

			}	
			if(!empty($emp_leave))
			{
					move_uploaded_file($_FILES['emp_leave']['tmp_name'], '../emp_leave/'.$emp_leave);
					
			}
			$sql = "UPDATE leave_employee 
				SET company_code ='$company_code'
					,nik = '$nik'
					,leave_type ='$leave_type'
					,leave_code ='$leave_code'
					,start_date ='$start_date'
					,end_date ='$end_date'
					,status ='2'
					,reason ='$reason'
					,remaining = '$remaining' 
					,last_update_by ='$user'
					,last_update_date = NOW() WHERE id = '$id' and status = '0'";
		if($conn->query($sql)){

			$leavedata = mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.leave_type,a.leave_code,c.config_desc,a.start_date,a.end_date,a.reason,a.remaining,b.approval_level_1,b.approval_level_2 ,CASE a.approve_by 
                          WHEN  '' 
                          THEN CASE b.approval_level_1 WHEN '' THEN b.approval_level_2 ELSE  b.approval_level_1 END
                          ELSE b.approval_level_1 END AS Approve_To
			FROM leave_employee a 
			LEFT JOIN employee_view b ON a.nik = b.nik 
			LEFT JOIN (SELECT * FROM configure WHERE CODE ='LEAVE') c ON a.leave_code = c.config_code
			WHERE a.nik ='$nik' AND start_date ='$start_date' AND end_date ='$end_date'"));
   
	    	$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
	   			   WHERE a.nik ='".$leavedata['Approve_To']."'"));
	        $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1'  AND TYPE='admin'"));

	    	 $to       = $cekmail['email_office'];
	   		 $subject  = 'Leave Notification to '.$cekmail['nik'].'-'.$cekmail['name'].'';
	   		 $message='<html><body>';
	   		 $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
	   		 $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve from:</span></span>';
	   		 $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
	   		 $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>". $leavedata['no'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Company Name:</strong></td><td>". $leavedata['company_code'] . "</td></tr>";
	   		 $message.="<tr><td><strong>NIK:</strong></td><td>". $leavedata['nik']. "</td></tr>";
	   		 $message.="<tr><td><strong>Employee Name:</strong></td><td>". $leavedata['name'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Division:</strong></td><td>". $leavedata['division_name'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Department:</strong></td><td>". $leavedata['department_name'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Position:</strong></td><td>". $leavedata['description'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Location:</strong></td><td>". $leavedata['loc_name'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Leave Type:</strong></td><td>". $leavedata['leave_type'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Leave:</strong></td><td>". $leavedata['config_desc'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Starting Date:</strong></td><td>". $leavedata['start_date']. "</td></tr>";
	   		 $message.="<tr><td><strong>Ending Date:</strong></td><td>". $leavedata['end_date'] . "</td></tr>";
	   		 $message.="<tr><td><strong>Reason Leave:</strong></td><td>". $leavedata['reason']. "</td></tr>";
	   		 $message.="<tr><td><strong>Remaining Leave:</strong></td><td>". $leavedata['remaining']. "</td></tr>";
	   		 $message.= "</table>";
	   		 $message.='<br>Click the following button to:</br>';
	   		 $message.='<p><a href="'.$ambil_smtp['base_url'].'/leave_approve.php?edit=4&id='.$leavedata['id'].'&username1='.$leavedata['approval_level_1'].'&username2='.$leavedata['approval_level_2'].'"><button class="btn btn-sm btn-success">Approve</button></a></p>';	   		 
	   		 $message.='<p><a href="'.$ambil_smtp['base_url'].'/leave_reject.php?edit=100&id='.$leavedata['id'].'&username1='.$leavedata['approval_level_1'].'&username2='.$leavedata['approval_level_2'].'"><button class="btn btn-sm btn-danger">Reject</button></a></p>';	  

	   		  $message.='<div class="footer">';
	          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
	          $message.='               <tr>';
	          $message.='                 <td class="content-block">';
	          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
	          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
	          $message.='                 </td>';
	          $message.='               </tr>';
	          $message.='               <tr>';
	          $message.='                 <td class="content-block powered-by">';
	          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
	          $message.='                 </td>';
	          $message.='               </tr>';
	          $message.='             </table>';
	          $message.='           </div>';
	          $message.='</body></html>';
	   
	   		  
	   		  $sender   = $ambil_smtp['email'];
	   		  $password = $ambil_smtp['password'];
	   		  //$password = base64_decode($ambilsmtp['password']);

			if(email_localhost($to, $subject, $message, $sender, $password))
			    echo "<script>alert('Approval has been sent!')</script>";
			
				else
					 echo "Email sending failed";

			$_SESSION['success'] = 'Leave updated successfully';




		}
		else{
			$_SESSION['error'] = $conn->error;
		}


	}

		
	}
	else{
		$_SESSION['error'] = 'Fill up edit form first';
	}

	header('location:leave.php');

?>