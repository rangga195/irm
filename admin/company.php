<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Company
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <a href="company_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Phone No</th>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT * FROM company";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                           <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['company_name']; ?></td>
                          
                          <td><?php echo $row['address']?></td>
                          <td><?php echo $row['phone_no']; ?></td>
                          <td>
                          

                           <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['company_code'];?>" href="company_edit.php?company_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Employee  <?php echo $row['company_code'];?>" href="company_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['company_code'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                            
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>     <!--<td>".number_format($row['rate'], 2)."</td>-->
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/company_modal.php'; ?>

</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'company_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('.compid').val(response.id);
      $('#edit_code').val(response.company_code);
      $('#edit_name').val(response.company_name);
      $('#edit_address').val(response.address);
      $('#edit_address2').val(response.address2);
      $('#edit_phone').val(response.phone_no);
      $('#edit_vat').val(response.vat_reg_no);
      $('#edit_postcode').val(response.postcode);
      $('#edit_city').val(response.city);
      $('#edit_country').val(response.country);
      $('#edit_series').val(response.no_series);
      $('#del_compid').val(response.id);
      $('#del_company_name').html(response.company_code+' '+response.company_name);

    //   $('#schedule_val').val(response.schedule_id).html(response.time_in+' - '+response.time_out);
    }
  });
}
</script>
</body>
</html>
