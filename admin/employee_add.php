<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
      <?php include 'includes/navbar.php'; ?>
      <?php include 'includes/menubar.php'; ?>
      <script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
      <script src="../aset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
            <h1>
               Add Employee List
            </h1>
            <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
               <li>Employees</li>
               <li class="active">Employee List</li>
            </ol>
         </section>
         <!-- Main content -->
         <section class="content">
            <?php
               if(isset($_SESSION['error'])){
                 echo "
                   <div class='alert alert-danger alert-dismissible'>
                     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                     <h4><i class='icon fa fa-warning'></i> Error!</h4>
                     ".$_SESSION['error']."
                   </div>
                 ";
                 unset($_SESSION['error']);
               }
               if(isset($_SESSION['success'])){
                 echo "
                   <div class='alert alert-success alert-dismissible'>
                     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                     <h4><i class='icon fa fa-check'></i> Success!</h4>
                     ".$_SESSION['success']."
                   </div>
                 ";
                 unset($_SESSION['success']);
               }
               ?>
            <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
            <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="employee_save.php" enctype="multipart/form-data">
               <div class="box box-solid box-primary" >
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="fa fa-user-md"></i> Employee Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="company_code" class="col-sm-2 control-label">Company<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="company_code" id="company_code" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT * FROM company";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <label for="nik" class="col-sm-2 control-label">NIK<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="nik" name="nik" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <label for="nick_name" class="col-sm-2 control-label">Nick Name</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="nick_name" name="nick_name">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="division_code" class="col-sm-2 control-label">Division<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="division_code" id="division_code" onchange="loadDepartment();" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT * FROM division";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['division_code']."'>".$comrow['division_name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <label for="department_code" class="col-sm-2 control-label">Department<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="department_code" id="department_code" required>
                              <option value="" selected>- Select -</option>
                              <!-- <?php
                                 $sql = "SELECT * FROM division";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['division_code']."'>".$comrow['division_name']."</option>
                                   ";
                                 }
                                 ?> -->
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="position_code" class="col-sm-2 control-label">Position<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="position_code" id="position_code" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT * FROM position";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['position_code']."'>".$comrow['description']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <label for="job_code" class="col-sm-2 control-label">Jobs<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="job_code" id="job_code" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT * FROM jobs";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['job_code']."'>".$comrow['name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="parent_position" class="col-sm-2 control-label">Parent Position</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="parent_position" name="parent_position">
                        </div>
                        <label for="location_code" class="col-sm-2 control-label">Location<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="location_code" id="location_code" onchange="loadGroup();" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT * FROM location";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="group_code" class="col-sm-2 control-label">Working Group</label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="group_code" id="group_code">
                              <option value="" selected>- Select -</option>
                              <!-- <?php
                                 $sql = "SELECT * FROM schedules_group";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['group_code']."'>".$comrow['group_name']."</option>
                                   ";
                                 }
                                 ?> -->
                           </select>
                        </div>
                        <label for="gender" class="col-sm-2 control-label">Gender<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="gender" id="gender" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='GENDER'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Status Employee<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="status" id="status" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='STATUS'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <label for="status_work" class="col-sm-2 control-label">Status Working<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="status_work" id="status_work" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='STATUS WK'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="email_office" class="col-sm-2 control-label">Email Office</label>
                        <div class="col-sm-3">
                           <input type="email" class="form-control " id="email_office" name="email_office" >
                        </div>
                        <label for="email_personal" class="col-sm-2 control-label">Email Personal</label>
                        <div class="col-sm-3">
                           <input type="email" class="form-control " id="email_personal" name="email_personal" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="entry_date" class="col-sm-2 control-label">Join Date<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <div class="date">
                              <input type="text" class="form-control" id="entry_date" name="entry_date" required="">
                           </div>
                        </div>
                        <label for="resign_date" class="col-sm-2 control-label">Resign Date</label>
                        <div class="col-sm-3">
                           <div class="date">
                              <input type="text" class="form-control " id="resign_date" name="resign_date">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="bpjs_tk_no" class="col-sm-2 control-label">BPJS TK No.</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="bpjs_tk_no" name="bpjs_tk_no" >
                        </div>
                        <label for="bpjs_tk_date" class="col-sm-2 control-label">BPJS TK Date</label>
                        <div class="col-sm-3">
                           <div class="date">
                              <input type="text" class="form-control" id="bpjs_tk_date" name="bpjs_tk_date">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="bpjs_ks_no" class="col-sm-2 control-label">BPJS KS No.</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="bpjs_ks_no" name="bpjs_ks_no" >
                        </div>
                        <label for="bpjs_ks_date" class="col-sm-2 control-label">BPJS KS Date</label>
                        <div class="col-sm-3">
                           <div class="date">
                              <input type="text" class="form-control " id="bpjs_ks_date" name="bpjs_ks_date">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="npwp_no" class="col-sm-2 control-label">NPWP No.</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="npwp_no" name="npwp_no" data-inputmask='"mask": "99.999.999.9-999.999"' data-mask >
                        </div>
                        <label for="ptkp_status" class="col-sm-2 control-label">PTKP Status<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="ptkp_status" id="ptkp_status" required="">
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='PTKP'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Other Employee Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="ktp_no" class="col-sm-2 control-label">KTP No.<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="ktp_no" name="ktp_no" required>
                        </div>
                        <label for="kk_no" class="col-sm-2 control-label">KK No.<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="kk_no" name="kk_no" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="sim_no" class="col-sm-2 control-label">SIM No.</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control" id="sim_no" name="sim_no" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="birth_place" class="col-sm-2 control-label">Birth Place<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="birth_place" name="birth_place" required>
                        </div>
                        <label for="birth_date" class="col-sm-2 control-label">Birth Date<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <div class="date">
                              <input type="text" class="form-control" id="birth_date" name="birth_date" required>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="age" class="col-sm-2 control-label">Age</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control" id="age" name="age">
                        </div>
                        <label for="height" class="col-sm-2 control-label">Height</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control" id="height" name="height" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="weight" class="col-sm-2 control-label">Weight</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="weight" name="weight" >
                        </div>
                        <label for="size" class="col-sm-2 control-label">Size</label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="size" id="size" >
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='SIZE'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="shoe_size" class="col-sm-2 control-label">Shoe Size</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="shoe_size" name="shoe_size" >
                        </div>
                        <label for="religion" class="col-sm-2 control-label">Religion<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="religion" id="religion" >
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='RELIGION'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="phone_no" class="col-sm-2 control-label">Phone No.<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="phone_no" name="phone_no" required>
                        </div>
                        <label for="phone_no2" class="col-sm-2 control-label">Phone No. 2</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="phone_no2" name="phone_no2" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="phone_home" class="col-sm-2 control-label">Phone Home</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="phone_home" name="phone_home" >
                        </div>
                        <label for="blood_type" class="col-sm-2 control-label">Blood Type</label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="blood_type" id="blood_type" >
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='BLOOD'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="marital_status" class="col-sm-2 control-label">Marital Status<label class="text-danger">*</label></label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="marital_status" id="marital_status" required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='MARITAL'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <label for="partner_name" class="col-sm-2 control-label">Husband/Wife Name</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="partner_name" name="partner_name" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="phone_no_partner" class="col-sm-2 control-label">Phone No. Husband/Wife</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="phone_no_partner" name="phone_no_partner" >
                        </div>
                        <label for="child" class="col-sm-2 control-label">Child</label>
                        <div class="col-sm-3">
                           <input type="number" class="form-control " id="child" name="child" >
                        </div>
                     </div>
                      <div class="form-group">
                        <label for="father_name" class="col-sm-2 control-label">Father Name</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="father_name" name="father_name" >
                        </div>
                         <label for="phone_no_father" class="col-sm-2 control-label">Phone No. Father</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="phone_no_father" name="phone_no_father" >
                        </div>
                     </div>
                       <div class="form-group">
                        <label for="mother_name" class="col-sm-2 control-label">Mother Name</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="mother_name" name="mother_name" required >
                        </div>
                         <label for="phone_no_mother" class="col-sm-2 control-label">Phone No. Mother</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="phone_no_mother" name="phone_no_mother" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="near_family" class="col-sm-2 control-label">Family Name</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="near_family" name="near_family" required>
                        </div>
                        <label for="phone_no_family" class="col-sm-2 control-label">Family Phone No.</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="phone_no_family" name="phone_no_family" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="address_family" class="col-sm-2 control-label">Family Address</label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control " id="address_family" name="address_family" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="hobby" class="col-sm-2 control-label">Hobby</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="hobby" name="hobby" >
                        </div>
                        <label for="language_skill" class="col-sm-2 control-label">Language Skill</label>
                        <div class="col-sm-3">
                           <input type="text" class="form-control " id="language_skill" name="language_skill" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="vehicle_status" class="col-sm-2 control-label">Vehicle Status</label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="vehicle_status" id="vehicle_status" >
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='VEHICLE'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                        <label for="vehicle_type" class="col-sm-2 control-label">Vehicle Type</label>
                        <div class="col-sm-3">
                           <select class="form-control select2" name="vehicle_type" id="vehicle_type" >
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='VEHI TYPE'";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['config_code']."'>".$comrow['config_code']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Photo Employee & Other Image 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="emp_photo" class="col-sm-2 control-label">Photo</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_photo" name="emp_photo">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="emp_kk" class="col-sm-2 control-label">KK</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_kk" name="emp_kk">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="emp_ktp" class="col-sm-2 control-label">KTP</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_ktp" name="emp_ktp">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="emp_npwp" class="col-sm-2 control-label">NPWP</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_npwp" name="emp_npwp">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="emp_sima" class="col-sm-2 control-label">SIM A</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_sima" name="emp_sima">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="emp_simc" class="col-sm-2 control-label">SIM C</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_simc" name="emp_simc">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="emp_siml" class="col-sm-2 control-label">SIM Others</label>
                        <div class="col-sm-9">
                           <input type="file" id="emp_siml" name="emp_siml">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Address Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="address" name="address" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="provinsi" class="col-sm-2 control-label">Province</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="provinsi" id="provinsi"  onchange='loadKab();loadKec();loadKel();loadKodepos();'  required>
                              <option value=""  selected>- Select -</option>
                              <?php
                                 $sql = "SELECT provinsi FROM postcode
                                         GROUP BY provinsi
                                         ";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['provinsi']."'>".$comrow['provinsi']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="kabupaten_kota" class="col-sm-2 control-label">District</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="kabupaten_kota" id="kabupaten_kota" onchange='loadKec()'  required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="kecamatan" class="col-sm-2 control-label">Sub -District</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="kecamatan" id="kecamatan"  onchange='loadKel();'  required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="kelurahan" class="col-sm-2 control-label">Region</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="kelurahan" id="kelurahan" onchange='loadKodepos();'  required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="post_code" class="col-sm-2 control-label">Post Code</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="post_code" id="post_code" required>
                              <option value="" selected>- Select -</option>
                              <!-- <?php
                                 $sql = "SELECT kodepos FROM postcode
                                         GROUP BY kodepos
                                 
                                         ";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['kodepos']."'>".$comrow['kodepos']."</option>
                                   ";
                                 }
                                 ?> -->
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Address Domicile Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="address2" class="col-sm-2 control-label">Address Domicile</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="address2" name="address2" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="provinsi2" class="col-sm-2 control-label">Province Domicile</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="provinsi2" id="provinsi2" onchange='loadKabDom();loadKecDom();loadKelDom();loadKodeposDom();' required>
                              <option value="" selected>- Select -</option>
                              <?php
                                 $sql = "SELECT provinsi FROM postcode
                                         GROUP BY provinsi
                                 
                                         ";
                                 $query = $conn->query($sql);
                                 while($comrow = $query->fetch_assoc()){
                                   echo "
                                     <option value='".$comrow['provinsi']."'>".$comrow['provinsi']."</option>
                                   ";
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="kabupaten_kota2" class="col-sm-2 control-label">District Domicile</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="kabupaten_kota2" id="kabupaten_kota2" onchange="loadKecDom();" required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="kecamatan2" class="col-sm-2 control-label">Sub -District Domicile</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="kecamatan2" id="kecamatan2" onchange="loadKelDom();"  required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="kelurahan2" class="col-sm-2 control-label">Region Domicile</label>
                        <div class="col-sm-9">
                           <select class="form-control select2" name="kelurahan2" id="kelurahan2" onchange="loadKodeposDom();" required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="post_code2" class="col-sm-2 control-label">Post Code Domicile</label>
                        <div class="col-sm-9">
                           <select class="form-control  select2" name="post_code2" id="post_code2" required>
                              <option value="" selected>- Select -</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Family Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="near_family" class="col-sm-2 control-label">Family Name</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="near_family" name="near_family" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="phone_no_family" class="col-sm-2 control-label">Family Phone No.</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="phone_no_family" name="phone_no_family" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="address_family" class="col-sm-2 control-label">Family Address</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="address_family" name="address_family" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="father_name" class="col-sm-2 control-label">Father Name</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="father_name" name="father_name" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="phone_no_father" class="col-sm-2 control-label">Phone No. Father</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="phone_no_father" name="phone_no_father" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="mother_name" class="col-sm-2 control-label">Mother Name</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control " id="mother_name" name="mother_name" required >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="phone_no_mother" class="col-sm-2 control-label">Phone No. Mother</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="phone_no_mother" name="phone_no_mother" >
                        </div>
                     </div>
                  </div>
               </div> -->
               <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Experience Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <?php
                        for ($i= 1; $i <= 1; $i++) { ?>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Job Experience</label>
                        <div>
                           <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Jobs Position" placeholder="Jobs Position"   name="job[]"   >
                           </div>
                           <div class="col-sm-3">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Company" placeholder="Company"   name="company[]" >
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearin[]">
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearout[]" >
                              </div>
                           </div>
                        </div>
                        <!--  <a href="#" class="remove-box btn btn-danger btn-sm "><i class="fa fa-remove"> </i></a> -->
                     </div>
                     <?php
                        }
                        ?> 
                     <div class="form-group">
                        <div id="main">
                           <div class="my-form">
                              <center>
                                 <p class="text-box">
                                    <a class="add-box btn btn-primary btn-flat" href="#" id > <i class="fa fa-plus"> </i> Add More</a>
                                 </p>
                              </center>
                           </div>
                        </div>
                        <script type="text/javascript">
                           jQuery(document).ready(function($){
                               $('.my-form .add-box').click(function(){
                                   var n = $('.text-box').length + 1;
                                   if( 5 < n ) {
                                       alert('Stop it!');
                                       return false;
                                   }
                                   var box_html = $('<div class="form-group"><label class="col-sm-2 control-label">Job Experience</label><div><div class="col-sm-2"> <input type="text" class="form-control"  data-toggle="tooltip" title="Jobs Position" placeholder="Jobs Position"   name="add_job[]"   ></div><div class="col-sm-3"><input type="text" class="form-control"  data-toggle="tooltip" title="Company" placeholder="Company"   name="add_company[]" ></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearin[]"></div></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearout[]" ></div></div></div><a href="#" class="remove-box btn btn-danger btn-sm btn-flat"><i class="fa fa-remove"> </i></a></div>');
                                   box_html.hide();
                                   $('.my-form p.text-box:last').after(box_html);
                                   box_html.fadeIn('slow');
                                   return false;
                               });
                               $('.my-form').on('click', '.remove-box', function(){
                                   $(this).parent().css( 'background-color', '#' );
                                   $(this).parent().fadeOut("slow", function() {
                                       $(this).remove();
                                       $('.box-number').each(function(index){
                                           $(this).text( index + 1 );
                                       });
                                   });
                                   return false;
                               });
                           });
                        </script>
                     </div>
                  </div>
               </div>
               <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Education Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     <?php
                        for ($i= 1; $i <= 1; $i++) { ?>
                     <div class="form-group">
                        <label class="col-sm-1 control-label">Education</label>
                        <div>
                           <div class="col-sm-2">
                              <select class="form-control select2" name="school_type[]"  >
                                 <option value="" selected>- Select -</option>
                                 <?php
                                    $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                    $query = $conn->query($sql);
                                    while($comrow = $query->fetch_assoc()){
                                      echo "
                                        <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                                      ";
                                    }
                                    ?>
                              </select>
                           </div>
                           <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Education" placeholder="Education"   name="school_name[]" >
                           </div>
                           <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Education Department" placeholder="Education Department"   name="education_dept[]" >
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearin[]" >
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group">
                                 <div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div>
                                 <input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="yearout[]" >
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php
                        }
                        ?> 
                     <div class="form-group">
                        <div id="main">
                           <div class="form-ku">
                              <center>
                                 <p class="text-itan">
                                    <a class="add-box btn btn-primary btn-flat" href="#" id > <i class="fa fa-plus"> </i> Add More</a>
                                 </p>
                              </center>
                           </div>
                        </div>
                        <script type="text/javascript">
                           jQuery(document).ready(function($){
                               $('.form-ku .add-box').click(function(){
                                   var n = $('.text-itan').length + 1;
                                   if( 5 < n ) {
                                       alert('Stop it!');
                                       return false;
                                   }
                                   var box_html = $('<div class="form-group"><label class="col-sm-1 control-label">Education</label><div><div class="col-sm-2"><select class="form-control select2" name="add_school_type[]" id="school_type" required> <option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                       $query = $conn->query($q);
                                     while ($k =  $query->fetch_assoc()){ ?>
                                     <option value="<?php echo $k['config_code']; ?>"{echo "selected=\"selected\"";};?><?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  <?php echo $k['config_desc'];?>
                                     </option> <?php   } ?>
                                      </select></div><div class="col-sm-2"><input type="text" class="form-control"  data-toggle="tooltip" title="Education" placeholder="Education"   name="add_school_name[]" ></div><div class="col-sm-2"><input type="text" class="form-control"  data-toggle="tooltip" title="Education Department" placeholder="Education Department"   name="add_education_dept[]" ></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearin[]" ></div></div><div class="col-sm-2"><div class="input-group"><div class="input-group-addon">  <i class="fa fa-calendar"> </i>    </div><input type="text" class="form-control"   data-toggle="tooltip" maxlength="4" title="Format: YYYY" placeholder="Year"   name="add_yearout[]" ></div></div></div><a href="#" class="remove-box btn btn-danger btn-sm btn-flat"><i class="fa fa-remove"> </i></a></div>');
                                   box_html.hide();
                                   $('.form-ku p.text-itan:last').after(box_html);
                                   box_html.fadeIn('slow');
                                   return false;
                               });
                               $('.form-ku').on('click', '.remove-box', function(){
                                   $(this).parent().css( 'background-color', '#' );
                                   $(this).parent().fadeOut("slow", function() {
                                       $(this).remove();
                                       $('.box-number').each(function(index){
                                           $(this).text( index + 1 );
                                       });
                                   });
                                   return false;
                               });
                           });
                        </script>
                     </div>
                  </div>
               </div>
                <div class="box box-solid box-primary">
                  <div class="box-header">
                     <h3 class="btn btn disabled box-title">
                        <i class="glyphicon glyphicon-briefcase"></i> Family Information 
                     </h3>
                     <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                     <i class="fa fa-minus"></i></a>
                  </div>
                  <div class="box-body">
                     
                  <?php
                        for ($i= 1; $i <= 1; $i++) { ?>

                        <div class="form-group">
                        <label class="col-sm-1 control-label">Family</label>
                        <div class="col-sm-2">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Family Name" placeholder="Family Name"   name="family_name[]" >
                           </div>
                           <div class="col-sm-1">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="KTP No." placeholder="KTP No."   name="ktp_no_fam[]" >
                           </div>
                           <div class="col-sm-1">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Birth Place" placeholder="Birth Place"   name="birth_place1[]" >
                           </div>
                           <div class="col-sm-1">
                              <div class="date">
                                 <input type="text" class="form-control" data-toggle="tooltip" title="Birth Date" placeholder="Birth Date"   name="birth_date1[]"   >
                              </div>
                           </div>
                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Religion" placeholder="Religion"   name="religion_fam[]"  >
                               <option value=" ">- Select -</option>
                                <?php
                                    $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='RELIGION'";
                                    $query = $conn->query($sql);
                                    while($comrow = $query->fetch_assoc()){
                                      echo "
                                        <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                                      ";
                                    }
                                    ?>
                           </select>
                           </div>
                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Gender" placeholder="Gender"   name="gender_fam[]"  >
                               <option value=" ">- Select -</option>
                               <?php
                                    $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='GENDER'";
                                    $query = $conn->query($sql);
                                    while($comrow = $query->fetch_assoc()){
                                      echo "
                                        <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                                      ";
                                    }
                                    ?>

                           </select>
                           </div>

                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Education" placeholder="Education"   name="education[]"  >
                               <option value=" ">- Select -</option>
                               <?php
                                    $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                    $query = $conn->query($sql);
                                    while($comrow = $query->fetch_assoc()){
                                      echo "
                                        <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                                      ";
                                    }
                                    ?>
                                
                           </select>
                           </div>
                           <div class="col-sm-1">
                              <input type="text" class="form-control"  data-toggle="tooltip" title="Jobs" placeholder="Jobs"   name="jobs[]" >
                           </div>
                           <div class="col-sm-1">
                              <select class="form-control select2"  data-toggle="tooltip" title="Relation" placeholder="Relation"   name="relation[]"  >
                               <option value=" ">- Select -</option>
                               <?php
                                    $sql = "SELECT config_code,config_desc FROM configure WHERE CODE='RELATION'";
                                    $query = $conn->query($sql);
                                    while($comrow = $query->fetch_assoc()){
                                      echo "
                                        <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                                      ";
                                    }
                                    ?>
                                
                           </select>
                           </div>
                            
                        </div>
                      <?php
                    }
                  ?> 
                  <div class="form-group">
                  <div id="main">
                      <div class="form-fam">
                              <center>
                           <p class="text-fam">
                                  <a class="add-box btn btn-primary btn-flat" href="#" id > <i class="fa fa-plus"> </i> Add More</a>
                              </p></center>
                      </div>
                  </div>
                  <script type="text/javascript">
                  jQuery(document).ready(function($){
                      $('.form-fam .add-box').click(function(){
                          var n = $('.text-fam').length + 1;
                          if( 5 < n ) {
                              alert('Stop it!');
                              return false;
                          }
                          var box_html = $('<div class="form-group"><label class="col-sm-1 control-label">Family</label><div class="col-sm-2"><input type="text" class="form-control"  data-toggle="tooltip" title="Family Name" placeholder="Family Name"   name="add_family_name[]" ></div><div class="col-sm-1"><input type="text" class="form-control"  data-toggle="tooltip" title="KTP No." placeholder="KTP No."   name="add_ktp_no_fam[]" "></div><div class="col-sm-1"><input type="text" class="form-control"  data-toggle="tooltip" title="Birth Place" placeholder="Birth Place"   name="add_birth_place[]" ></div><div class="col-sm-1"><div class="date"><input type="text" class="form-control"  name="add_birth_date1[]"  ></div></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Religion" placeholder="Religion"   name="add_religion_fam[]"  <option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELIGION'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>"{echo "selected=\"selected\"";};?><?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>   <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Gender" placeholder="Gender"   name="add_gender_fam[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='GENDER'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>"{echo "selected=\"selected\"";};?><?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>   <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Education" placeholder="Education"   name="add_education[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='SCHOOL'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>"{echo "selected=\"selected\"";};?><?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>   <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><div class="col-sm-1"><input type="text" class="form-control"  data-toggle="tooltip" title="Jobs" placeholder="Jobs"   name="add_jobs[]" ></div><div class="col-sm-1"><select class="form-control select2"  data-toggle="tooltip" title="Relation" placeholder="Relation"   name="add_relation[]"  ><option value=" ">- Select -</option><?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='RELATION'";
                                       $query = $conn->query($q);
                                 while ($k =  $query->fetch_assoc()){ ?>
                                 <option value="<?php echo $k['config_code']; ?>"{echo "selected=\"selected\"";};?><?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>   <?php echo $k['config_desc'];?>
                              </option> <?php   } ?>
                           </select></div><a href="#" class="remove-box btn btn-danger btn-sm btn-flat"><i class="fa fa-remove"> </i></a></div>');
                          box_html.hide();
                          $('.form-fam p.text-fam:last').after(box_html);
                          box_html.fadeIn('slow');
                          return false;
                      });
                      $('.form-fam').on('click', '.remove-box', function(){
                          $(this).parent().css( 'background-color', '#' );
                          $(this).parent().fadeOut("slow", function() {
                              $(this).remove();
                              $('.box-number').each(function(index){
                                  $(this).text( index + 1 );
                              });
                          });
                          return false;
                      });
                  });
                  </script>
               </div>
            </div>
         </div>
               <div class="form-group">
                  <label class="col-sm-4"></label>
                  <div class="col-sm-5">
                     <hr/>
                     <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                     <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                     <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
                  </div>
               </div>
      </div>
      </form>
      </section>
   </div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/employee_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>