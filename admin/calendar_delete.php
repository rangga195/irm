<?php
	include 'includes/session.php';

	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$sql = "DELETE FROM calendar WHERE id = '$id'";
		if($conn->query($sql)){
			$_SESSION['success'] = 'Calendar deleted successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}
	}
	else{
		$_SESSION['error'] = 'Select item to delete first';
	}

	header('location: calendar.php');
	
?>