<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
  
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Employee Entitlement Leave
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Leave</li>
         <li class="active">Entitlement Leave List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" enctype="multipart/form-data" action="employee_entitlement_save.php" name="add_employee_leave" onsubmit="return validateFormadd()">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Entitlement Leave
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <form class="form-horizontal" method="POST" action="leave_add.php">
                <div class="form-group">
                
                <div class="form-group">
                    <label for="company" class="col-sm-2 control-label">Company</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="company_code" id="company_code" onchange="loadEmployee();"required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $company_code =$user['company_code'];
                          $sql = "SELECT * FROM company where company_code  LIKE CASE '$company_code' WHEN 'TACO' THEN '%%' ELSE '$company_code' END";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                     <input type="text"  id="location_code" name="location_code" value="<?php echo $user['location_code'];?>"  hidden>
                </div>
                <div class="form-group">
                    <label for="nik" class="col-sm-2 control-label">NIK</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="nik" id="nik" required>
                        <option value="" selected>- Select -</option>
                        <!-- <?php
                         $loc_code =$user['location_code'];
                          $sql = "SELECT * FROM employee_view   where location_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['nik']."'>".$comrow['nik']."-".$comrow['name']."</option>
                            ";
                          }
                        ?> -->
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="leave_type" class="col-sm-2 control-label">Type</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="leave_type" id="leave_type" required>
                         <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM configure WHERE CODE ='TYPE LEAVE'";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
               
                 <div class="form-group">
                    <label for="start_date" class="col-sm-2 control-label">Start Date</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="start_date" name="start_date" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="end_date" class="col-sm-2 control-label">End Date</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="end_date" name="end_date" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="remaining" class="col-sm-2 control-label">Remaining Leave</label>
                    
                     <div class="col-sm-9">
                     
                      <input type="number" class="form-control " id="remaining" name="remaining"  >
                    </div> 
                </div>
                
                
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="add" href="javascript:history.back()" ><i class="fa fa-save"></i> Save</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/company_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>