<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Positions
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Positions</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>

      <div class="row">
               <div class="col-xs-12">
                  <div class="box box-primary">
                     <div class="box-header">
                        <form method="get" >
                           <div class="col-sm-2">
                             <input type="text" class="form-control" id="search" name="search" placeholder="Search">
                          </div>
                           
                              <div class="col-sm-2">
                               <select class="form-control select2" name="department_code" id="department_code" >
                                  <option value="" selected>- Select Department-</option>
                                  <?php
                                     $sql = "SELECT * FROM department";
                                     $query = $conn->query($sql);
                                     while($comrow = $query->fetch_assoc()){
                                       echo "
                                         <option value='".$comrow['dept_code']."'>".$comrow['name']."</option>
                                       ";
                                     }
                                     ?>
                               </select>
                            </div>
                              
                              <div class="col-sm-2">
                              <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                                 
                                     <?php  
                                    if(isset($_GET['search'])){
                                         @$search =$_GET['search'];
                                         @$department_code =$_GET['department_code'];
                                        
                                        }else{
                                          $search ='';
                                          $department_code='';
                                        }
                                          
                                
                                 ?> 
                             </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <a href="position_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Company</th>
                  <th>Organization</th>
                  <th>Department Code</th>
                  <th>Code</th>
                  <th>Position Title</th>
                   <th>Status</th>
                  <th>Rate per Hour</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    if(isset($_GET['search']))
                      {
                        $search = $_GET['search'];                                    
                        $department_code =$_GET['department_code'];
                        $sql ="SELECT a.id,
                          a.company_code,
                          a.org_code,
                          b.name,
                          a.position_code,
                          a.description,
                          a.rate,
                          a.status,
                          a.department_code,
                          c.name as dept_name 
                          FROM POSITION a
                                  LEFT JOIN organization b ON a.org_code = b.org_code
                                  LEFT JOIN department c ON a.department_code = c.dept_code
                                  WHERE a.description  LIKE CASE '$search' WHEN '' THEN '%%' ELSE '$search' END
                                  AND a.department_code LIKE CASE '$department_code' WHEN '' THEN '%%' ELSE '$department_code' END";
                                 

                        }
                        else
                        {
                         $sql = "SELECT a.id,a.company_code,a.org_code,b.name,a.position_code,a.description,a.rate,a.status,a.department_code,c.name  as dept_name FROM POSITION a
                                  LEFT JOIN organization b ON a.org_code = b.org_code
                                  LEFT JOIN department c ON a.department_code = c.dept_code";
                        }
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                            <td><?php echo $row['company_code']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['dept_name']; ?></td>
                            <td><?php echo $row['position_code']; ?></td>
                            <td><?php echo $row['description']; ?></td>
                            <td><?php echo $row['status']; ?></td>
                            <td><?php echo $row['rate']; ?></td>
                          <td>
                           <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['description'];?>" href="position_edit.php?position_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Position  <?php echo $row['description'];?>" href="position_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['description'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                    <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
         </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/position_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'position_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#posid').val(response.id);
      $('#edit_code').val(response.position_code);
      $('#edit_title').val(response.description);
      $('#edit_rate').val(response.rate);
      $('#del_posid').val(response.id);
      $('#del_position').html(response.description);
      $('#company_val').val(response.company_code).html(response.company_name);
      $('#org_val').val(response.org_code).html(response.name);
    }
  });
}
</script>
</body>
</html>
