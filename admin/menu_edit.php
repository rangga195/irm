<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['menu_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM menu WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Menu List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Configure</li>
         <li class="active">Menu List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="menu_update.php?id=<?php echo $row['id']; ?>">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Menu Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <div class="form-group">
                    <label for="menu_name" class="col-sm-2 control-label">Menu Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="menu_name" name="menu_name" value="<?php echo $row['menu_name'];?>" required>
                    </div>
                </div>
                 
                <div class="form-group">
                    <label for="link" class="col-sm-2 control-label">Link</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="link" name="link" value="<?php echo $row['link'];?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="icon" class="col-sm-2 control-label">Icon</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="icon" name="icon" value="<?php echo $row['icon'];?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_main_menu" class="col-sm-2 control-label">Main Menu</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="is_main_menu" name="is_main_menu" value="<?php echo $row['is_main_menu'];?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_sub_menu" class="col-sm-2 control-label">Sub Menu</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="is_sub_menu" name="is_sub_menu" value="<?php echo $row['is_sub_menu'];?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_sub_sub_menu" class="col-sm-2 control-label">Sub Sub Menu</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="is_sub_sub_menu" name="is_sub_sub_menu" value="<?php echo $row['is_sub_sub_menu'];?>" >
                    </div>
                </div>

            </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/organization_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>