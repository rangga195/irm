<?php
include 'includes/session_approval.php';
include 'includes/sendEmail-v156.php';

if(($_GET['edit'])&& $_GET['edit']==4)
{
 $data = mysqli_fetch_array(mysqli_query($conn, "SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.transport_code,c.config_desc,a.from_departure,a.to_arrive,a.start_date,a.end_date,a.notes,a.approve_by,b.approval_level_1,b.approval_level_2,
  CASE a.approve_by WHEN  '' THEN b.approval_level_1 ELSE b.approval_level_2 END AS Approve_To,a.status
  FROM travel a 
  LEFT JOIN employee_view b ON a.nik = b.nik 
  LEFT JOIN configure c ON a.transport_code = c.config_code 
  WHERE c.code='TRAVEL'AND a.id='".$_GET['id']."' "));
 $approveTravel1 = $data['approval_level_1'];
 $approveTravel2 = $data['approval_level_2'];
 $approveTravelby =$data['approve_by'];
 $status =$data['status'];
 if (($approveTravelby=='') &&  ($approveTravel1==$_GET['username1']) && ($approveTravel2 ==''))
 {

   $data_update1 ="update travel
   set status =1,
   approve_by ='".$_GET['username1']."',
   last_update_by = '".$_GET['username1']."',
   last_update_date =NOW()
   where id ='".$_GET['id']."'and status =2 ";

   if ($status==2)
   {
    if($conn->query($data_update1))
    {
      $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
       WHERE a.nik ='".$data['nik']."'"));

      $to       = $cekmail['email_office'];
      $subject  = 'Approve Request Itinerary for'.$cekmail['nik'].'-'.$cekmail['name'].'';
      $message='<html><body>';
      $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
      $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Itinerary Request has been approved :</span></span>';
      $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
      $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
      $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
      $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
      $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
      $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
      $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
      $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
      $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
      $message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
      $message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
      $message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
      $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
      $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
      $message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
      $message.= "</table>";
      $message.='<div class="footer">';
      $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
      $message.='               <tr>';
      $message.='                 <td class="content-block">';
      $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
      $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
      $message.='                 </td>';
      $message.='               </tr>';
      $message.='               <tr>';
      $message.='                 <td class="content-block powered-by">';
      $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
      $message.='                 </td>';
      $message.='               </tr>';
      $message.='             </table>';
      $message.='           </div>';
      $message.='</body></html>';


      $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='user'"));
      $sender   = $ambil_smtp['email'];
      $password = $ambil_smtp['password'];

      if(email_localhost($to, $subject, $message, $sender, $password))
      {
        echo "<script>alert('Request has been approved and email has been sent!')</script>";
      }

      else
      {
        echo "<script>alert('Email sending failed...')</script>";
      } 
    }
  }
  elseif ($status==1)
  {
    echo "<script>alert('Data Has been Approved...')</script>";
  }
  elseif ($status==0)
  {
   echo "<script>alert('Data Has been Rejected...')</script>";
 }


}
if (($approveTravelby=='') &&  ($approveTravel1=='') && ($approveTravel2 ==$_GET['username2']))
{

 $data_update1 ="update travel
 set status =1,
 approve_by ='".$_GET['username2']."',
 last_update_by = '".$_GET['username2']."',
 last_update_date =NOW()
 where id ='".$_GET['id']."'and status =2 ";

 if ($status==2)
 {
  if($conn->query($data_update1))
  {
    $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
     WHERE a.nik ='".$data['nik']."'"));

    $to       = $cekmail['email_office'];
    $subject  = 'Approve Request Itinerary for'.$cekmail['nik'].'-'.$cekmail['name'].'';
    $message='<html><body>';
    $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
    $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Itinerary Request has been approved :</span></span>';
    $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
    $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
    $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
    $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
    $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
    $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
    $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
    $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
    $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
    $message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
    $message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
    $message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
    $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
    $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
    $message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
    $message.= "</table>";
    $message.='<div class="footer">';
    $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
    $message.='               <tr>';
    $message.='                 <td class="content-block">';
    $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
    $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
    $message.='                 </td>';
    $message.='               </tr>';
    $message.='               <tr>';
    $message.='                 <td class="content-block powered-by">';
    $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
    $message.='                 </td>';
    $message.='               </tr>';
    $message.='             </table>';
    $message.='           </div>';
    $message.='</body></html>';


    $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='user'"));
    $sender   = $ambil_smtp['email'];
    $password = $ambil_smtp['password'];

    if(email_localhost($to, $subject, $message, $sender, $password))
    {
      echo "<script>alert('Request has been approved and email has been sent!')</script>";
    }

    else
    {
     echo "<script>alert(Email sending failed..)</script>"; 
   }


 }
 else
 {
  echo "<script>alert(Email sending failed..)</script>"; 
}  

}
elseif ($status==1)
{
  echo "<script>alert('Data Has been Approved...')</script>";
}
elseif ($status==0)
{
 echo "<script>alert('Data Has been Rejected...')</script>";
}


}
if (($approveTravelby=='') &&  ($approveTravel1==$_GET['username1']) && ($approveTravel2 ==$_GET['username2']))
{
  $data_update1 ="update travel
  set approve_by ='".$_GET['username1']."',
  last_update_by = '".$_GET['username1']."',
  last_update_date =NOW()
  where id ='".$_GET['id']."'and status =2 ";
  if ($status==2)
  { 
    if($conn->query($data_update1))
    {
      $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
       WHERE a.nik ='".$data['approval_level_2']."'"));

      $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='user'"));

      $to       = $cekmail['email_office'];
      $subject  = 'Itinerary Approval Notification to '.$cekmail['nik'].'-'.$cekmail['name'].'';
      $message='<html><body>';
      $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
      $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve Itinerary from the following employees :</span></span>';
      $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
      $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
      $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
      $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
      $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
      $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
      $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
      $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
      $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
      $message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
      $message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
      $message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
      $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
      $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
      $message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
      $message.= "</table>";
      $message.='<br>Click the following button to:</br>';
      $message.='<p><a href="'.$ambil_smtp['base_url'].'/travel_approve.php?edit=4&id='.$data['id'].'&username2='.$data['approval_level_2'].'&username1='.$data['approval_level_1'].'"><button class="btn btn-sm btn-success">Approve</button></a></p>';

      $message.='<p><a href="'.$ambil_smtp['base_url'].'/travel_reject.php?edit=100&id='.$data['id'].'&username2='.$data['approval_level_2'].'&username1='.$data['approval_level_1'].'"><button class="btn btn-sm btn-danger">Reject</button></a></p>';

      $message.='<div class="footer">';
      $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
      $message.='               <tr>';
      $message.='                 <td class="content-block">';
      $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
      $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
      $message.='                 </td>';
      $message.='               </tr>';
      $message.='               <tr>';
      $message.='                 <td class="content-block powered-by">';
      $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
      $message.='                 </td>';
      $message.='               </tr>';
      $message.='             </table>';
      $message.='           </div>';
      $message.='</body></html>';


      $sender   = $ambil_smtp['email'];
      $password = $ambil_smtp['password'];
                          //$password = base64_decode($ambilsmtp['password']);

      if(email_localhost($to, $subject, $message, $sender, $password))
      {
       echo "<script>alert('Request has been approved and email has been sent!')</script>";
     }

     else
     {
       echo "<script>alert(Email sending failed..)</script>";
     }

   }
   else
   {
    echo "<script>alert(Email sending failed..)</script>"; 
  }  
}
elseif ($status==1)
{
  echo "<script>alert('Data Has been Approved...')</script>";
}
elseif ($status==0)
{
 echo "<script>alert('Data Has been Rejected...')</script>";
}


}

if (($approveTravelby!='') &&  ($approveTravel1==$_GET['username1']) && ($approveTravel2 ==$_GET['username2']))
{
  $data_update2 ="update travel
  set status =1,
  approve_by ='".$_GET['username2']."',
  last_update_by = '".$_GET['username2']."',
  last_update_date =NOW()
  where id ='".$_GET['id']."' and status =2 ";

  if($status==2)
  {
    if($conn->query($data_update2))
    {

     $cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
       WHERE a.nik ='".$data['nik']."'"));

     $to       = $cekmail['email_office'];
     $subject  = 'Approve Request Itinerary for '.$cekmail['nik'].'-'.$cekmail['name'].'';
     $message='<html><body>';
     $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
     $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">Your Itinerary Request has been approved :</span></span>';
     $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
     $message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
     $message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
     $message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
     $message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
     $message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
     $message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
     $message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
     $message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
     $message.="<tr><td><strong>Transport:</strong></td><td>" . $data['config_desc']. "</td></tr>";
     $message.="<tr><td><strong>Departure:</strong></td><td>" . $data['from_departure']. "</td></tr>";
     $message.="<tr><td><strong>Arrival:</strong></td><td>" . $data['to_arrive']. "</td></tr>";
     $message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
     $message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
     $message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
     $message.= "</table>";

     $message.='<div class="footer">';
     $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
     $message.='               <tr>';
     $message.='                 <td class="content-block">';
     $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
     $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
     $message.='                 </td>';
     $message.='               </tr>';
     $message.='               <tr>';
     $message.='                 <td class="content-block powered-by">';
     $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
     $message.='                 </td>';
     $message.='               </tr>';
     $message.='             </table>';
     $message.='           </div>';
     $message.='</body></html>';

     $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='user'"));
     $sender   = $ambil_smtp['email'];
     $password = $ambil_smtp['password'];

     if(email_localhost($to, $subject, $message, $sender, $password))
     {
      echo "<script>alert('Request has been approved and email has been sent!')</script>";
    }
    else
    {
     echo "<script>alert(Email sending failed..)</script>"; 
   }

 }
 else
 {
   echo "<script>alert(Email sending failed..)</script>"; 
 }  
}
elseif ($status==1)
{
  echo "<script>alert('Data Has been Approved...')</script>";
}
elseif ($status==0)
{
 echo "<script>alert('Data Has been Rejected...')</script>";
}


}
echo "<script type='text/javascript'>
setTimeout(function() { 
 window.location.href = 'http://192.168.6.76';
 },2000);
 </script>";
}
?>