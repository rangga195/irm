<?php
	include 'includes/session.php';

	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$sql = "DELETE FROM closing_period WHERE id = '$id'";
		if($conn->query($sql)){
			$_SESSION['success'] = 'Closing Period has been deleted successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}
	}
	else{
		$_SESSION['error'] = 'Select item to delete first';
	}

	header('location: closing_period.php');
	
?>