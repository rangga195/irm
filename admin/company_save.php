<?php
	include 'includes/session.php';

	if(isset($_POST['add'])){
		$code = $_POST['code'];
		$name = $_POST['name'];
		$address = $_POST['address'];
		$address2 = $_POST['address2'];
		$phone = $_POST['phone'];
		$vat = $_POST['vat'];
		$postcode = $_POST['postcode'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$series = $_POST['series'];
		$user = $user['username'];

		if ($_FILES['images']['name']!=''){
		$target_dir='../images/';
		$file=$_FILES['images'];
		$type = explode(".", $file["name"]);
		$images = $code.'.'.$type[1];	
		}	
		if(!empty($images)){
			move_uploaded_file($_FILES['images']['tmp_name'], '../images/'.$images);	
		}
		//creating employeeid
		$letters = '';
		$numbers = '';
		foreach (range('A', 'Z') as $char) {
		    $letters .= $char;
		}
		for($i = 0; $i < 10; $i++){
			$numbers .= $i;
		}
		$employee_id = substr(str_shuffle($letters), 0, 3).substr(str_shuffle($numbers), 0, 9);
		//
		$sql = "INSERT INTO company (company_code, company_name, address, address2, phone_no, vat_reg_no, postcode, city, country, no_series, img_name,create_by,create_date) 
				VALUES ('$code', '$name', '$address', '$address2', '$phone', '$vat', '$postcode', '$city', '$country', '$series','$images','$user', NOW())";
		if($conn->query($sql)){
			$_SESSION['success'] = 'Company added successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}

	}
	else{
		$_SESSION['error'] = 'Fill up add form first';
	}

	header('location: company.php');
?>