<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
  include '../timezone.php';
   $range_to = date('Y-m-d');
   $range_from = date('Y-m-01');//date('Y-m-d', strtotime('-30 day', strtotime($range_to)));
?>
  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bussiness Trip Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Bussiness Trip</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
              <a href="trip_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
               <div class="pull-right">
                <form method="get" class="form-inline" >
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input autocomplete="off" type="text" class="form-control " id="start_date" name="start_date" value="<?php echo  $range_from; ?>" >
                  </div>
                  <input autocomplete="off" type="text" class="form-control " id="end_date" name="end_date" value="<?php echo  $range_to; ?>" >
                   <select class="form-control select2" name="location_code" id="location_code" >
                        <option value="" selected>- Select Location -</option>
                        <?php
                           $loc_code =$user['location_code'];
                           $sql = "SELECT * FROM location  
                           where loc_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END";
                           $query = $conn->query($sql);
                           while($comrow = $query->fetch_assoc()){
                             echo "
                               <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                             ";
                           }
                           ?>
                     </select>
                  <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                  
                </form>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>No.</th>
                  <th>Company</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>Location</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Check In</th>
                  <th>Check Out</th>
                  <th>Destination</th>
                  <th>Notes</th>
                  <th>Status</th>
                  <th>Approve By</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                   if(isset($_GET['start_date']))
                      {
                        $start_date = $_GET['start_date'];
                        $end_date = $_GET['end_date'];
                        $loc_code =$_GET['location_code'];
                        $sql = "SELECT a.id,a.no
                                 ,a.company_code
                                 ,a.nik
                                 ,b.name
                                 ,b.loc_name
                                 ,a.start_date
                                 ,a.end_date
                                 ,a.check_in
                                 ,a.check_out
                                 ,a.notes
                                 ,a.destination
                                 ,CASE a.status WHEN 0 THEN 'Open'
                                                WHEN 1 THEN 'Approve'
                                                WHEN 2 THEN 'Pending Approval' END AS status
                                 ,a.approve_by
                            FROM trip a 
                          LEFT JOIN employee_view b ON a.nik = b.nik
                          where b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END 
                          and a.start_date between '$start_date' and '$end_date'";
                        }
                        else 
                        {
                          $start_date = $range_from;
                          $end_date = $range_to;
                          $loc_code =$user['location_code'];
                           $sql = "SELECT a.id,a.no
                                 ,a.company_code
                                 ,a.nik
                                 ,b.name
                                 ,b.loc_name
                                 ,a.start_date
                                 ,a.end_date
                                 ,a.check_in
                                 ,a.check_out
                                 ,a.notes
                                 ,a.destination
                                 ,CASE a.status WHEN 0 THEN 'Open'
                                                WHEN 1 THEN 'Approve'
                                                WHEN 2 THEN 'Pending Approval' END AS status
                                 ,a.approve_by
                            FROM trip a 
                          LEFT JOIN employee_view b ON a.nik = b.nik
                          where b.location_code LIKE CASE '$loc_code' WHEN '12' THEN '%%' ELSE '$loc_code' END 
                          and a.start_date between '$start_date' and '$end_date'";
                        }
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      // $status = ($row['status'])?'<span class="label label label-danger ">Pending Approval</span>':'<span class="label label-success">Approve</span>';
                      ?>
                        <tr>
                           <td><?php echo $row['no']; ?></td>
                           <td><?php echo $row['company_code']; ?></td>
                           <td><?php echo $row['nik']; ?></td>
                           <td><?php echo $row['name']; ?></td>
                           <td><?php echo $row['loc_name']; ?></td>
                           <td><?php echo $row['start_date']; ?></td>
                           <td><?php echo $row['end_date']; ?></td>
                           <td><?php echo $row['check_in']; ?></td>
                           <td><?php echo $row['check_out']; ?></td>
                           <td><?php echo $row['destination']; ?></td>
                           <td><?php echo $row['notes']; ?></td>
                           <td><?php 
                                            if($row['status'] == 'Open'){
                                echo '<span class="label label-danger btn-flat">Open</span>';
                              }
                                            else if ($row['status'] == 'Pending Approval' ){
                                echo '<span class="label label-warning btn-flat">Pending Approval</span>';
                              }
                                            else if ($row['status'] == 'Approve' ){
                                echo '<span class="label label-success btn-flat">Approve</span>';
                              }
                             ?></td>
                           <td><?php echo $row['approve_by']; ?></td>
                          <td>
                            <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['no'];?>" href="trip_edit.php?trip_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Bussiness Trip  <?php echo $row['no'];?>" href="trip_delete.php?id=<?php echo $row['id']; ?>"  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['no'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/jobs_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'jobs_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#jobsid').val(response.id);
      $('#edit_code').val(response.job_code);
      $('#edit_name').val(response.name);
      $('#del_jobsid').val(response.id);
      $('#del_jobs').html(response.name);
      $('#company_val').val(response.company_code).html(response.company_name);
    }
  });
}
</script>
</body>
</html>
