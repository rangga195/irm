<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['smtp_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM smtp WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit SMTP List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>SMTP</li>
         <li class="active">SMTP List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="smtp_update.php?id=<?php echo $row['id']; ?>">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Jobs Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <div class="form-group">
                  <label for="company_code" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="company_code" id="company_code"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM company";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['company_code']; ?>" <?php if(($k['company_code'])== ($row['company_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['company_code']==$k['company_code'])?print(" "):print(""); ?>  > <?php echo $k['company_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              <div class="form-group">
                    <label for="edit_email" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="edit_email" name="email" value="<?php echo $row['email'];?>"  required>
                    </div>
                </div>
               
                <div class="form-group">
                    <label for="edit_pass" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_pass" name="password" value="<?php echo $row['password'];?>" >
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_base_url" class="col-sm-2 control-label">Base Url</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="edit_base_url" name="edit_base_url" value="<?php echo $row['base_url'];?>"  required>
                    </div>
                </div>
                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="status" id="status" required>
                       <!--  <option value="" selected>- Select -</option> -->
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE='STATUS SMT'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['status']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>

            </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/organization_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>