<?php
  session_start();
  
?>
<?php include 'header.php'; ?>
<link rel="icon" href="images/images.jpg">
<body class="hold-transition login-page">
<div class="login-box">
  	<div class="login-logo">
  		<b>TACONNECT SYSTEM</b>
  	</div>
  
  	<div class="login-box-body">
    	<p class="login-box-msg">TACONNECT MANAGEMENT SYSTEM</p>

    	<form action="login.php" method="POST">
      		<div class="form-group has-feedback">
        		<input type="text" class="form-control" name="username" placeholder="NIK" required autofocus>
        		<span class="glyphicon glyphicon-user form-control-feedback"></span>
      		</div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
      		<div class="row">
    			<div class="col-xs-12">
          			<button type="submit" class="btn btn-primary btn-block btn-flat" name="login"><i class="fa fa-sign-in"></i> Sign In</button>
        		</div>

      		</div>

    	</form>
      <center><h5 class="form-signin">Copyright &copy; <a href="#" data-toggle="modal" data-target="#contact">TACO GROUP</a> 2019</h5></center> 
    <!--   <a href="#">I forgot my password</a><br> -->
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->
  	</div>
  	<?php
  		if(isset($_SESSION['error'])){
  			echo "
  				<div class='callout callout-danger text-center mt20'>
			  		<p>".$_SESSION['error']."</p> 
			  	</div>
  			";
  			unset($_SESSION['error']);
  		}
  	?>
</div>
	
<?php include 'scripts.php' ?>
</body>
</html>