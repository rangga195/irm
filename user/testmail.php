<?php
    include 'includes/session.php';
    include 'includes/sendEmail-v156.php';

    $nik='C12910267';
		$leave_code='1';
		$start_date='2019-03-20';
		$end_date='2019-03-26';
		$reason='tes';
		$remaining = '0';

    $leavedata = mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.leave_code,c.config_desc,a.start_date,a.end_date,a.reason,a.remaining,b.approval_level_1,b.approval_level_2 
		FROM leave_employee a 
		LEFT JOIN employee_view b ON a.nik = b.nik 
		LEFT JOIN configure c ON a.leave_code = c.config_code
		WHERE a.nik ='C12910267' AND start_date ='2019-03-28' AND end_date ='2019-03-29'
		AND c.code ='LEAVE'"));
   
    	$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
   			   WHERE a.nik ='A11790035'"))	;
   	
   		 $to       = $cekmail['email_office'];
   		 $subject  = 'Leave Approval Notification to '.$cekmail['nik'].'-'.$cekmail['name'].'';
   		 $message='<html><body>';
   		 $message.='<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">';
   		 $message.='<style>body {font: 400 11px/1.8 Calibri;font-family: Calibri;font-size:11.0pt;color: #777;}table {border-collapse: collapse !important;border-spacing: 0;border: 1px solid #ddd;padding:0;}th, td {text-align: left;}tr:nth-child(even) {background-color: #f2f2f2}.container {padding: 80px 120px;}</style>';
   		 
   		 $message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
   		 $message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve leave from the following employees :</span></span>';
   		 $message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
   		 $message.="<tr style='background: #eee;'><td><strong>No.:        </strong></td><td>" . $leavedata['id'] . "</td></tr>";
   		 $message.="<tr><td><strong>Company Name:     </strong></td><td>" . $leavedata['company_code'] . "</td></tr>";
   		 $message.="<tr><td><strong>NIK:              </strong></td><td>" . $leavedata['nik'] . "</td></tr>";
   		 $message.="<tr><td><strong>Employee Name:    </strong></td><td>" . $leavedata['name'] . "</td></tr>";
   		 $message.="<tr><td><strong>Division:         </strong></td><td>" . $leavedata['division_name'] . "</td></tr>";
   		 $message.="<tr><td><strong>Department:       </strong></td><td>" . $leavedata['department_name'] . "</td></tr>";
   		 $message.="<tr><td><strong>Position:         </strong></td><td>" . $leavedata['description'] . "</td></tr>";
   		 $message.="<tr><td><strong>Location:         </strong></td><td>" . $leavedata['loc_name'] . "</td></tr>";
   		 $message.="<tr><td><strong>Reason for Leave: </strong></td><td>" . $leavedata['config_desc'] . "</td></tr>";
   		 $message.="<tr><td><strong>Starting Date:    </strong></td><td>" . $leavedata['start_date']. "</td></tr>";
   		 $message.="<tr><td><strong>Ending Date:      </strong></td><td>" . $leavedata['end_date'] . "</td></tr>";
   		 $message.="<tr><td><strong>Reason Leave:     </strong></td><td>" . $leavedata['reason']. "</td></tr>";
   		 $message.="<tr><td><strong>Remaining Leave:  </strong></td><td>" . $leavedata['remaining']. "</td></tr>";
   		 $message.= "</table>";
   		 // $message.='<br>Click the following button to Approve <span style="font-size: 12.0pt; font-family: Calibri"><a href="'.$cekhost['base_url'].'/taco/fungsi/approve_leave.php?edit=4&id_leave='.$leavedata['id'].'&username='.$cekmail['nik'].'&level_app='.$_GET['level_app'].'"><button class="btn btn-sm btn-success">Approve</button></a></span><br>';
   		 
   		 // $message.='<br>Click the following button to Reject <span style="font-size: 12.0pt; font-family: Calibri"><a href="'.$cekhost['base_url'].'/taco/fungsi/approve_leave.php?edit=4&id_leave='.$leavedata['id'].'&username='.$cekmail['nik'].'&level_app=100"><button class="btn btn-sm btn-danger">Reject</button></a></span>';
   
   		 $message.=' <div class="footer">';
   		 $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
   		 $message.='               <tr>';
   		 $message.='                 <td class="content-block">';
   		 $message.='                   <span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span>';
   		 $message.='                   <span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span>';
   		 $message.='                 </td>';
   		 $message.='               </tr>';
   		 $message.='               <tr>';
   		 $message.='                 <td class="content-block powered-by">';
   		 $message.='                   Powered by <a href="http://taco.co.id">TACO GROUP</a>.';
   		 $message.='                 </td>';
   		 $message.='               </tr>';
   		 $message.='             </table>';
   		 $message.='           </div>';
   		 $message.='</body></html>';
   
   		  $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1'"));
   		  $sender   = $ambil_smtp['email'];
   		  $password = $ambil_smtp['password'];
   		  //$password = base64_decode($ambilsmtp['password']);
   
   		if(email_localhost($to, $subject, $message, $sender, $password))
   		    echo "<script>alert('Approval has been sent!')</script><meta http-equiv='refresh' content='1 url=../index.php?page=bWFzdGVyX2VtcGxveWVlX2xlYXZlX3VzZXI=&edit'>";
   		else
   		    echo "Email sending failed";
   
   		
   
   // // $to ='andi.lestianto@taco.co.id';
   // // $subject ='TEST';
   // // $message ='TEST';
   // // $sender ='andi.lestianto@taco.co.id';		
   // // $password ='7989@HR!';
   
   // // if(email_localhost($to, $subject, $message, $sender, $password))
   // //     echo "<script>alert('Approval has been sent!')</script>";
   
   // // 	else
   // // 		 echo "Email sending failed";
   // $currentDir = getcwd();
   
   //    chdir('sendEmail-v156');
   //   	$send_email = shell_exec('sendEmail.exe -f '.$to.' -t '.$sender.' -cc '.$sender.' -u '.escapeshellarg('tes').' -m '.escapeshellarg($subject ).' -s smtp-local.taco.co.id:25 -xu '.''.' -xp '.escapeshellarg($message).' -o message-content-type=html message-charset=utf-8 tls=yes');
   //    chdir($currentDir);
       
   //    if($send_email){
   //        return true;
   //    }else{
   //        return false;
   //    }
   
   
   
   ?>