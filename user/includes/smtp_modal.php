<!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Add SMTP</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="smtp_add.php">
                <div class="form-group">
                    <label for="company" class="col-sm-3 control-label">Company</label>

                    <div class="col-sm-9">
                      <select class="form-control input-sm" name="company" id="company" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="email" name="email" required>
                    </div>
                </div>

          		  <div class="form-group">
                  	<label for="password" class="col-sm-3 control-label">Password</label>

                  	<div class="col-sm-9">
                    	<input type="text" class="form-control input-sm" id="password" name="password" required>
                  	</div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-3 control-label">Status</label>

                    <div class="col-sm-9"> 
                      <select class="form-control input-sm" name="status" id="status" required>
                        <option value="" selected>- Select -</option>
                        <option value="0">Non Active</option>
                        <option value="1">Active</option>
                      </select>
                    </div>
                </div>
                
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button>
            	</form>
          	</div>
        </div>
    </div>
</div>

<!-- Edit -->
<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Update SMTP</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="smtp_edit.php">
            		<input type="hidden" id="smtpid" name="id">
                <div class="form-group">
                    <label for="edit_comp" class="col-sm-3 control-label">Company</label>
                    <div class="col-sm-9">
                      <select class="form-control input-sm" name="edit_comp" id="edit_comp">
                        <option selected id="company_val"></option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($prow = $query->fetch_assoc()){
                            echo "
                              <option value='".$prow['company_code']."'>".$prow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>

                 
                <div class="form-group">
                    <label for="edit_email" class="col-sm-3 control-label">Email</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_email" name="email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_pass" class="col-sm-3 control-label">Password</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_pass" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_status" class="col-sm-3 control-label">Status</label>

                    <div class="col-sm-9"> 
                      <select class="form-control input-sm" name="edit_status" id="status" required>
                        <option selected id="smtp_val"></option>
                        <option value="0">Non Active</option>
                        <option value="1">Active</option>
                      </select>
                    </div>
                </div>
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-success btn-flat" name="edit"><i class="fa fa-check-square-o"></i> Update</button>
            	</form>
          	</div>
        </div>
    </div>
</div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Deleting...</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="smtp_delete.php">
            		<input type="hidden" id="del_smtpid" name="id">
            		<div class="text-center">
	                	<p>DELETE SMTP</p>
	                	<h2 id="del_smtp" class="bold"></h2>
	            	</div>
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-danger btn-flat" name="delete"><i class="fa fa-trash"></i> Delete</button>
            	</form>
          	</div>
        </div>
    </div>
</div>


     