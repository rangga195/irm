<!-- Modal -->
<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form autocomplete="off" class="form-horizontal" method="POST" action="addEvent.php">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">Add Event</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                    <label for="meeting_room" class="col-sm-2 control-label">Meeting Room</label>

                    <div class="col-sm-10">
                      <select class="form-control select2" name="meeting_room" id="meeting_room" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT config_code,config_desc FROM configure WHERE CODE ='MEETING'";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['config_code']."'>".$comrow['config_desc']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
               <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                     <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                  </div>
               </div>
               <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                     <input type="text" name="description" class="form-control" id="description" placeholder="Description">
                  </div>
               </div>
               <div class="form-group">
                  <label for="color" class="col-sm-2 control-label">Color</label>
                  <div class="col-sm-10">
                     <select name="color" class="form-control" id="color">
                        <option value="">Choose</option>
                        <option style="color:#0071c5;" value="#0071c5">&#9724; Dark blue</option>
                        <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquoise</option>
                        <option style="color:#008000;" value="#008000">&#9724; Green</option>
                        <option style="color:#FFD700;" value="#FFD700">&#9724; Yellow</option>
                        <option style="color:#FF8C00;" value="#FF8C00">&#9724; Orange</option>
                        <option style="color:#FF0000;" value="#FF0000">&#9724; Red</option>
                        <option style="color:#000;" value="#000">&#9724; Black</option>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="start" class="col-sm-2 control-label">Start Date</label>
                  <div class="col-sm-10">
                    <div class="date">
                     <input type="text" class="form-control " id="start" name="start" required>
                   </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="end" class="col-sm-2 control-label">End Date</label>
                  <div class="col-sm-10">
                    <div class="date">
                     <input type="text" class="form-control " id="end" name="end" required>
                   </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="start_time" class="col-sm-2 control-label">Start Time</label>
                  <div class="col-sm-10">
                     <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="start_time" name="start_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="end_time" class="col-sm-2 control-label">End Time</label>
                  <div class="col-sm-10">
                     <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="end_time" name="end_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                     </div>
                  </div>
               </div>

                <div class="form-group">
                  <label for="consume" class="col-sm-2 control-label">Consume</label>
                  <div class="col-sm-10">
                     <input type="text" name="consume" class="form-control" id="consume">
                  </div>
               </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total Consume</label>
                  <div class="col-sm-10">
                     <input type="number" name="total" class="form-control" id="total">
                  </div>
               </div>
                <div class="form-group">
                  <label for="notes" class="col-sm-2 control-label">Notes</label>
                  <div class="col-sm-10">
                     <input type="text" name="notes" class="form-control" id="notes">
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- Modal -->

<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form class="form-horizontal" method="POST" action="editEventTitle.php">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">Edit Event</h4>
            </div>
            <div class="modal-body">
               <div class="form-group">
                  <label for="meeting_room" class="col-sm-2 control-label">Meeting Room</label>
                  <div class="col-sm-10">
                     <select class="form-control select2" name="meeting_room" id="meeting_room"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE ='MEETING'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ("meeting_room"))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                     <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                  </div>
               </div>
               <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                     <input type="text" name="description" class="form-control" id="description" placeholder="Description">
                  </div>
               </div>
               <div class="form-group">
                  <label for="color" class="col-sm-2 control-label">Color</label>
                  <div class="col-sm-10">
                     <select name="color" class="form-control" id="color">
                        <option value="">Choose</option>
                        <option style="color:#0071c5;" value="#0071c5">&#9724; Dark blue</option>
                        <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquoise</option>
                        <option style="color:#008000;" value="#008000">&#9724; Green</option>
                        <option style="color:#FFD700;" value="#FFD700">&#9724; Yellow</option>
                        <option style="color:#FF8C00;" value="#FF8C00">&#9724; Orange</option>
                        <option style="color:#FF0000;" value="#FF0000">&#9724; Red</option>
                        <option style="color:#000;" value="#000">&#9724; Black</option>
                     </select>
                  </div>
               </div>
                <div class="form-group">
                  <label for="start_edit" class="col-sm-2 control-label">Start Date</label>
                  <div class="col-sm-10">
                    <div class="date">
                     <input type="text" class="form-control " id="start_edit" name="start_edit"  required>
                   </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="end_edit" class="col-sm-2 control-label">End Date</label>
                  <div class="col-sm-10">
                    <div class="date">
                     <input type="text" class="form-control " id="end_edit" name="end_edit" required>
                   </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="start_time" class="col-sm-2 control-label">Start Time</label>
                  <div class="col-sm-10">
                     <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="start_time" name="start_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="end_time" class="col-sm-2 control-label">End Time</label>
                  <div class="col-sm-10">
                     <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="end_time" name="end_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                     </div>
                  </div>
               </div>
                <div class="form-group">
                  <label for="consume" class="col-sm-2 control-label">Consume</label>
                  <div class="col-sm-10">
                     <input type="text" name="consume" class="form-control" id="consume">
                  </div>
               </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total Consume</label>
                  <div class="col-sm-10">
                     <input type="number" name="total" class="form-control" id="total">
                  </div>
               </div>
                <div class="form-group">
                  <label for="notes" class="col-sm-2 control-label">Notes</label>
                  <div class="col-sm-10">
                     <input type="text" name="notes" class="form-control" id="notes">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                     <div class="checkbox">
                        <label class="text-danger"><input type="checkbox"  name="delete"> Delete event</label>
                     </div>
                  </div>
               </div>
               <input type="hidden" name="id" class="form-control" id="id">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
         </form>
      </div>
   </div>

</div>