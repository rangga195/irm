<?php
	include 'includes/session.php';

	if(isset($_POST['edit'])){
		$id =$_POST['id'];
		$company_code = $_POST['company_code'];
		$nik = $_POST['nik'];
		$name = $_POST['name'];
		$nick_name = $_POST['nick_name'];
		$division_code = $_POST['division_code'];
		$position_code = $_POST['position_code'];
		$job_code = $_POST['job_code'];
		$parent_position = $_POST['parent_position'];
		$location_code = $_POST['location_code'];
		$ktp_no = $_POST['ktp_no'];
		$sim_no = $_POST['sim_no'];
		$gender = $_POST['gender'];
		$status = $_POST['status'];
		$birth_place = $_POST['birth_place'];
		$birth_date = $_POST['birth_date'];
		$age = $_POST['age'];
		$height = $_POST['height'];
		$weight = $_POST['weight'];
		$size = $_POST['size'];
		$shoe_size = $_POST['shoe_size'];
		$religion = $_POST['religion'];
		$phone_no = $_POST['phone_no'];
		$phone_no2 = $_POST['phone_no2'];
		$phone_home = $_POST['phone_home'];
		$address = $_POST['address'];
		$kelurahan = $_POST['kelurahan'];
		$kecamatan = $_POST['kecamatan'];
		$kabupaten_kota = $_POST['kabupaten_kota'];
		$provinsi = $_POST['provinsi'];
		$post_code = $_POST['post_code'];
		$email_office = $_POST['email_office'];
		$email_personal = $_POST['email_personal'];
		$address2 = $_POST['address2'];
		$kelurahan2 = $_POST['kelurahan2'];
		$kecamatan2 = $_POST['kecamatan2'];
		$kabupaten_kota2 = $_POST['kabupaten_kota2'];
		$provinsi2 = $_POST['provinsi2'];
		$post_code2 = $_POST['post_code2'];
		$blood_type = $_POST['blood_type'];
		$father_name = $_POST['father_name'];
		$phone_no_father = $_POST['phone_no_father'];
		$mother_name = $_POST['mother_name'];
		$phone_no_mother = $_POST['phone_no_mother'];
		$marital_status = $_POST['marital_status'];
		$partner_name = $_POST['partner_name'];
		$phone_no_partner = $_POST['phone_no_partner'];
		$child = $_POST['child'];
		$near_family = $_POST['near_family'];
		$phone_no_family = $_POST['phone_no_family'];
		$address_family = $_POST['address_family'];
		$entry_date = $_POST['entry_date'];
		$resign_date = $_POST['resign_date'];
		$bpjs_tk_no = $_POST['bpjs_tk_no'];
		$bpjs_tk_date = $_POST['bpjs_tk_date'];
		$bpjs_ks_no = $_POST['bpjs_ks_no'];
		$bpjs_ks_date = $_POST['bpjs_ks_date'];
		$npwp_no = $_POST['npwp_no'];
		$ptkp_status = $_POST['ptkp_status'];
		$hobby = $_POST['hobby'];
		$language_skill = $_POST['language_skill'];
		$vehicle_status = $_POST['vehicle_status'];
		$vehicle_type = $_POST['vehicle_type'];
		$emp_photo = $_FILES['emp_photo']['name'];
		$emp_kk = $_FILES['emp_kk']['name'];
		$emp_ktp = $_FILES['emp_ktp']['name'];
		$emp_npwp = $_FILES['emp_npwp']['name'];
		$emp_sima = $_FILES['emp_sima']['name'];
		$emp_sima = $_FILES['emp_sima']['name'];
		$emp_siml = $_FILES['emp_siml']['name'];
		$user ='andi';
		if(!empty($emp_photo)){
			move_uploaded_file($_FILES['emp_photo']['tmp_name'], '../emp_photo/'.$emp_photo);	
		}
		if(!empty($emp_kk)){
			move_uploaded_file($_FILES['emp_kk']['tmp_name'], '../emp_kk/'.$emp_kk);	
		}
		if(!empty($emp_ktp)){
			move_uploaded_file($_FILES['emp_ktp']['tmp_name'], '../emp_ktp/'.$emp_ktp);	
		}
		if(!empty($emp_npwp)){
			move_uploaded_file($_FILES['emp_npwp']['tmp_name'], '../emp_npwp/'.$emp_npwp);	
		}
		if(!empty($emp_sima)){
			move_uploaded_file($_FILES['emp_sima']['tmp_name'], '../emp_sima/'.$emp_sima);	
		}
		if(!empty($emp_simc)){
			move_uploaded_file($_FILES['emp_simc']['tmp_name'], '../emp_simc/'.$emp_simc);	
		}
		if(!empty($emp_siml)){
			move_uploaded_file($_FILES['emp_siml']['tmp_name'], '../emp_siml/'.$emp_siml);	
		}
		//creating employeeid
		$letters = '';
		$numbers = '';
		foreach (range('A', 'Z') as $char) {
		    $letters .= $char;
		}
		for($i = 0; $i < 10; $i++){
			$numbers .= $i;
		}
		$employee_id = substr(str_shuffle($letters), 0, 3).substr(str_shuffle($numbers), 0, 9);
		//
		$sql = " UPDATE employee
				 set company_code='$company_code'
				 	,nik='$nik'
				 	,name='$name'
				 	,nick_name='$nick_name'
				 	,division_code='$division_code'
				 	,position_code='$position_code'
				 	,job_code='$job_code'
				 	,parent_position='$parent_position'
				 	,location_code='$location_code'
				 	,ktp_no='$ktp_no'
				 	,sim_no='$sim_no'
				 	,gender='$gender'
				 	,status='$status'
				 	,status_work='$status_work'
				 	,birth_place='$birth_place'
				 	,birth_date='$birth_date'
				 	,age='$age'
				 	,height='$height'
				 	,weight='$weight'
				 	,size='$size'
				 	,shoe_size='$shoe_size'
				 	,religion='$religion'
				 	,phone_no='$phone_no'
				 	,phone_no2='$phone_no2'
				 	,phone_home='$phone_home'
				 	,address='$address'
				 	,kelurahan='$kelurahan'
				 	,kecamatan='$kecamatan'
				 	,kabupaten_kota='$kabupaten_kota'
				 	,provinsi='$provinsi'
				 	,post_code='$post_code'
				 	,email_office='$email_office'
				 	,email_personal='$email_personal'
				 	,address2='$address2'
				 	,kelurahan2='$kelurahan2'
				 	,kecamatan2='$kecamatan2'
				 	,kabupaten_kota2='$kabupaten_kota2'
				 	,provinsi2='$provinsi2'
				 	,post_code2='$post_code2'
				 	,blood_type='$blood_type'
				 	,father_name='$father_name'
				 	,phone_no_father='$phone_no_father'
				 	,mother_name='$mother_name'
				 	,phone_no_mother='$phone_no_mother'
				 	,marital_status='$marital_status'
				 	,partner_name='$partner_name'
				 	,phone_no_partner='$phone_no_partner'
				 	,child='$child'
				 	,near_family='$near_family'
				 	,phone_no_family='$phone_no_family'
				 	,address_family='$address_family'
				 	,entry_date='$entry_date'
				 	,resign_date='$resign_date'
				 	,bpjs_tk_no='$bpjs_tk_no'
				 	,bpjs_tk_date='$bpjs_tk_date'
				 	,bpjs_ks_no='$bpjs_ks_no'
				 	,bpjs_ks_date='$bpjs_ks_date'
				 	,npwp_no='$npwp_no'
				 	,ptkp_status='$ptkp_status'
				 	,hobby='$hobby'
				 	,language_skill='$language_skill'
				 	,vehicle_status='$vehicle_status'
				 	,vehicle_type='$vehicle_type'
				 	,emp_photo='$emp_photo'
				 	,emp_kk='$emp_kk'
				 	,emp_ktp='$emp_ktp'
				 	,emp_npwp='$emp_npwp'
				 	,emp_sima='$emp_sima'
				 	,emp_simc='$emp_simc'
				 	,emp_siml='$emp_siml'
				 	,last_update_by='$user'
				 	,last_update_date=NOW()
				 	where id ='$id'




		";
		if($conn->query($sql)){
			$_SESSION['success'] = 'Employee Update successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}

	}
	else{
		$_SESSION['error'] = 'Fill up add form first';
	}

	header('location: employee.php');
?>