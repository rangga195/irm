<!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Add Company</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="company_add.php">
                <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="code" name="code" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="name" name="name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="address" name="address" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address2" class="col-sm-2 control-label">Address2</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="address2" name="address2" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone No</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="phone" name="phone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vat" class="col-sm-2 control-label">VAT No.</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="vat" name="vat" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="postcode" class="col-sm-2 control-label">Post Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="postcode" name="postcode" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label">City</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="city" name="city" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label">Country</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="country" name="country" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="series" class="col-sm-2 control-label">No Series</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="series" name="series" required>
                    </div>
                </div>
               <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image</label>

                    <div class="col-sm-9">
                      <input type="file" name="image" id="image">
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit -->
<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="id"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="company_edit.php">
                <input type="hidden" id="comid" name="id">
                <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_code" name="code" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_name" name="name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_address" name="address" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address2" class="col-sm-2 control-label">Address2</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_address2" name="address2" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone No</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_phone" name="phone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vat" class="col-sm-2 control-label">VAT No.</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_vat" name="vat" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="postcode" class="col-sm-2 control-label">Post Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_postcode" name="postcode" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label">City</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_city" name="city" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label">Country</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_country" name="country" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="series" class="col-sm-2 control-label">No Series</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="edit_series" name="series" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="edit"><i class="fa fa-check-square-o"></i> Update</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span id="id"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="company_delete.php">
                <input type="hidden" id="del_compid" name="id">
                <div class="text-center">
                    <p>DELETE COMPANY</p>
                    <h2 id="del_company_name" class="bold"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-danger btn-flat" name="delete"><i class="fa fa-trash"></i> Delete</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Update Photo -->
<!-- <div class="modal fade" id="edit_image">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_company_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="company_edit_image.php" enctype="multipart/form-data">
                <input type="hidden" class="id" name="id">
                <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image</label>

                    <div class="col-sm-9">
                      <input type="file" id="image" name="image" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="upload"><i class="fa fa-check-square-o"></i> Update</button>
              </form>
            </div>
        </div>
    </div>
</div>     -->
     