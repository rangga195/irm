<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- DataTables -->
<script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Morris.js charts -->
<script src="../bower_components/raphael/raphael.min.js"></script>
<script src="../bower_components/morris.js/morris.min.js"></script>
<!-- ChartJS -->
<script src="../bower_components/chart.js/Chart.js"></script>
<!-- Sparkline -->
<script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../plugins/summernote/summernote.js"></script>
<script src="../plugins/summernote/summernote.min.js"></script>
<!-- Slimscroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- fullCalendar -->
<script src="../bower_components/moment/moment.js"></script>
<script src="../bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="../bower_components/alert/js/sweetalert.min.js"></script>
<script src="../bower_components/alert/js/qunit-1.18.0.js"></script>
<!-- Chained -->
<!-- <script src="../bower_components/jquery-chained/jquery.chained.min.js"></script>
<script src="../bower_components/jquery-chained/jquery-1.10.2.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script> -->




<script>
  $(function () {

    $('#example1').DataTable({
      responsive: true
    })
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    $('.select2').select2()
  })
  $('#example3').DataTable( {
     'paging'      : true,
      "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
</script>
<script>
$(function(){
  /** add active class and stay opened when selected */
  var url = window.location;

  // for sidebar menu entirely but not cover treeview
  $('ul.sidebar-menu a').filter(function() {
     return this.href == url;
  }).parent().addClass('active');

  // for treeview
  $('ul.treeview-menu a').filter(function() {
     return this.href == url;
  }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
  
});
</script>
<script>
$(function(){
	//Date picker
  $('#datepicker_add').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#datepicker_edit').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })

  $('#birth_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#birth_date1').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })

  $('#entry_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#resign_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#bpjs_tk_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#bpjs_ks_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#start_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#end_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#overtime_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#att_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('[data-mask]').inputmask()

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })
  
  $("#tahun").datepicker( {
    autoclose: true,
    minViewMode: 2,
    format: 'yyyy'

  });
  $("#yearin").datepicker( {
    autoclose: true,
    minViewMode: 2,
    format: 'yyyy'

  });
  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }

  )
  
}

);
</script>
<script>
  function validateHhMm(inputField) {
        var isValid = /^([0-1]?[0-9]|2[0-3]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#fff';
        } else {
            inputField.style.backgroundColor = '#fba';
      swal("Error!","invalid time. Time format should be 24 Hours (HH:MM). Example : 23:59","error");
        }

        return isValid;
    }
</script>
<script type="text/javascript">
   function loadKab() {
     var provinsi = $('#provinsi').val();

     $.ajax({
      type: 'GET',
      url: 'function/loadDataProv.php',
      data: "provinsi=" +provinsi,
      success: function(data){
       if(data.length > 0){
       
      $('#kabupaten_kota').html(data);
       } 
      }
     });
    }
  function loadKec() {
   var provinsi = $('#provinsi').val();
   var kabupaten_kota = $('#kabupaten_kota').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadDataKab.php?kabupaten_kota='+kabupaten_kota,
    data: "provinsi=" +provinsi,
    success: function(data){
     if(data.length > 0){
     
    $('#kecamatan').html(data);
     } 
    }
   });
  }
  function loadKel() {
   var provinsi = $('#provinsi').val();
   var kabupaten_kota = $('#kabupaten_kota').val();
   var kecamatan = $('#kecamatan').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadDataKec.php?kabupaten_kota='+kabupaten_kota+'&kecamatan='+kecamatan,
    data: "provinsi=" +provinsi,
    success: function(data){
     if(data.length > 0){
     
    $('#kelurahan').html(data);
     } 
    }
   });
  }
 function loadKodepos() {
   var provinsi = $('#provinsi').val();
   var kabupaten_kota = $('#kabupaten_kota').val();
   var kecamatan = $('#kecamatan').val();
   var kelurahan = $('#kelurahan').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadDataKel.php?kabupaten_kota='+kabupaten_kota+'&kecamatan='+kecamatan+'&kelurahan='+kelurahan,
    data: "provinsi=" +provinsi,
    success: function(data){
     if(data.length > 0){
     
    $('#post_code').html(data);
     } 
    }
   });
  }
</script>
<script type="text/javascript">
  ////Domisili
  function loadKabDom() {
     var provinsi = $('#provinsi2').val();

     $.ajax({
      type: 'GET',
      url: 'function/loadDataProvDom.php',
      data: "provinsi=" +provinsi,
      success: function(data){
       if(data.length > 0){
       
      $('#kabupaten_kota2').html(data);
       } 
      }
     });
    }
  function loadKecDom() {
   var provinsi = $('#provinsi2').val();
   var kabupaten_kota = $('#kabupaten_kota2').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadDataKabDom.php?kabupaten_kota='+kabupaten_kota,
    data: "provinsi=" +provinsi,
    success: function(data){
     if(data.length > 0){
     
    $('#kecamatan2').html(data);
     } 
    }
   });
  }
  function loadKelDom() {
   var provinsi = $('#provinsi2').val();
   var kabupaten_kota = $('#kabupaten_kota2').val();
   var kecamatan = $('#kecamatan2').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadDataKecDom.php?kabupaten_kota='+kabupaten_kota+'&kecamatan='+kecamatan,
    data: "provinsi=" +provinsi,
    success: function(data){
     if(data.length > 0){
     
    $('#kelurahan2').html(data);
     } 
    }
   });
  }
 function loadKodeposDom() {
   var provinsi = $('#provinsi2').val();
   var kabupaten_kota = $('#kabupaten_kota2').val();
   var kecamatan = $('#kecamatan2').val();
   var kelurahan = $('#kelurahan2').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadDataKelDom.php?kabupaten_kota='+kabupaten_kota+'&kecamatan='+kecamatan+'&kelurahan='+kelurahan,
    data: "provinsi=" +provinsi,
    success: function(data){
     if(data.length > 0){
     
    $('#post_code2').html(data);
     } 
    }
   });
  }
 function loadDepartment() {
     var division = $('#division_code').val();

     $.ajax({
      type: 'GET',
      url: 'function/loadDataDept.php',
      data: "division=" +division,
      success: function(data){
       if(data.length > 0){
       
      $('#department_code').html(data);
       } 
      }
     });
    }
    function loadGroup() {
     var location = $('#location_code').val();

     $.ajax({
      type: 'GET',
      url: 'function/loadDataGroup.php',
      data: "location=" +location,
      success: function(data){
       if(data.length > 0){
       
      $('#group_code').html(data);
       } 
      }
     });
    }
    function loadTime() {
     var location = $('#location_code').val();

     $.ajax({
      type: 'GET',
      url: 'function/loadDataTime.php',
      data: "location=" +location,
      success: function(data){
       if(data.length > 0){
       
      $('#sch_code').html(data);
       } 
      }
     });
    }
   function loadWorkSCH() {
   
   var location_code = $('#location_code').val();
   var group_code = $('#group_code').val();
   var tahun = $('#tahun').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadWorkSCH.php?tahun='+tahun+'&location_code='+location_code,
    data: "group_code=" + group_code,
    success: function(data){
     if(data.length > 0){
      $('#isitable').html(data);
    
     }  else {
      $('#isitable').html("kosong");
     
     } 
    }
   });
  }


  function loadRemaining() {
   
   var nik = $('#nik').val();
   var leave_type = $('#leave_type').val();
   var start_date = $('#start_date').val();
   var end_date = $('#end_date').val();
   var leave_code = $('#leave_code').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadRemaining.php?nik='+nik+'&start_date='+start_date+'&end_date='+end_date+'&leave_code='+leave_code,
    data: "leave_type=" + leave_type,
    success: function(data){
    if(data.length > 0){
       
      $('#remaining').html(data);
       } 
      }
   });
  }
    

    function loadRemainingEdit() {
   
   var nik = $('#nik').val();
   var leave_type = $('#leave_type').val();
   var start_date = $('#start_date').val();
   var end_date = $('#end_date').val();
   var leave_code = $('#leave_code').val();

   $.ajax({
    type: 'GET',
    url: 'function/loadRemainingEdit.php?nik='+nik+'&start_date='+start_date+'&end_date='+end_date+'&leave_code='+leave_code,
    data: "leave_type=" + leave_type,
    success: function(data){
    if(data.length > 0){
       
      $('#remaining').html(data);
       } 
      }
   });
  }


function loadDataLeave() {
     var leave_type = $('#leave_type').val();

     $.ajax({
      type: 'GET',
      url: 'function/loadDataLeave.php',
      data: "leave_type=" +leave_type,
      success: function(data){
       if(data.length > 0){
       
      $('#leave_code').html(data);
       } 
      }
     });
    }


    function encrypt( $s ) {
        $cryptKey  = 'd8578edf8458ce06fbc5bb76a58c5ca4';
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $s, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }
     
    function decrypt($s) {
        $cryptKey  = 'd8578edf8458ce06fbc5bb76a58c5ca4';
        $qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $s ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }

  $(document).ready(function() { $('.summernote-modal').summernote({height: 200}); });
    
 function validateFormadd()
 {

  var strA = document.forms["add_employee_leave"]["start_date"].value;
          DDA = strA.slice(8) ;
          MMA = strA.slice(5, 7);
          YYA = strA.slice(0,4);
          DATEA = YYA + '-' +MMA + '-' + DDA ; 


  var strB = document.forms["add_employee_leave"]["end_date"].value;
          DDB = strB.slice(8) ;
          MMB = strB.slice(5, 7);
          YYB = strB.slice(0,4);
          DATEB = YYB + '-' +MMB + '-' + DDB ; 
      var X= DATEA
      var  Y = DATEB


  var strNow = new Date();
  var DD =  ('0' + strNow.getDate()).slice(-2);
  var MM =('0' + (strNow.getMonth() + 1)).slice(-2);
  var YY =strNow.getFullYear();
  var DATENOW = YY + '-' +MM + '-' + DD ; 
  var leave_type = document.forms["add_employee_leave"]["leave_type"].value;
   var leave_code = document.forms["add_employee_leave"]["leave_code"].value;
  var emp_leave =document.forms["add_employee_leave"]["emp_leave"].value;
   var remaining =document.forms["add_employee_leave"]["remaining"].value;

      if (X > Y ){
        //swal("The date range you selected is incorrect ( end date cannot be less than the start date ) !.", "error");
        swal("Error!", "The date range you selected is incorrect ( end date cannot be less than the start date ) !", "error");
        return false; 

      }

      else if (leave_type=='SAKIT')
      {
         if (emp_leave=='')
          {
             //alert("Error : Please upload file image leave request for type  " + leave_type);
             swal("Error!","Please upload file image leave request for type " + leave_type ,"error")
            return false; 
          } 
      }else if (leave_type=='CUTI TAHUNAN')

      {
        if (remaining ==' ')
        {
          swal("Error!","Please select remaining leave first for type " + leave_type,"error");
            return false; 
        }else if (remaining < 0)
        {
            swal("Error","Your remaining leave must not be less than zero (0)", "error");
            return false; 
        }

      }
      else if (leave_type=='CUTI KHUSUS')
      {
         if (emp_leave=='')
          {
             //alert("Error : Please upload file image leave request for type  " + leave_type);
             swal("Error!","Please upload file image leave request for type " + leave_type ,"error")
            return false; 
          } 
      }

      ;


     
 }
 function validateFormEdit()
 {

  var strA = document.forms["edit_employee_leave"]["start_date"].value;
          DDA = strA.slice(8) ;
          MMA = strA.slice(5, 7);
          YYA = strA.slice(0,4);
          DATEA = YYA + '-' + MMA + '-' + DDA ; 


  var strB = document.forms["edit_employee_leave"]["end_date"].value;
          DDB = strB.slice(8) ;
          MMB = strB.slice(5, 7);
          YYB = strB.slice(0,4);
          DATEB = YYB + '-' + MMB + '-' + DDB ; 
      var X= DATEA
      var  Y = DATEB
      if (X > Y){
        swal("Error!","The date range you selected is incorrect ( End date cannot be less than the start date ) !","error" );
        return false; 

      };

      var leave_type = document.forms["edit_employee_leave"]["leave_type"].value;
      var remaining =document.forms["edit_employee_leave"]["remaining"].value;

     if (leave_type=='CUTI TAHUNAN')

      {
        if (remaining == ' ')
        {
          swal("Error!","Please select remaining leave first for type " + leave_type,"error");
            return false; 
        }
      };
     
 }
function validateFormaddTrip()
 {

  var strA = document.forms["add_trip"]["start_date"].value;
          DDA = strA.slice(8) ;
          MMA = strA.slice(5, 7);
          YYA = strA.slice(0,4);
          DATEA = YYA + '-' +MMA + '-' + DDA ; 


  var strB = document.forms["add_trip"]["end_date"].value;
          DDB = strB.slice(8) ;
          MMB = strB.slice(5, 7);
          YYB = strB.slice(0,4);
          DATEB = YYB + '-' +MMB + '-' + DDB ; 
      var X=DATEA
      var  Y = DATEB
      if (X > Y ){
        swal("Error!","The date range you selected is incorrect ( End date cannot be less than the start date ) !","error");
        return false; 

      };


     
 }
 function validateFormEditTrip()
 {

  var strA = document.forms["edit_trip"]["start_date"].value;
         DDA = strA.slice(8) ;
          MMA = strA.slice(5, 7);
          YYA = strA.slice(0,4);
          //DATEA = MMA + '/' + DDA + '/' + YYA;
          DATEA = YYA + '-' + MMA + '-' + DDA ; 


  var strB = document.forms["edit_trip"]["end_date"].value;
          DDB = strB.slice(8) ;
          MMB = strB.slice(5, 7);
          YYB = strB.slice(0,4);
          DATEB = YYB + '-' + MMB + '-' + DDB ; 
      var X= DATEA
      var  Y = DATEB
      if ( X > Y )
      {
        swal("Error!","The date range you selected is incorrect ( End date cannot be less than the start date ) !","error");
        return false; 

      };


     
 }

 function validateFormaddTravel()
 {

  var strA = document.forms["add_travel"]["start_date"].value;
          DDA = strA.slice(8) ;
          MMA = strA.slice(5, 7);
          YYA = strA.slice(0,4);
          //DATEA = MMA + '/' + DDA + '/' + YYA;
          DATEA = YYA + '-' +MMA + '-' + DDA ; 


  var strB = document.forms["add_travel"]["end_date"].value;
          DDB = strB.slice(8) ;
          MMB = strB.slice(5, 7);
          YYB = strB.slice(0,4);
          DATEB = YYB + '-' +MMB + '-' + DDB ; 
      var X= DATEA
      var  Y = DATEB
      if (X > Y ){
        swal("Error!","The date range you selected is incorrect ( End date cannot be less than the start date ) !","error");
        return false; 

      };


     
 }
 function validateFormaddBackDate()
 {

 // var Leave_type = document.forms["add_travel"]["Leave_type"].value;

  var strA = document.forms["add_travel"]["start_date"].value;
          DDA = strA.slice(8) ;
          MMA = strA.slice(5, 7);
          YYA = strA.slice(0,4);
          DATEA = YYA + '-' +MMA + '-' + DDA ; 

  var strNow = new Date();
  var DD =  ('0' + strNow.getDate()).slice(-2);
  var MM =('0' + (strNow.getMonth() + 1)).slice(-2);
  var YY =strNow.getFullYear();
  var DATENOW = YY + '-' +MM + '-' + DD ; 
    alert(DATENOW);
   //alert("Error : The date you selected is not appropriate ( End date cannot be less than the start date ) !");
        return false; 

   // if (DATEA < DATE )
   // {
   //   alert("Error : The date you selected is not appropriate ( End date cannot be less than the start date ) !");
   //      return false; 
   //    };

     
 }
</script>
