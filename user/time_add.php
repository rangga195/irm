<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Time List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Time</li>
         <li class="active">Time List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="time_save.php">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Time Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <form class="form-horizontal" method="POST" action="jobs_add.php">
                 <div class="form-group">
                    <label for="company" class="col-sm-3 control-label">Company</label>

                    <div class="col-sm-9">
                      <select class="form-control  select2" name="company" id="company" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="location" class="col-sm-3 control-label">Location</label>

                    <div class="col-sm-9">
                      <select class="form-control  select2" name="location" id="location" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM location";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>

                 <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="code" name="code" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="name" name="name" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="start_time" class="col-sm-3 control-label">Start Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="start_time" name="start_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="end_time" class="col-sm-3 control-label">End Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="end_time" name="end_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="late" class="col-sm-3 control-label">Late</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control " id="late" name="late" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="early" class="col-sm-3 control-label">Early</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control " id="early" name="early" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkin1" class="col-sm-3 control-label">Check In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="checkin1" name="checkin1"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);"required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkout1" class="col-sm-3 control-label">Check Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="checkout1" name="checkout1"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="checkin2" class="col-sm-3 control-label">Check in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="checkin2" name="checkin2"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="checkout2" class="col-sm-3 control-label">Check Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="checkout2" name="checkout2"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="ovetimein1" class="col-sm-3 control-label">Overtime In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="ovetimein1" name="ovetimein1"   data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout1" class="col-sm-3 control-label">Overtime Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="overtimeout1" name="overtimeout1"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimein2" class="col-sm-3 control-label">Overtime In2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  "  id="overtimein2" name="overtimein2"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout2" class="col-sm-3 control-label">Overtime Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker ">
                        <input type="text" class="form-control  " id="overtimeout2" name="overtimeout2"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimein3" class="col-sm-3 control-label">Overtime In3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="overtimein3" name="overtimein3"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout3" class="col-sm-3 control-label">Overtime Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="overtimeout3" name="overtimeout3"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimein4" class="col-sm-3 control-label">Overtime In4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="overtimein4" name="overtimein4"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="overtimeout4" class="col-sm-3 control-label">Overtime Out4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="overtimeout4" name="overtimeout4"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="breakein1" class="col-sm-3 control-label">Breake in1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="breakein1" name="breakein1"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakeout1" class="col-sm-3 control-label">Breake Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="breakeout1" name="breakeout1"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakein2" class="col-sm-3 control-label">Breake in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="breakein2" name="breakein2"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakeout2" class="col-sm-3 control-label">Breake Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="breakeout2" name="breakeout2"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakein3" class="col-sm-3 control-label">Breake in3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="breakein3" name="breakein3" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);"  required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breakeout3" class="col-sm-3 control-label">Breake Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control " id="breakeout3" name="breakeout3"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Kembali</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/company_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>