<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['time_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM schedules_work WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Time List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Time</li>
         <li class="active">Time List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="time_update.php?id=<?php echo $row['id']; ?>">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Time Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <div class="form-group">
                  <label for="company_code" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="company_code" id="company_code"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM company";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['company_code']; ?>" <?php if(($k['company_code'])== ($row['company_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['company_code']==$k['company_code'])?print(" "):print(""); ?>  > <?php echo $k['company_name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>

               <div class="form-group">
                  <label for="location" class="col-sm-2 control-label">Location</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="location" id="location"  required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT * FROM location";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['loc_code']; ?>" <?php if(($k['loc_code'])== ($row['loc_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['loc_code']==$k['loc_code'])?print(" "):print(""); ?>  > <?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                    <label for="edit_code" class="col-sm-2 control-label">Code</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_code" name="code" value="<?php echo $row['sch_code'];?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit_name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="edit_name" name="name" value="<?php echo $row['sch_name'];?>" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="edit_start_time" class="col-sm-2 control-label">Start Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_start_time" name="start_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" value="<?php echo $row['start_time'];?>" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_end_time" class="col-sm-2 control-label">End Time</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_end_time" name="end_time"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" value="<?php echo $row['end_time'];?>"required>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit_late" class="col-sm-2 control-label">Late</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control " id="edit_late" name="late" value="<?php echo $row['late'];?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_early" class="col-sm-2 control-label">Early</label>

                    <div class="col-sm-9">
                      <input type="number" class="form-control " id="edit_early" name="early" value="<?php echo $row['early'];?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_checkin1" class="col-sm-2 control-label">Check In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_checkin1" name="checkin1" value="<?php echo $row['checkin1'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_checkout1" class="col-sm-2 control-label">Check Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_checkout1" name="checkout1" value="<?php echo $row['checkout1'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_checkin2" class="col-sm-2 control-label">Check in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_checkin2" name="checkin2" value="<?php echo $row['checkin2'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_checkout2" class="col-sm-2 control-label">Check Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_checkout2" name="checkout2" value="<?php echo $row['checkout2'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein1" class="col-sm-2 control-label">Overtime In1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_overtimein1" name="overtimein1"  value="<?php echo $row['overtimein1'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout1" class="col-sm-2 control-label">Overtime Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_overtimeout1" name="overtimeout1" value="<?php echo $row['overtimeout1'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);"required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein2" class="col-sm-2 control-label">Overtime In2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_overtimein2" name="overtimein2" value="<?php echo $row['overtimein2'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout2" class="col-sm-2 control-label">Overtime Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker ">
                        <input type="text" class="form-control  " id="edit_overtimeout2" value="<?php echo $row['overtimeout2'];?>" name="overtimeout2" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein3" class="col-sm-2 control-label">Overtime In3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_overtimein3" name="overtimein3" value="<?php echo $row['overtimein3'];?>" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout3" class="col-sm-2 control-label">Overtime Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_overtimeout3" name="overtimeout3"  value="<?php echo $row['overtimeout3'];?>" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimein4" class="col-sm-2 control-label">Overtime In4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_overtimein4" name="overtimein4"  value="<?php echo $row['overtimein4'];?>" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_overtimeout4" class="col-sm-2 control-label">Overtime Out4</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control " id="edit_overtimeout4" name="overtimeout4" value="<?php echo $row['overtimeout4'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="edit_breakein1" class="col-sm-2 control-label">Breake in1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_breakein1" name="breakein1" value="<?php echo $row['breakein1'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakeout1" class="col-sm-2 control-label">Breake Out1</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_breakeout1" name="breakeout1" value="<?php echo $row['breakeout1'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakein2" class="col-sm-2 control-label">Breake in2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_breakein2" name="breakein2" value="<?php echo $row['breakein2'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakeout2" class="col-sm-2 control-label">Breake Out2</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_breakeout2" name="breakeout2" value="<?php echo $row['breakeout2'];?>" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakein3" class="col-sm-2 control-label">Breake in3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_breakein3" name="breakein3" value="<?php echo $row['breakein3'];?>"  data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit_breakeout3" class="col-sm-2 control-label">Breake Out3</label>

                    <div class="col-sm-9">
                      <div class="bootstrap-timepicker">
                        <input type="text" class="form-control  " id="edit_breakeout3" name="breakeout3" value="<?php echo $row['breakeout3'];?>" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                      </div>
                    </div>
                </div>

            </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Kembali</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/organization_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>