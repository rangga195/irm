<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
  include '../timezone.php';
   $range_to = date('Y-m-d');
   $range_from = date('Y-m-01');//date('Y-m-d', strtotime('-30 day', strtotime($range_to)));
?>
  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Remaining Leave
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Leave</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
            <!--   <a href="employee_entitlement_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a> -->
              <div class="pull-right">
                <form method="get" class="form-inline" >
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input autocomplete="off" type="text" class="form-control " id="start_date" name="start_date" value="<?php echo  $range_from; ?>" >
                  </div>
                  <input autocomplete="off" type="text" class="form-control " id="end_date" name="end_date" value="<?php echo  $range_to; ?>" >
                   <select class="form-control select2" name="location_code" id="location_code" >
                        <option value="" selected>- Select Location -</option>
                        <?php
                           $loc_code =$user['location_code'];
                           $sql = "SELECT * FROM location
                            where loc_code = '$loc_code' ";
                           $query = $conn->query($sql);
                           while($comrow = $query->fetch_assoc()){
                             echo "
                               <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                             ";
                           }
                           ?>
                     </select>
                  <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                  
                </form>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                <!--   <th>No.</th> -->
                  <th>Company</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>Leave Type</th>
                  <th>Period Start</th>
                  <th>Period End</th>
                  <th>Remaining Leave</th>
                 <!--  <th>Tools</th> -->
                </thead>
                <tbody>
                  <?php
                  if(isset($_GET['start_date']))
                      {
                        $start_date = $_GET['start_date'];
                        $end_date = $_GET['end_date'];
                        $loc_code =$_GET['location_code'];
                        $user=$user['username'];
                        $sql = "SELECT 
                                  a.id,
                                  a.company_code,
                                  a.nik,
                                  b.name,
                                  a.leave_type,
                                  a.period_start,
                                  a.period_end,
                                  a.remaining_leave
                                   FROM leave_remaining a
                                  LEFT JOIN employee_view b ON a.nik = b.nik
                                  WHERE b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END 
                                  and a.nik ='$user'
                                  ";
                        }
                        else
                        {
                          $start_date = $range_from;
                          $end_date = $range_to;
                          $loc_code =$user['location_code'];
                          $user=$user['username'];
                          $sql = "SELECT 
                                  a.id,
                                  a.company_code,
                                  a.nik,
                                  b.name,
                                  a.leave_type,
                                  a.period_start,
                                  a.period_end,
                                  a.remaining_leave
                                   FROM leave_remaining a
                                  LEFT JOIN employee_view b ON a.nik = b.nik
                                  WHERE b.location_code LIKE CASE '$loc_code' WHEN '' THEN '%%' ELSE '$loc_code' END
                                  and a.nik ='$user'
                                   ";
                        }
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      // $status = ($row['status'])?'<span class="label label-danger">Pending Approval</span>':'<span class="label label-success">Approve</span>';
                      ?>

                        <tr>
                           <!-- <td><?php echo $row['id']; ?></td> -->
                           <td><?php echo $row['company_code']; ?></td>
                           <td><?php echo $row['nik']; ?></td>
                           <td><?php echo $row['name']; ?></td>
                           <td><?php echo $row['leave_type']; ?></td>
                           <td><?php echo $row['period_start']; ?></td>
                           <td><?php echo $row['period_end']; ?></td>
                           <td><?php echo $row['remaining_leave']; ?></td>
                          <!-- <td>
                            <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['nik'];?>" href="employee_entitlement_edit.php?leave_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-sm btn-danger btn-flat"   data-toggle="tooltip" title="Delete Leave  <?php echo $row['nik'];?>" href="employee_entitlement_delete.php?id=<?php echo $row['id']; ?>"  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['id'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td> -->
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/jobs_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'jobs_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#jobsid').val(response.id);
      $('#edit_code').val(response.job_code);
      $('#edit_name').val(response.name);
      $('#del_jobsid').val(response.id);
      $('#del_jobs').html(response.name);
      $('#company_val').val(response.company_code).html(response.company_name);
    }
  });
}
</script>
</body>
</html>
