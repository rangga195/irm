<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Employee List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Employees</li>
        <li class="active">Employee List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header">
               <!-- <a href="employee_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a> -->
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Tools</th>
                  <th>Company</th>
                  <th>NIK</th>
                  <th>Photo</th>
                  <th>Name</th>
                  <th>Division</th>
                  <th>Position</th>
                  <th>Jobs Level</th>
                  <th>Location</th>
                  <th>Status</th>
                  <th>Member Since</th>
                <!--   <th>Tools</th> -->
                </thead>
                <tbody>
                  <?php
                   $nik = $user['username'];
                    $sql = "SELECT id,company_code,nik,name,division_name,description,job_name,loc_name,status,entry_date,emp_photo FROM employee_view where status ='Active'
                    and nik ='$nik'
                     order by nik ";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                      ?>
                        <tr>
                          <td>
                           <!--<a href="employee_edit.php?employee_add=edit&id=<?php echo $row['id']; ?>">
                          <button name="edit" type="submit" class="btn btn-sm btn-flat"><i class="fa fa-edit"></i> Edit</a>-->  

                           <a class="btn btn-sm btn-primary btn-flat"   data-toggle="tooltip" title="Edit Data <?php echo $row['nik'];?>" href="employee_edit.php?employee_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                           <!-- <a class="btn btn-xs btn-info"   data-toggle="tooltip" title="Generate User <?php echo $row['nik'];?>" href="employee_generate.php?nik=<?php echo $row['nik']; ?>"><i class="glyphicon glyphicon-user"></i></a> 

                         
                            <a class="btn btn-xs btn-warning"   data-toggle="tooltip" title="Delete Employee  <?php echo $row['nik'];?>" href="employee_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['nik'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a> -->
                            
                          </td>
                           <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['nik']; ?></td>
                          <td><img src="<?php echo (!empty($row['emp_photo']))? '../emp_photo/'.$row['emp_photo']:'../emp_photo/profile.jpg'; ?>" width="30px" height="30px"> 
                            <a href="#edit_photo" data-toggle="modal" class="pull-right photo" data-id="<?php echo $row['nik']; ?>"><span class="fa fa-edit"></span>
                            </a> 

                          </td>


                          <!-- Update Photo -->
                          <div class="modal fade" id="edit_photo"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="edit_photo">Photo <?php echo $row['name'];?></h4>
                                      </div>
                                      <div class="modal-body">
                                        <form class="form-horizontal" method="POST" action="employee_edit_photo.php?<?php echo $row['nik']; ?>" enctype="multipart/form-data">
                                          <input  hidden class="empid" name="id" value="<?php echo $row['nik']; ?>" >
                                          <div class="form-group">
                                              <!-- label for="photo" class="col-sm-3 control-label">Photo</label> -->

                                              <div class="col-sm-10">
                                                <figure>
                                                  <a data-toggle="modal" data-target="#edit_photo">
                                                  <img src="<?php echo (!empty($row['emp_photo']))? '../emp_photo/'.$row['emp_photo']:'../emp_photo/profile.jpg'; ?>" height='50%' width='50%'/>
                                                  </a>
                                                  
                                                  </figure>
                                               <!--  <input type="file" id="photo" name="photo" required> -->
                                              </div>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                                        <button type="submit" class="btn btn-success btn-flat" name="upload"><i class="fa fa-check-square-o"></i> Update</button> -->
                                        </form>
                                      </div>
                                  </div>
                              </div>
                          </div>    


                          <td><?php echo $row['name']?></td>
                          <td><?php echo $row['division_name']; ?></td>
                          <td><?php echo $row['description']; ?></td>
                          <td><?php echo $row['job_name']; ?></td>
                          <td><?php echo $row['loc_name']; ?></td>
                          <td><?php echo $row['status']; ?></td>
                         
                          <td><?php echo date('M d, Y', strtotime($row['entry_date'])) ?></td>
                          <!-- <td>

                           <a class="btn btn-xs btn-info"   data-toggle="tooltip" title="Edit Data <?php echo $row['nik'];?>" href="employee_edit.php?employee_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-xs btn-warning"   data-toggle="tooltip" title="Delete Employee  <?php echo $row['nik'];?>" href="employee_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['nik'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                            
                          </td> -->
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
  

  <?php include 'includes/footer.php'; ?>
 <!--  <?php include 'includes/employee_modal.php'; ?> -->
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.photo').click(function(e){
    e.preventDefault();
    var id = $(this).data('id');
    getRow(id);
  });

});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'employee_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#id').val(response.id);
      $('#nik').html(response.nik);
      $('.del_employee_name').html(response.firstname+' '+response.lastname);
      $('#employee_name').html(response.firstname+' '+response.lastname);
      $('#edit_nik').val(response.nik);
      $('#company_val').val(response.company_code).html(response.company_name);
      $('#edit_lastname').val(response.lastname);
      $('#edit_address').val(response.address);
      $('#datepicker_edit').val(response.birthdate);
      $('#edit_contact').val(response.contact_info);
      $('#gender_val').val(response.gender).html(response.gender);
      $('#position_val').val(response.position_id).html(response.description);
      $('#schedule_val').val(response.schedule_id).html(response.time_in+' - '+response.time_out);
    }
  });
}
</script>
</body>
</html>
