<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <?php
      if($_GET['leave_add']=='edit')
      {
         $id =$_GET['id'];
         $sql = "SELECT * FROM leave_employee WHERE id = '$id'";
         $query = $conn->query($sql);
         $row = $query->fetch_assoc();
      }
      ?>
   <section class="content-header">
      <h1>
         Edit Leave List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Leave</li>
         <li class="active">Leave List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="leave_update.php?id=<?php echo $row['id']; ?>" enctype="multipart/form-data" name="edit_employee_leave" onsubmit="return validateFormEdit();";>
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Leave Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              
                <div class="form-group">
                    <label for="no" class="col-sm-2 control-label">No.</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="no" name="no"  value="<?php echo $row['no'];?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                  <label for="company_code" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-9">
                      <input type="hidden" class="form-control " id="company_code" name="company_code" value="<?php echo  $user['company_code']; ?>"   >
                       <input type="text" class="form-control " id="company_name" name="company_name" value="<?php echo  $user['company_name']; ?>"  readonly >



                    <!--  <select class="form-control select2" name="company_code" id="company_code"  readonly="readonly">
                        <option value=" ">- Select -</option>
                           <?php 
                                  $company_code =$user['company_code'];
                                  $q = "SELECT * FROM company where company_code ='$company_code'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['company_code']; ?>" <?php if(($k['company_code'])== ($row['company_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['company_code']==$k['company_code'])?print(" "):print(""); ?>  > <?php echo $k['company_name'];?>
                           </option> <?php   } ?>
                     </select> -->
                  </div>
               </div>
               <div class="form-group">
                  <label for="nik" class="col-sm-2 control-label">NIK</label>
                  <div class="col-sm-9">
                    <input type="hidden" class="form-control " id="nik" name="nik" value="<?php echo  $user['nik']; ?>"   >

                       <input type="text" class="form-control " id="name" name="name" value="<?php echo  $user['nama']; ?>"  readonly >
                     <!-- <select class="form-control select2" name="nik" id="nik"  readonly="readonly">
                        <option value=" ">- Select -</option>
                           <?php 
                                $loc_code =$user['location_code'];
                                $user =$user['username'];
                                $q = "SELECT * FROM employee_view   where nik = '$user'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['nik']; ?>" <?php if(($k['nik'])== ($row['nik']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['nik']==$k['nik'])?print(" "):print(""); ?>  > <?php echo $k['nik'];?>-<?php echo $k['name'];?>
                           </option> <?php   } ?>
                     </select> -->
                  </div>
               </div>

               <div class="form-group">
                  <label for="leave_type" class="col-sm-2 control-label">Leave Type</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="leave_type" id="leave_type"  onchange ="loadDataLeave();loadRemainingEdit()" required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE ='TYPE LEAVE'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['leave_type']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>

               <div class="form-group">
                  <label for="leave_code" class="col-sm-2 control-label">Leave</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="leave_code" id="leave_code" onchange ='loadRemainingEdit();' required>
                        <option value=" ">- Select -</option>
                           <?php $q = "SELECT config_code,config_desc FROM configure WHERE CODE ='LEAVE'";
                                 $query = $conn->query($q);
                           while ($k =  $query->fetch_assoc()){ ?>
                           <option value="<?php echo $k['config_code']; ?>" <?php if(($k['config_code'])== ($row['leave_code']))
                                       {echo "selected=\"selected\"";};?>
                           <?php (@$h['config_code']==$k['config_code'])?print(" "):print(""); ?>  > <?php echo $k['config_desc'];?>
                           </option> <?php   } ?>
                     </select>
                  </div>
               </div>
              <div class="form-group">
                    <label for="start_date" class="col-sm-2 control-label">Start Date</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="start_date" name="start_date" value="<?php echo $row['start_date'];?>" onchange ='loadRemainingEdit();'  required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="end_date" class="col-sm-2 control-label">End Date</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="end_date" name="end_date" value="<?php echo $row['end_date'];?>" onchange ='loadRemainingEdit();'  required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="reason" class="col-sm-2 control-label">Reason</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="reason" name="reason"  value="<?php echo $row['reason'];?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="remaining" class="col-sm-2 control-label">Remaining</label>

                    <div class="col-sm-9">
                      <select class="form-control select2" name="remaining" id="remaining"  >
                        <option value=" " selected>- Select -</option>
                    </div>
                </div>

                <div class="form-group">
                  <label for="remaining" class="col-sm-2 control-label">Upload File Image</label>
                    <div class="col-sm-9">
                     <input type="file" id="emp_leave" name="emp_leave" accept="image/jpeg,image/jpg,image/png">
                     <p class="help-block">Format Upload : jpeg, jpg, png</p>
                      <figure>
                        <a data-toggle="modal" data-target="#popemp_leave">
                        <img src="<?php echo (!empty($row['file_upload']))? '../emp_leave/'.$row['file_upload']:'../emp_leave/images.jpg'; ?>" height='20%' width='20%'/>
                        </a>
                        </figure>
                  </div>
                </div>
                <div class="modal fade" id="popemp_leave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                     <h5 class="modal-title" id="popemp_leave">File <?php echo $row['file_upload'];?></h5>
                        
                    </div>
                    <div class="modal-body">
                        <center> 
                        <img src="<?php echo (!empty($row['file_upload']))? '../emp_leave/'.$row['file_upload']:'../emp_leave/images.jpg'; ?>" height='100%' width='100%'/>
                       </center>
                     </div>
                  <div class="modal-footer">
                     <button class="btn btn-secondary" type="button" data-dismiss="modal">close</button>
                  </div>
                  </div>
                 </div>
               </div>



            </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="edit"  ><i class="fa fa-save"></i> Update</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
 <!--   <?php include 'includes/organization_modal.php'; ?> -->
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>