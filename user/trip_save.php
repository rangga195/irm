<?php
include 'includes/session.php';
include 'includes/sendEmail-v156.php';
if(isset($_POST['add'])){
	$no =$_POST['no'];
	$company_code = $_POST['company_code'];
	$nik=$_POST['nik'];
	$start_date=$_POST['start_date'];
	$end_date=$_POST['end_date'];
	$check_in=$_POST['check_in'];
	$check_out=$_POST['check_out'];
	$destination=$_POST['destination'];
	$notes=$_POST['notes'];
	$user = $user['username'];
	$ceksql ="SELECT * FROM trip WHERE nik ='$nik' 
			 AND ((start_date BETWEEN '$start_date' AND '$end_date')
                OR (end_date BETWEEN '$start_date' AND '$end_date'))";
	$query = $conn->query($ceksql);
	if($query->num_rows >= 1)
	{
		$_SESSION['error'] = 'Data already exists, the date range is the same as your previous request';
	}
	else
	{


		$sql = "INSERT INTO trip (no,company_code,nik,start_date,end_date,check_in,check_out,destination,notes,status,create_by,create_date) 
		VALUES ('$no','$company_code','$nik','$start_date','$end_date','$check_in','$check_out','$destination','$notes',2,'$user',NOW())";
		if($conn->query($sql)){

			$data = mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.start_date,a.end_date,a.check_in,a.check_out,a.destination,a.notes,a.approve_by,b.approval_level_1,b.approval_level_2 ,CASE a.approve_by 
                          WHEN  '' 
                          THEN CASE b.approval_level_1 WHEN '' THEN b.approval_level_2 ELSE  b.approval_level_1 END
                          ELSE b.approval_level_1 END AS Approve_To
				FROM trip a 
				LEFT JOIN employee_view b ON a.nik = b.nik 
				WHERE a.nik ='$nik' AND a.start_date ='$start_date' AND a.end_date ='$end_date'"));
			
			$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
				WHERE a.nik ='".$data['Approve_To']."'"));
			$ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' and type ='admin'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Business Trip Notification to '.$cekmail['nik'].'-'.$cekmail['name'].'';
			$message='<html><body>';
						
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve business trip from :</span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_date']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_date'] . "</td></tr>";
			$message.="<tr><td><strong>Check In:</strong></td><td>" . $data['check_in'] . "</td></tr>";
			$message.="<tr><td><strong>Check Out:</strong></td><td>" . $data['check_out'] . "</td></tr>";
			$message.="<tr><td><strong>Destination:</strong></td><td>" . $data['destination']. "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			$message.='<br>Click the following button to:</br>';
			$message.='<p><a href="'.$ambil_smtp['base_url'].'/trip_approve.php?edit=4&id='.$data['id'].'&username1='.$data['approval_level_1'].'&username2='.$data['approval_level_2'].'"><button class="btn btn-sm btn-success">Approve</button></a></p>';
			$message.='<p><a href="'.$ambil_smtp['base_url'].'/trip_reject.php?edit=100&id='.$data['id'].'&username1='.$data['approval_level_1'].'&username2='.$data['approval_level_2'].'"><button class="btn btn-sm btn-danger">Reject</button></a></p>';
			
			$message.='<div class="footer">';
          $message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
          $message.='               <tr>';
          $message.='                 <td class="content-block">';
          $message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
          $message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='               <tr>';
          $message.='                 <td class="content-block powered-by">';
          $message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
          $message.='                 </td>';
          $message.='               </tr>';
          $message.='             </table>';
          $message.='           </div>';
          $message.='</body></html>';
			
			
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];
		           //$password = base64_decode($ambilsmtp['password']);

			if(email_localhost($to, $subject, $message, $sender, $password))
				echo "<script>alert('Approval has been sent!')</script>";
			
			else
				echo "Email sending failed";



			$_SESSION['success'] = 'Your business trip request was successfully added and the email approval was sent';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}

	}	
}	
else{
	$_SESSION['error'] = 'Fill up add form first';
}

header('location: trip.php');

?>