<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <?php
    $sql ="SELECT IFNULL(CAST(SUBSTRING(MAX(NO),7,5)AS INTEGER),0) AS max_id FROM overtime WHERE SUBSTR(no, 1, 4) = YEAR(NOW()) AND CONVERT(SUBSTR(no, 5, 2),DECIMAL) = MONTH(NOW())";
      $hasil = $conn->query($sql);
      $data = $hasil->fetch_assoc();
     
      $lastID = $data['max_id'];
      $YM = date('Ym');
      $lastNoUrut = substr($lastID, 8, 4);
      $nextNoUrut = $lastID + 1;

      $nextID = $YM.sprintf("%04s",$nextNoUrut);
?> 
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Overtime List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Overtime</li>
         <li class="active">Overtime List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="overtime_save.php">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Overtime Information 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <form class="form-horizontal" method="POST" action="overtime_add.php">
                <div class="form-group">
                    <label for="no" class="col-sm-2 control-label">No.</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="no" name="no" value="<?php echo  $nextID; ?>" readonly="readonly" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="company" class="col-sm-2 control-label">Company</label>

                    <div class="col-sm-9">
                       <input type="hidden" class="form-control " id="company_code" name="company_code" value="<?php echo  $user['company_code']; ?>"   >
                       <input type="text" class="form-control " id="company_name" name="company_name" value="<?php echo  $user['company_name']; ?>"  readonly >

                     <!--  <select class="form-control select2" name="company_code" id="company_code" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $company_code =$user['company_code'];
                          $sql = "SELECT * FROM company where company_code ='$company_code'";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select> -->
                    </div>
                </div>
                <div class="form-group">
                    <label for="nik" class="col-sm-2 control-label">NIK</label>

                    <div class="col-sm-9">
                      <input type="hidden" class="form-control " id="nik" name="nik" value="<?php echo  $user['nik']; ?>"   >

                       <input type="text" class="form-control " id="name" name="name" value="<?php echo  $user['nama']; ?>"  readonly >
                       
                      <!-- <select class="form-control select2" name="nik" id="nik" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $user =$user['username'];
                          $sql = "SELECT * FROM employee_view   where nik = '$user'";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['nik']."'>".$comrow['nik']."-".$comrow['name']."</option>
                            ";
                          }
                        ?>
                      </select> -->
                    </div>
                </div>
                             
                 <div class="form-group">
                    <label for="overtime_date" class="col-sm-2 control-label">Overtime Date</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="overtime_date" name="overtime_date" required>
                    </div>
                </div>
               
                 <div class="form-group">
                    <label for="start_time" class="col-sm-2 control-label">Start Time</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control  " id="start_time" name="start_time" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);"  required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="end_time" class="col-sm-2 control-label">End Time</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control  " id="end_time" name="end_time" data-inputmask='"mask": "99:99"' data-mask onchange="validateHhMm(this);" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="notes" class="col-sm-2 control-label">Notes</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="notes" name="notes" required>
                    </div>
                </div>
                                
                
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Back</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/company_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>