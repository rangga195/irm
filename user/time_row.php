<?php 
	include 'includes/session.php';

	if(isset($_POST['id'])){
		$id = $_POST['id'];
		$sql = "SELECT  a.id,
                                    a.company_code,
                                    b.company_name,
                                    a.loc_code,
                                    c.name,
                                    a.sch_code,
                                    a.sch_name,
                                    a.start_time,
                                    a.end_time,
                                    a.late, 
                                    a.early, 
                                    a.checkin1, 
                                    a.checkout1, 
                                    a.checkin2, 
                                    a.checkout2, 
                                    a.overtimein1, 
                                    a.overtimeout1, 
                                    a.overtimein2, 
                                    a.overtimeout2, 
                                    a.overtimein3, 
                                    a.overtimeout3, 
                                    a.overtimein4, 
                                    a.overtimeout4, 
                                    a.breakein1, 
                                    a.breakeout1, 
                                    a.breakein2, 
                                    a.breakeout2, 
                                    a.breakein3, 
                                    a.breakeout3 FROM schedules_work a
                      LEFT JOIN company b ON a.company_code = b.company_code
                      LEFT JOIN location c ON a.loc_code = c.loc_code
				WHERE a.id = '$id'";
		$query = $conn->query($sql);
		$row = $query->fetch_assoc();

		echo json_encode($row);
	}
?>