<?php include 'includes/session.php'; ?>
<?php
include '../timezone.php';
$range_to = date('Y-m-d');
   $range_from = date('Y-m-01'); //date('Y-m-d', strtotime('-30 day', strtotime($range_to)));
   ?>
   <?php include 'includes/header.php'; ?>
   <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <?php include 'includes/navbar.php'; ?>
      <?php include 'includes/menubar.php'; ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Attendance
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Attendance</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <?php
          if(isset($_SESSION['error'])){
            echo "
            <div class='alert alert-danger alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-warning'></i> Error!</h4>
            ".$_SESSION['error']."
            </div>
            ";
            unset($_SESSION['error']);
          }
          if(isset($_SESSION['success'])){
            echo "
            <div class='alert alert-success alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-check'></i> Success!</h4>
            ".$_SESSION['success']."
            </div>
            ";
            unset($_SESSION['success']);
          }
          ?>
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-solid box-primary">
                <div class="box-header">
                  <div class="pull-right">
                    <form method="get" class="form-inline" >
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input autocomplete="off" type="text" class="form-control " id="start_date" name="start_date" value="<?php echo  $range_from; ?>" >
                      </div>
                      <input autocomplete="off" type="text" class="form-control " id="end_date" name="end_date" value="<?php echo  $range_to; ?>" >
                      
                      <button  class="btn btn-success btn-sm btn-flat" type="submit" value="FILTER" ><span class="fa fa-search-plus"></span> Search</button>
                      
                    </form>
                  </div>
                  <!-- <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a> -->
                </div>
                <div class="box-body">
                 <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                    <thead>
                      <th class="hidden"></th>
                      <th>Date</th>
                      <th>Days</th>
                      <th>Employee ID</th>
                      <th>Name</th>
                      <th>Location</th>
                      <th>Time In</th>
                      <th>Time Out</th>
                      <th>Work Hour</th>
                      <th>Work Minute</th>
                      <th>Late Minute</th>
                      <th>Overtime</th>
                      <th>Notes</th>
                      <!-- th>Tools</th> -->
                    </thead>
                    <tbody>
                      <?php

                      if(isset($_GET['start_date']))
                      {
                        
                        $nik =$user['username'];
                        $start_date = $_GET['start_date'];
                        $end_date = $_GET['end_date'];
                        $sql = "SELECT a.id,a.company_code,a.nik,b.name,b.loc_name,a.att_date,a.time_in,a.time_out,a.work_hour,a.work_minute,a.late_minute,a.overtime,a.overtime_hour1,a.overtime_hour2,a.overtime_hour3,a.overtime_hour4,a.total_overtime,a.notes,a.is_late,DAYNAME(a.att_date) AS Days FROM attendance_new a
                        LEFT JOIN employee_view b ON a.company_code = b.company_code AND a.nik = b.nik
                        where a.att_date between '$start_date' and '$end_date'
                        and a.nik ='$nik'
                        ORDER BY a.att_date desc";
                      }
                      else
                      {
                        $nik =$user['username'];
                        $start_date = $range_from;
                        $end_date = $range_to;

                        $sql = "SELECT a.id,a.company_code,a.nik,b.name,b.loc_name,a.att_date,a.time_in,a.time_out,a.work_hour,a.work_minute,a.late_minute,a.overtime,a.overtime_hour1,a.overtime_hour2,a.overtime_hour3,a.overtime_hour4,a.total_overtime,a.notes,a.is_late,DAYNAME(a.att_date) AS Days FROM attendance_new a
                        LEFT JOIN employee_view b ON a.company_code = b.company_code AND a.nik = b.nik
                        where a.att_date between '$start_date' and '$end_date'
                        and a.nik ='$nik'
                        ORDER BY a.att_date desc";

                      }
                      $query = $conn->query($sql);
                      while($row = $query->fetch_assoc()){
                      // $status = ($row['status'])?'<span class="label label-warning pull-right">ontime</span>':'<span class="label label-danger pull-right">late</span>';
                        ?>
                        <tr>
                          <td class='hidden'></td>
                          <td><?php echo date('M d, Y', strtotime($row['att_date'])); ?></td>
                          <td><?php echo $row['Days']; ?></td>
                          <td><?php echo $row['nik']; ?></td>
                          <td><?php echo $row['name']; ?></td>
                          <td><?php echo $row['loc_name']; ?></td>
                          <td><?php echo $row['time_in']; ?></td>
                          <td><?php echo $row['time_out']; ?></td>
                          <td><?php echo $row['work_hour']; ?></td>
                          <td><?php echo $row['work_minute']; ?></td>
                          <td><?php echo $row['late_minute']; ?>
                          <?php 
                          if($row['is_late'] == 1){
                            echo '<span class="label label-danger btn-flat">Late</span>';
                          }
                          ?>


                        </td>
                        <td><?php echo $row['overtime']; ?></td>
                        <td><span class="label label-danger"><?php echo $row['notes']; ?></span></td>
                          <!-- <td>

                             <a class="btn btn-xs btn-info"   data-toggle="tooltip" title="Edit Data <?php echo $row['id'];?>" href="shift_edit.php?shift_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-xs btn-warning"   data-toggle="tooltip" title="Delete Shift  <?php echo $row['id'];?>" href="shift_delete.php?id=<?php echo $row['id']; ?>"  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['id'] ?>-<?php echo  $row['group_name'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td> -->
                        </tr>
                        <?php



                      // echo "
                      //   <tr>
                      //     <td class='hidden'></td>
                      //     <td>".date('M d, Y', strtotime($row['att_date']))."</td>
                      //     <td>".$row['nik']."</td>
                      //     <td>".$row['name']."</td>
                      //     <td>".$row['time_in']."</td>
                      //     <td>".$row['time_out']."</td>
                      //     <td>".$row['work_hour']."</td>
                      //     <td>".$row['work_minute']."</td>
                      //     <td>
                      //       <button class='btn btn-success btn-sm btn-flat edit' data-id='".$row['id']."'><i class='fa fa-edit'></i> Edit</button>
                      //       <button class='btn btn-danger btn-sm btn-flat delete' data-id='".$row['id']."'><i class='fa fa-trash'></i> Delete</button>
                      //     </td>
                      //   </tr>
                      // ";
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>   
    </div>
    
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/attendance_modal.php'; ?>
  </div>
  <?php include 'includes/scripts.php'; ?>
  <script>
    $(function(){
      $('.edit').click(function(e){
        e.preventDefault();
        $('#edit').modal('show');
        var id = $(this).data('id');
        getRow(id);
      });

      $('.delete').click(function(e){
        e.preventDefault();
        $('#delete').modal('show');
        var id = $(this).data('id');
        getRow(id);
      });
    });

    function getRow(id){
      $.ajax({
        type: 'POST',
        url: 'attendance_row.php',
        data: {id:id},
        dataType: 'json',
        success: function(response){
          $('#datepicker_edit').val(response.date);
          $('#attendance_date').html(response.date);
          $('#edit_time_in').val(response.time_in);
          $('#edit_time_out').val(response.time_out);
          $('#attid').val(response.attid);
          $('#employee_name').html(response.firstname+' '+response.lastname);
          $('#del_attid').val(response.attid);
          $('#del_employee_name').html(response.firstname+' '+response.lastname);
        } 
      });
    }
  </script>
</body>
</html>
