<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
   <?php include 'includes/navbar.php'; ?>
   <?php include 'includes/menubar.php'; ?>
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Shift List
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li>Shift Management</li>
         <li class="active">Shift List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php
         if(isset($_SESSION['error'])){
           echo "
             <div class='alert alert-danger alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-warning'></i> Error!</h4>
               ".$_SESSION['error']."
             </div>
           ";
           unset($_SESSION['error']);
         }
         if(isset($_SESSION['success'])){
           echo "
             <div class='alert alert-success alert-dismissible'>
               <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <h4><i class='icon fa fa-check'></i> Success!</h4>
               ".$_SESSION['success']."
             </div>
           ";
           unset($_SESSION['success']);
         }
         ?>
      <!-- <form class="form-horizontal" action="<?php echo $aksi?>?module=pegawai&aksi=tambah" role="form" method="post"> -->
         <form  autocomplete="off" class="form-horizontal" method="POST"  role="form" action="shift_save.php">
         <div class="box box-solid box-primary">
            <div class="box-header">
               <h3 class="btn btn disabled box-title">
                  <i class="fa fa-user-md"></i> Shift Management 
               </h3>
               <a class="btn btn-default btn-sm pull-right btn-flat" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
               <i class="fa fa-minus"></i></a>
            </div>
            <div class="box-body">
              <form class="form-horizontal" method="POST" action="group_add.php">
                <div class="form-group">
                    <label for="company" class="col-sm-2 control-label">Company</label>

                    <div class="col-sm-9">
                      <select class="form-control  select2" name="company" id="company" required>
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM company";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['company_code']."'>".$comrow['company_name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="location_code" class="col-sm-2 control-label">Location</label>

                    <div class="col-sm-9">
                      <select class="form-control  select2" name="location_code" id="location_code" required onchange="loadGroup();loadTime();">
                        <option value="" selected>- Select -</option>
                        <?php
                          $sql = "SELECT * FROM location";
                          $query = $conn->query($sql);
                          while($comrow = $query->fetch_assoc()){
                            echo "
                              <option value='".$comrow['loc_code']."'>".$comrow['name']."</option>
                            ";
                          }
                        ?>
                      </select>
                    </div>
                </div>
                 <div class="form-group">
                  <label for="group_code" class="col-sm-2 control-label">Working Group</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="group_code" id="group_code" required>
                        <option value="" selected>- Select -</option>
                        <!-- <?php
                           $sql = "SELECT * FROM schedules_group";
                           $query = $conn->query($sql);
                           while($comrow = $query->fetch_assoc()){
                             echo "
                               <option value='".$comrow['group_code']."'>".$comrow['group_name']."</option>
                             ";
                           }
                           ?> -->
                     </select>
                  </div>
               </div>

                <div class="form-group">
                  <label for="sch_code" class="col-sm-2 control-label">Time Schedule</label>
                  <div class="col-sm-9">
                     <select class="form-control select2" name="sch_code" id="sch_code" required>
                        <option value="" selected>- Select -</option>
                        <!-- <?php
                           $sql = "SELECT * FROM schedules_group";
                           $query = $conn->query($sql);
                           while($comrow = $query->fetch_assoc()){
                             echo "
                               <option value='".$comrow['group_code']."'>".$comrow['group_name']."</option>
                             ";
                           }
                           ?> -->
                     </select>
                  </div>
               </div>
               <div class="form-group">
                    <label for="start_date" class="col-sm-2 control-label">Start Date</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control " id="start_date" name="start_date" required>
                    </div>
                </div>
            <div class="form-group">
               <label class="col-sm-4"></label>
               <div class="col-sm-5">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button> 
                  <button type="reset" class="btn btn-danger btn-flat"><i class="fa fa-refresh"></i> <i>Reset</i></button>
                  <a href="javascript:history.back()" class="btn btn-info pull-right btn-flat"><i class="fa fa-backward"></i> Kembali</a>        
               </div>
            </div>
         </div>
      </form>
   </section>
</div>
   <?php include 'includes/footer.php'; ?>
   <?php include 'includes/company_modal.php'; ?>
   </div>
   <?php include 'includes/scripts.php'; ?>
</body>
</html>