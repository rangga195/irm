<?php
	include 'includes/session.php';
	include 'includes/sendEmail-v156.php';
	if(isset($_POST['add'])){
		$no = $_POST['no'];
		$company_code = $_POST['company_code'];
		$nik = $_POST['nik'];
		$overtime_date = $_POST['overtime_date'];
		$start_time = $_POST['start_time'];
		$end_time = $_POST['end_time'];
		$notes = $_POST['notes'];
		$user = $user['username'];
		
		$sql = "SELECT * FROM overtime WHERE nik = '$nik' and overtime_date = '$overtime_date'";
		$query = $conn->query($sql);
		if($query->num_rows >= 1){
			$_SESSION['error'] = 'Data Already Exist';
		}
		else{
			$row = $query->fetch_assoc();
			$employee_id = $row['id'];
			$sql = "INSERT INTO overtime (no,company_code, nik,overtime_date,start_time,end_time,notes,status,create_by,create_date) 
				VALUES ('$no','$company_code', '$nik', '$overtime_date', '$start_time','$end_time','$notes',2,'$user',NOW())";
			if($conn->query($sql)){
				$data = mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.no,a.company_code,a.nik,b.name,b.division_name,b.department_name,b.description,b.loc_name,a.overtime_date,a.start_time,a.end_time,a.notes,a.approve_by,b.approval_level_1,b.approval_level_2 ,CASE a.approve_by 
                          WHEN  '' 
                          THEN CASE b.approval_level_1 WHEN '' THEN b.approval_level_2 ELSE  b.approval_level_1 END
                          ELSE b.approval_level_1 END AS Approve_To
				        FROM overtime a 
				        LEFT JOIN employee_view b ON a.nik = b.nik 
		           		 WHERE a.nik ='$nik' AND a.overtime_date ='$overtime_date'"));
		      
		         	$cekmail =mysqli_fetch_array(mysqli_query($conn,"SELECT a.id,a.nik,a.name,a.email_office FROM employee_view a
		                  WHERE a.nik ='".$data['Approve_To']."'"));
		            $ambil_smtp=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM smtp WHERE status='1' AND TYPE='admin'"));

			$to       = $cekmail['email_office'];
			$subject  = 'Overtime Approval Notification to '.$cekmail['nik'].' - '.$cekmail['name'].'';
			$message='<html><body>';          
			$message.='<br><span><span style="font-size: 11.0pt; font-family: Calibri">Dear Mr./Mrs. '.$cekmail['name'].',</span><span style="font-size: 11.0pt; font-family: Calibri"></span></span><br>';
			$message.='<span><span style="font-size: 11.0pt; font-family: Calibri">You get a notification to approve overtime from the following employees : </span></span>';
			$message.='<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message.="<tr style='background: #eee;'><td><strong>No.:</strong></td><td>" . $data['no'] . "</td></tr>";
			$message.="<tr><td><strong>Company Name:</strong></td><td>" . $data['company_code'] . "</td></tr>";
			$message.="<tr><td><strong>NIK:</strong></td><td>" . $data['nik'] . "</td></tr>";
			$message.="<tr><td><strong>Employee Name:</strong></td><td>" . $data['name'] . "</td></tr>";
			$message.="<tr><td><strong>Division:</strong></td><td>" . $data['division_name'] . "</td></tr>";
			$message.="<tr><td><strong>Department:</strong></td><td>" . $data['department_name'] . "</td></tr>";
			$message.="<tr><td><strong>Position:</strong></td><td>" . $data['description'] . "</td></tr>";
			$message.="<tr><td><strong>Location:</strong></td><td>" . $data['loc_name'] . "</td></tr>";
			$message.="<tr><td><strong>Overtime Date:</strong></td><td>" . $data['overtime_date']. "</td></tr>";
			$message.="<tr><td><strong>Starting Date:</strong></td><td>" . $data['start_time']. "</td></tr>";
			$message.="<tr><td><strong>Ending Date:</strong></td><td>" . $data['end_time'] . "</td></tr>";
			$message.="<tr><td><strong>Notes:</strong></td><td>" . $data['notes']. "</td></tr>";
			$message.= "</table>";
			$message.='<br>Click the following button to:</br>';

			$message.='<p><a href="'.$ambil_smtp['base_url'].'/overtime_approve.php?edit=4&id='.$data['id'].'&username1='.$data['approval_level_1'].'&username2='.$data['approval_level_2'].'"><button class="btn btn-sm btn-success">Approve</button></a></p>';		          
			$message.='<p><a href="'.$ambil_smtp['base_url'].'/overtime_reject.php?edit=100&id='.$data['id'].'&username1='.$data['approval_level_1'].'&username2='.$data['approval_level_2'].'"><button class="btn btn-sm btn-danger">Reject</button></a></p>';
			
			$message.='<div class="footer">';
			$message.='            <table role="presentation" border="0" cellpadding="0" cellspacing="0">';
			$message.='               <tr>';
			$message.='                 <td class="content-block">';
			$message.='                   <br><span class="apple-link">PT. Tangkas Cipta Optimal, Maspion Plaza 8th Floor</span></br';
			$message.='                   <br><span class="apple-link">Jl. Gunung Sahari Kav.18 Jakarta, 14420, INDONESIA</span></br>';
			$message.='                 </td>';
			$message.='               </tr>';
			$message.='               <tr>';
			$message.='                 <td class="content-block powered-by">';
			$message.='                   Copyright &copy; <a href="http://taco.co.id">TACO GROUP</a>.';
			$message.='                 </td>';
			$message.='               </tr>';
			$message.='             </table>';
			$message.='           </div>';
			$message.='</body></html>';
			
			
			$sender   = $ambil_smtp['email'];
			$password = $ambil_smtp['password'];
		           //$password = base64_decode($ambilsmtp['password']);

			if(email_localhost($to, $subject, $message, $sender, $password))
				echo "<script>alert('Approval has been sent!')</script>";
			
			else
				echo "Email sending failed";




			$_SESSION['success'] = 'Overtime added successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}
	}
}	
else{
	$_SESSION['error'] = 'Fill up add form first';
}

header('location: overtime.php');

?>