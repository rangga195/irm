<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Time Schedule
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Time Schedule</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid box-primary">
            <div class="box-header ">
              <a href="time_add.php" data-toggle="form" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
            </div>
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped nowrap stripe hover">
                <thead>
                  <th>Company</th>
                  <th>Location</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $sql = "SELECT  a.id,
                                    a.company_code,
                                    b.company_name,
                                    a.loc_code,
                                    c.name,
                                    a.sch_code,
                                    a.sch_name,
                                    a.start_time,
                                    a.end_time,
                                    a.late, 
                                    a.early, 
                                    a.checkin1, 
                                    a.checkout1, 
                                    a.checkin2, 
                                    a.checkout2, 
                                    a.overtimein1, 
                                    a.overtimeout1, 
                                    a.overtimein2, 
                                    a.overtimeout2, 
                                    a.overtimein3, 
                                    a.overtimeout3, 
                                    a.overtimein4, 
                                    a.overtimeout4, 
                                    a.breakein1, 
                                    a.breakeout1, 
                                    a.breakein2, 
                                    a.breakeout2, 
                                    a.breakein3, 
                                    a.breakeout3 FROM schedules_work a
                      LEFT JOIN company b ON a.company_code = b.company_code
                      LEFT JOIN location c ON a.loc_code = c.loc_code";
                    $query = $conn->query($sql);
                    while($row = $query->fetch_assoc()){
                       ?>
                        <tr>
                         <td><?php echo $row['company_code']; ?></td>
                          <td><?php echo $row['name']; ?></td>
                          <td><?php echo $row['sch_code']; ?></td>
                          <td><?php echo $row['sch_name']; ?></td>
                          <td><?php echo $row['start_time']; ?></td>
                          <td><?php echo $row['end_time']; ?></td>
                          <td>
                            <a class="btn btn-xs btn-info"   data-toggle="tooltip" title="Edit Data <?php echo $row['name'];?>" href="time_edit.php?time_add=edit&id=<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a> 

                         
                            <a class="btn btn-xs btn-warning"   data-toggle="tooltip" title="Delete Time  <?php echo $row['name'];?>" href="time_delete.php?id=<?php echo $row['id']; ?>""  alt="Delete Data" name ="delete" onclick="return confirm('Are you sure delete this data ? <?php echo  $row['name'] ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
                          </td>
                        </tr>
                       <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          </div>
        </div>
      </div>
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/time_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $('.edit').click(function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $('.delete').click(function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'time_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#timeid').val(response.id);
      $('#edit_code').val(response.sch_code);
      $('#edit_name').val(response.sch_name);
      $('#edit_start_time').val(response.start_time);
      $('#edit_end_time').val(response.end_time);
      $('#edit_late').val(response.late);
      $('#edit_early').val(response.early);
      $('#edit_checkin1').val(response.checkin1);
      $('#edit_checkout1').val(response.checkout1);
      $('#edit_checkin2').val(response.checkin2);
      $('#edit_checkout2').val(response.checkout2);
      $('#edit_ovetimein1').val(response.overtimein1);
      $('#edit_overtimeout1').val(response.overtimeout1);
      $('#edit_overtimein2').val(response.overtimein2);
      $('#edit_overtimeout2').val(response.overtimeout2);
      $('#edit_overtimein3').val(response.overtimein3);
      $('#edit_overtimeout3').val(response.overtimeout3);
      $('#edit_overtimein4').val(response.overtimein4);
      $('#edit_overtimeout4').val(response.overtimeout4);
      $('#edit_breakein1').val(response.breakein1);
      $('#edit_breakeout1').val(response.breakeout1);
      $('#edit_breakein2').val(response.breakein2);
      $('#edit_breakeout2').val(response.breakeout2);
      $('#edit_breakein3').val(response.breakein3);
      $('#edit_breakeout3').val(response.breakeout3);
      $('#del_timeid').val(response.id);
      $('#del_time').html(response.sch_name);
      $('#company_val').val(response.company_code).html(response.company_name);
      $('#location_val').val(response.loc_code).html(response.name);
    }
  });
}
</script>
</body>
</html>
