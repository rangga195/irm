<?php
	session_start();
	include 'conn.php';

	if(isset($_POST['login'])){
		$username = $_POST['username'];
		$password = $_POST['password'];
		$ip = getenv('HTTP_CLIENT_IP')?:
			getenv('HTTP_X_FORWARDED_FOR')?:
			getenv('HTTP_X_FORWARDED')?:
			getenv('HTTP_FORWARDED_FOR')?:
			getenv('HTTP_FORWARDED')?:
			getenv('REMOTE_ADDR');
		

		
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($conn, $username);
		$password = mysqli_real_escape_string($conn, $password);

		$sql = "SELECT * FROM admin WHERE username = '$username' and status =1";
		$query = $conn->query($sql);

		if($query->num_rows < 1){
			$_SESSION['error'] = 'Cannot find account with the username or account has been disable';
			header('location: index.php');

		}
		else{
			$row = $query->fetch_assoc();
			if(password_verify($password, $row['password'])){
				if($row['type']=='user')
				{
					$_SESSION['user'] = $row['id'];
					header('location:user/home.php');
					 // header('location:admin/home.php');
					
					$SqlLog ="INSERT INTO systemlog (userid,ipaddress,description,login_date)
							  VALUES ('$username','$ip','User Logged In',now())";
					$conn->query($SqlLog);
				}
				else
				{
					$_SESSION['admin'] = $row['id'];
					header('location:admin/home.php');
					
					$SqlLog ="INSERT INTO systemlog (userid,ipaddress,description,login_date)
							  VALUES ('$username','$ip','Admin Logged In',now())";
					$conn->query($SqlLog);
					
				}
				
			}
			else{
				$_SESSION['error'] = 'Incorrect password';
				header('location: index.php');
			}
		}
		
	}
	else{
		$_SESSION['error'] = 'Input admin credentials first';
	}

	 // header('location: index.php');


?>